function checkChildren(checkboxElement, checked) {
    var c = checkboxElement.nextSibling;
    var cc, ccc;
    
    while (c != null) {
	if (c.nodeName == 'UL' && c.hasChildNodes()) {
	    cc = c.firstChild;
	    while (cc != null) {
		if (cc.nodeName == "LI" && cc.hasChildNodes()) {
		    ccc = cc.firstChild;
		    while (ccc != null) {
			if (ccc.type == 'checkbox') {
			    ccc.checked = checked;
			    checkChildren(ccc, checked);
			}

			ccc = ccc.nextSibling;
		    }
		}
		
		cc = cc.nextSibling;
	    }
	}

	c = c.nextSibling;
    }
    
    return true;
}

var groupName = document.querySelectorAll('.tree_group > input[type=checkbox], .tree_group_family > input[type=checkbox]'); //.tree_group > label, .tree_group_family > label');

for(var i = 0, c = groupName.length; i < c; i++) {
    // console.log(groupName[i]);

    groupName[i].addEventListener('click', function(e){
	// var checkbox = e.currentTarget.previousSibling;
	// while (checkbox.type != 'checkbox') {
	//     checkbox = checkbox.previousSibling;
	// }
	checkChildren(e.currentTarget, e.currentTarget.checked);
    }, false);
}
