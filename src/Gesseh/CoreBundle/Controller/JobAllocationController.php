<?php

namespace Gesseh\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * @Route("/joballocation")
 * @Security("has_role('ROLE_USER_JOB_ALLOCATION')")
 */
class JobAllocationController extends Controller
{
    /**
     * @Route("/myjoballocations", name="GCore_MyJobAllocations")
     * @Template()
     */
    public function listMyJobAllocationsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $jobAllocations = $em->getRepository('GessehCoreBundle:JobAllocation')->getByUserWithCompleteJob($user->getId());

        $this->get('gesseh_core.period_handler')->sortPeriods($jobAllocations, 'asc');

        return array(
            'jobAllocations' => $jobAllocations
        );
    }
}