<?php

namespace Gesseh\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Gesseh\CoreBundle\Entity\JobAllocation;
use Gesseh\CoreBundle\Entity\JobAllocationPriority;
use Gesseh\CoreBundle\Entity\JobAllocationPeriod;
use Gesseh\CoreBundle\Form\JobAllocationJobAutoType;
use Gesseh\CoreBundle\Form\JobAllocationUserAutoType;
use Gesseh\CoreBundle\Form\JobAllocationPriorityType;
use Gesseh\CoreBundle\Form\JobAllocationPriorityUserAutoType;
use Gesseh\CoreBundle\Form\JobAllocationPeriodType;


/**
 * @Route("/admin/joballocation")
 * @Security("has_role('ROLE_ADMIN_JOB_ALLOCATION')")
 */
class JobAllocationAdminController extends Controller
{

    /**
     * Manage users allocated for a job
     *
     * @Route("/{id}/usersmanagement", name="GCore_AdminJobAllocationManagement", requirements={"id" = "\d+"})
     * @Template()
     */
    public function manageJobAllocationsAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $job = $em->getRepository('GessehCoreBundle:Job')->find($id);

        if (null == $job) {
            throw $this->createNotFoundException("Unable to find this job");
        }

        $jobAllocations = $em->getRepository('GessehCoreBundle:JobAllocation')->getByJob($id);

        return array(
            'job' => $job,
            'jobAllocations' => $jobAllocations
        );
    }

    /**
     * Manage period of job allocations
     *
     * @Route("/periods/{id}",
     name="GCore_AdminJobAllocationPeriods",
     requirements={"id" = "\d+"},
     defaults={"id" = -1})
     * @Template()
     */
    public function manageJobAllocationPeriodsAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $jobAllocationPeriods = $em->getRepository('GessehCoreBundle:JobAllocationPeriod')->getCompleteByGroups($this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_ADMIN_JOB_ALLOCATION', 'Group'));

        $newJobAllocationPeriod = null;
        
        if ($id >= 0) {
            foreach ($jobAllocationPeriods as $jobAllocationPeriod) {
                if ($id == $jobAllocationPeriod->getId()) {
                    $newJobAllocationPeriod = $jobAllocationPeriod;
                }
            }
        }

        if (null == $newJobAllocationPeriod) {
            $newJobAllocationPeriod = new JobAllocationPeriod();
        }

        $newJobAllocationPeriod->setCompleted(false);

        $form = $this->createForm('job_allocation_period', $newJobAllocationPeriod);

        $request = $this->getRequest();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($newJobAllocationPeriod);
            $em->flush();

            return $this->redirect($this->generateUrl('GCore_AdminJobAllocationPeriods'));
        }

        return array (
            'id' => $id,
            'jobAllocationPeriods' => $jobAllocationPeriods,
            'form' => $form->createView()
        );
    }

    
    /**
     * Manage job allocation period completion
     *
     * @Route("/period/{id}/completion",
     name="GCore_AdminJobAllocationPeriodCompletion",
     requirements={"id" = "\d+"})
    */
    public function changeJobAllocationPeriodCompletionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $jobAllocationPeriod = $em->getRepository('GessehCoreBundle:JobAllocationPeriod')->find($id);

        $jobAllocationPeriod->setCompleted(! $jobAllocationPeriod->isCompleted());

        $em->flush();

        return $this->redirect($this->generateUrl('GCore_AdminJobAllocationPeriods'));
    }
    
    
    /**
     * Manage job allocations for a period
     *
     * @Route("/period/{periodId}/{userId}/{jobAllocationId}",
     name="GCore_AdminJobAllocationForPeriod",
     requirements={"periodId" = "\d+", "userId" = "\d+", "jobAllocationId" = "\d+"},
     defaults={"userId" = -1, "jobAllocationId" = -1})
     * @Template()
     */
    public function manageJobAllocationsForPeriodAction(Request $request, $periodId, $userId, $jobAllocationId)
    {
        if ($userId < 0 and null != $request->request->get('userId')) {
            $userId = $request->request->get('userId');
        }
        
        $em = $this->getDoctrine()->getManager();

        $jobAllocationPeriod = $em->getRepository('GessehCoreBundle:JobAllocationPeriod')->getComplete($periodId);

        if (null == $jobAllocationPeriod) {
            throw $this->createNotFoundException("Unable to find this job allocation period"); 
        }

        
        /* $jobAllocations = $jobAllocationPeriod->getJobAllocations()->toArray(); */

        $groups = $this->get('gesseh_user.groups_hierarchy')->getReachableGroups($jobAllocationPeriod->getGroups()->toArray());
        
        $jobs = $em->getRepository('GessehCoreBundle:Job')->getJobWithAffiliationsByGroups($this->get('gesseh_user.groups_hierarchy')->getParentGroups($groups));

        $jobAllocations = $em->getRepository('GessehCoreBundle:JobAllocation')->getByJobsAndPeriod($jobs, $jobAllocationPeriod);
        foreach ($jobAllocationPeriod->getJobAllocations() as $jobAllocation) {
            if (! in_array($jobAllocation, $jobAllocations)) {
                $jobAllocations[] = $jobAllocation;
            }
        }

        // for validation
        $this->get('gesseh_core.job_allocation_period_handler')->setJobAllocations($jobAllocations);

        
        $alreadyAllocatedUsers = array();
        foreach ($jobAllocationPeriod->getJobAllocations() as $jobAllocation) {
            $alreadyAllocatedUsers[] = $jobAllocation->getUser();
        }
        
        $users = $em->getRepository('GessehUserBundle:User')->getCompleteByGroup($groups, $jobAllocationPeriod);

        $users = $this->get('gesseh_user.priority_handler')->getUsersByPriorityPeriod($users, $groups, $jobAllocationPeriod, 'jobAllocation');
        if (count ($users) < 1) {
            $this->get('session')->getFlashBag()->add('notice', "No users found for this job allocation period");
            return $this->redirect($this->generateUrl('GCore_AdminJobAllocationPeriods'));
        }
        ///////
        /* foreach ($users as $u) { */
        /*     echo $u->__toString().'<br>'; */
        /* } */
        /////

        $userToAllocate = null;
        foreach ($users as $key => $user) {
            $managedUser = $key;
            
            if ($userId >= 0) {
                if ($userId == $user->getId()) {
                    $userToAllocate = $user;
                    break;
                }
            }
            elseif (! in_array($user, $alreadyAllocatedUsers)) {
                $userToAllocate = $user;
                break;
            }
        }

        $allUsersAllocated = false;
        
        if (null == $userToAllocate) {
            if ($userId >= 0) {
                throw $this->createNotFoundException("User not found"); 
            }
            else {
                $allUsersAllocated = true;
                $this->get('session')->getFlashBag()->add('notice', "All users already have a job");
                /* return $this->redirect($this->generateUrl('GCore_AdminJobAllocationForPeriod', array('periodId' => $periodId, 'userId' => $jobAllocationPriorities[0]->getUser()->getId()))); */
                $userToAllocate = $users[0];
            }
        }
        
        $hospitals = array();
        $departments = array();
        foreach ($jobs as $job) {
            $department = $job->getDepartment();
            if (! in_array ($department, $departments)) {
                $departments[] = $department;
                if (! in_array ($department->getHospital(), $hospitals)) {
                        $hospitals[] = $department->getHospital();
                    }
            }
        }

        $newJobAllocation = null;
        
        foreach ($jobAllocationPeriod->getJobAllocations() as $jobAllocation) {
            if ($jobAllocationId == $jobAllocation->getId() or ($jobAllocationId < 0 and $userToAllocate == $jobAllocation->getUser())) {
                $newJobAllocation = $jobAllocation;
            }
        }

        if (null == $newJobAllocation) {
            $newJobAllocation = new JobAllocation();
            $newJobAllocation->setStartDate($jobAllocationPeriod->getStartDate());
            $newJobAllocation->setEndDate($jobAllocationPeriod->getEndDate());
            $newJobAllocation->setUser($userToAllocate);
            $newJobAllocation->setJobAllocationPeriod($jobAllocationPeriod);
        }

        $form = $this->createForm(new JobAllocationUserAutoType($jobs), $newJobAllocation);

        $request = $this->getRequest();

        $form->handleRequest($request);

        $nextUser = null;
        if (isset ($users[$managedUser +1])) {
            $nextUser = $users[$managedUser +1];
        }
            
        if ($form->get('cancel')->isClicked()) {
            return $this->redirect($this->generateUrl('GCore_AdminJobAllocationForPeriod', array('periodId' => $periodId, 'userId' => null != $nextUser?$nextUser->getId():-1)));
        }
        elseif ($form->get('remove')->isClicked()) {
            $em->remove($newJobAllocation);
            $em->flush();

            return $this->redirect($this->generateUrl('GCore_AdminJobAllocationForPeriod', array('periodId' => $periodId, 'userId' => $newJobAllocation->getUser()->getId(), 'jobAllocationId' => 0)));
        }
        elseif ($form->isValid()) {
            $em->persist($newJobAllocation);
            $em->flush();

            if ($form->get('saveAndAdd')->isClicked()) {
                return $this->redirect($this->generateUrl('GCore_AdminJobAllocationForPeriod', array('periodId' => $periodId, 'userId' => $newJobAllocation->getUser()->getId(), 'jobAllocationId' => 0)));
            }
            elseif ($form->get('save')->isClicked()) {
                return $this->redirect($this->generateUrl('GCore_AdminJobAllocationForPeriod', array('periodId' => $periodId, 'userId' => null != $nextUser?$nextUser->getId():-1)));
            }
        }

        
        return array(
            'jobAllocationPeriod' => $jobAllocationPeriod,
            'users' => $users,
            'jobAllocations' => $jobAllocations,
            'newJobAllocation' => $newJobAllocation,
            'nextUser' => $nextUser,
            'allUsersAllocated' => $allUsersAllocated,
            'jobs' => $jobs,
            'departments' => $departments,
            'hospitals' => $hospitals,
            'form' => $form->createView()
        );
    }

    
    /**
     * Allocate a job to a user
     *
     * @Route("/{id}/allocate", name="GCore_AdminAllocateUserToJob", requirements={"id" = "\d+"})
     * @Template()
     */
    public function allocateUserToJobAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('GessehCoreBundle:Job')->find($id);

        if (null == $job) {
            throw $this->createNotFoundException("Unable to find this job");
        }

        $jobAllocation = new JobAllocation();
        $jobAllocation->setJob($job);
        
        $form = $this->createForm(new JobAllocationJobAutoType($job), $jobAllocation);
        $request = $this->get('request');

        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em->persist($jobAllocation);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('notice', 'Job "' . $job->__toString() . '" allocated to user "'.$jobAllocation->getUser()->__toString().'"');

            return $this->redirect($this->generateUrl('GCore_AdminJobAllocationManagement', array('id' => $job->getId())));
        }

        return array(
            'job' => $job,
            'user_job_allocation_form' => $form->createView(),
        );
    }


    /**
     * Delete an allocation of user to a job
     *
     * @Route("/{id}/delete", name="GCore_AdminDeleteJobAllocation", requirements={"id" = "\d+"})
     */
    public function deleteJobAllocationAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $jobAllocation = $em->getRepository('GessehCoreBundle:JobAllocation')->getComplete($id);

        if (null == $jobAllocation) {
            throw $this->createNotFoundException("Unable to find this job allocation");
        }

        $em->remove($jobAllocation);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'Job allocation for user "'.$jobAllocation->getUser()->__toString().'" deleted');

        return $this->redirect($this->generateUrl('GCore_AdminJobAllocationManagement', array('id' => $jobAllocation->getJob()->getId())));
    }


    /* /\** */
    /*  * Manage priority of allocations */
    /*  * */
    /*  * @Route("/priority/management", name="GCore_AdminManageJobAllocationPriority") */
    /*  * @Template() */
    /*  *\/ */
    /* public function manageJobAllocationPrioritiesAction() */
    /* { */
    /*     $em = $this->getDoctrine()->getManager(); */

    /*     $groups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_ADMIN_JOB_ALLOCATION', 'Group'); */
        
    /*     $users = $em->getRepository('GessehUserBundle:User')->getCompleteByGroup($groups); */

    /*     $jobAllocationPriorities = $em->getRepository('GessehCoreBundle:JobAllocationPriority')->getCompleteByUsers($users); */

    /*     $usersWhithoutPriority = $users; */
    /*     foreach ($usersWhithoutPriority as $key => $userWhithoutPriority) { */
    /*         foreach ($jobAllocationPriorities as $jobAllocationPriority) { */
    /*             if ($jobAllocationPriority->getUser() == $userWhithoutPriority) { */
    /*                 unset ($usersWhithoutPriority[$key]); */
    /*             } */
    /*         } */
    /*     } */
        
    /*     $newJobAllocationPriority = new JobAllocationPriority(); */

    /*     $form = $this->createForm(new JobAllocationPriorityType($usersWhithoutPriority), $newJobAllocationPriority); */
        
    /*     $request = $this->get('request'); */

    /*     $form->handleRequest($request); */

    /*     if ($form->isValid()) { */
    /*         foreach ($jobAllocationPriorities as $jobAllocationPriority) { */
    /*             if ($jobAllocationPriority->getUser() == $newJobAllocationPriority->getUser()) { */
    /*                 $this->get('session')->getFlashBag()->add('notice', 'Job allocation priority has been updated for user "'.$jobAllocationPriority->getUser()->__toString().'" (rank was '.$jobAllocationPriority->getRank().' before)'); */
    /*                 $em->remove($jobAllocationPriority); */
    /*             } */
    /*         } */
            
    /*         $em->persist($newJobAllocationPriority); */
    /*         $em->flush(); */

    /*         return $this->redirect($this->generateUrl('GCore_AdminManageJobAllocationPriority')); */
    /*     } */

    /*     return array( */
    /*         'jobAllocationPriorities' => $jobAllocationPriorities, */
    /*         'form' => $form->createView() */
    /*     ); */
    /* } */


    /* /\** */
    /*  * Edit a priority for job allocation */
    /*  * */
    /*  * @Route("/priority/{id}/edit", name="GCore_AdminEditJobAllocationPriority", requirements={"id" = "\d+"}) */
    /*  * @Template("GessehCoreBundle:JobAllocationAdmin:manageJobAllocationPriorities.html.twig") */
    /*  *\/ */
    /* public function editJobAllocationPriorityAction($id) */
    /* { */
    /*     $em = $this->getDoctrine()->getManager(); */

    /*     $groups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_ADMIN_JOB_ALLOCATION', 'Group'); */
        
    /*     $users = $em->getRepository('GessehUserBundle:User')->getCompleteByGroup($groups); */

    /*     $jobAllocationPriorities = $em->getRepository('GessehCoreBundle:JobAllocationPriority')->getCompleteByUsers($users); */

    /*     $jobAllocationPriority = $em->getRepository('GessehCoreBundle:JobAllocationPriority')->find($id); */

    /*     if (null == $jobAllocationPriority) { */
    /*         throw $this->createNotFoundException("Unable to find this job allocation priority"); */
    /*     } */

    /*     $form = $this->createForm(new JobAllocationPriorityUserAutoType(), $jobAllocationPriority); */

    /*     $request = $this->get('request'); */

    /*     $form->handleRequest($request); */

    /*     if ($form->isValid()) { */
    /*         $em->flush(); */

    /*         return $this->redirect($this->generateUrl('GCore_AdminManageJobAllocationPriority')); */
    /*     } */

    /*     return array( */
    /*         'jobAllocationPriorities' => $jobAllocationPriorities, */
    /*         'id' => $id, */
    /*         'form' => $form->createView() */
    /*     ); */
    /* } */


    /* /\** */
    /*  * Delete a priority for job allocation */
    /*  * */
    /*  * @Route("/priority/{id}/delete", name="GCore_AdminDeleteJobAllocationPriority", requirements={"id" = "\d+"}) */
    /*  *\/ */
    /* public function deleteJobAllocationPriorityAction($id) */
    /* { */
    /*     $em = $this->getDoctrine()->getManager(); */

    /*     $jobAllocationPriority = $em->getRepository('GessehCoreBundle:JobAllocationPriority')->getComplete($id); */

    /*     if (! $jobAllocationPriority) { */
    /*         throw $this->createNotFoundException("Unable to find this priority"); */
    /*     } */

    /*     $this->get('session')->getFlashBag()->add('notice', 'Job allocation priority (user "'.$jobAllocationPriority->getUser()->__toString().'", rank = '.$jobAllocationPriority->getRank().') deleted'); */
    /*     $em->remove($jobAllocationPriority); */
    /*     $em->flush(); */

    /*     return $this->redirect($this->generateUrl('GCore_AdminManageJobAllocationPriority')); */
    /* } */
}