<?php

namespace Gesseh\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * @Route("/jobsite")
 * @Security("has_role('ROLE_USER_JOBSITE')")
 */
class JobSiteController extends Controller
{
    /**
     * @Route("/jobsites", name="GCore_ListJobSites")
     * @Template()
     */
    public function listJobSitesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $jobs = $em->getRepository('GessehCoreBundle:Job')->getJobByGroups($this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_USER_JOBSITE', 'Group'));

        if (count ($jobs) < 1) {
            $this->get('session')->getFlashBag()->add('warning', "No jobs found");
        }

        $departments = $em->getRepository('GessehCoreBundle:Department')->getDepartmentByJobs($jobs);

        $hospitals = $em->getRepository('GessehCoreBundle:Hospital')->getHospitalByDepartments($departments);

        return array(
            'hospitals' => $hospitals,
            'departments' => $departments,
            'jobs' => $jobs
        );
    }

    /**
     * @Route("/hospital/{id}", name="GCore_ShowHospital", requirements={"id" = "\d+"})
     * @Template()
     */
    public function showHospitalAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $hospital = $em->getRepository('GessehCoreBundle:Hospital')->find($id);

        if (null == $hospital) {
            throw $this->createNotFoundException("Unable to find this hospital");
        }

        return array(
            'hospital' => $hospital
        );
    }

    /**
     * @Route("/department/{id}", name="GCore_ShowDepartment", requirements={"id" = "\d+"})
     * @Template()
     */
    public function showDepartmentAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $department = $em->getRepository('GessehCoreBundle:Department')->find($id);

        if (null == $department) {
            throw $this->createNotFoundException("Unable to find this department");
        }

        return array(
            'department' => $department
        );
    }

    /**
     * @Route("/job/{id}", name="GCore_ShowJob", requirements={"id" = "\d+"})
     * @Template()
     */
    public function showJobAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $job = $em->getRepository('GessehCoreBundle:Job')->getWithSectors($id);

        if (null == $job) {
            throw $this->createNotFoundException("Unable to find this job");
        }

        return array(
            'job' => $job
        );        
    }
}