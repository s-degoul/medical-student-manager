<?php

namespace Gesseh\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\ExpressionLanguage\Expression;

use Gesseh\CoreBundle\Entity\Hospital;
use Gesseh\CoreBundle\Form\HospitalType;
use Gesseh\CoreBundle\Entity\Sector;
use Gesseh\CoreBundle\Form\SectorType;
use Gesseh\CoreBundle\Entity\Department;
use Gesseh\CoreBundle\Form\DepartmentType;
use Gesseh\CoreBundle\Entity\Job;
use Gesseh\CoreBundle\Form\JobType;


/**
 * @Route("/admin/jobsite")
 */
class JobSiteAdminController extends Controller
{
    /**
     * @Route("/jobsites", name="GCore_AdminListJobSites")
     * @Template()
     * @Security("has_role('ROLE_ADMIN_JOB')")
     */
    public function listJobSitesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $jobs = $em->getRepository('GessehCoreBundle:Job')->getJobByGroups($this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_ADMIN_JOB', 'Group'));

        if ($this->get('security.authorization_checker')->isGranted(new Expression("has_role('ROLE_ADMIN_DEPARTMENT')"))) {
            $departments = $em->getRepository('GessehCoreBundle:Department')->findAll();
        }
        else {
            $departments = $em->getRepository('GessehCoreBundle:Department')->getDepartmentByJobs($jobs);
        }

        if ($this->get('security.authorization_checker')->isGranted(new Expression("has_role('ROLE_ADMIN_HOSPITAL')"))) {
            $hospitals = $em->getRepository('GessehCoreBundle:Hospital')->findAll();
        }
        else {
            $hospitals = $em->getRepository('GessehCoreBundle:Hospital')->getHospitalByDepartments($departments);
        }

        
        return array(
            'hospitals' => $hospitals,
            'departments' => $departments,
            'jobs' => $jobs
        );
    }
    
    /**
     * Lists all sectors
     *
     * @Route("/sectors", name="GCore_AdminListSectors")
     * @Template()
     * @Security("has_role('ROLE_ADMIN_JOB')")
     */
    public function listSectorsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $sectors = $em->getRepository('GessehCoreBundle:Sector')->findAll();

        return array(
            'sectors' => $sectors
        );
    }

    
    /**
     * Displays a form to create a new Hospital entity.
     *
     * @Route("/hospital/new", name="GCore_AdminNewHospital")
     * @Template("GessehCoreBundle:JobSiteAdmin:editHospital.html.twig")
     * @Security("has_role('ROLE_ADMIN_HOSPITAL')")
     */
    public function newHospitalAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hospital = new Hospital();
        $form = $this->createForm(new HospitalType(), $hospital, array(
                                      'cancel_action' => $this->generateUrl('GCore_AdminListJobSites')
                                  ));
        
        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($hospital);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Registration of hospital "' . $hospital->__toString() . '" completed');

            return $this->redirect($this->generateUrl('GCore_AdminListJobSites'));
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Hospital entity.
     *
     * @Route("/hospital/{id}/edit", name="GCore_AdminEditHospital", requirements={"id" = "\d+"})
     * @Template()
     * @Security("has_role('ROLE_ADMIN_HOSPITAL')")
     */
    public function editHospitalAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $hospital = $em->getRepository('GessehCoreBundle:Hospital')->find($id);

        if (null == $hospital)
            throw $this->createNotFoundException("Unable to find this hospital");

        $form = $this->createForm(new HospitalType(), $hospital, array(
                                      'cancel_action' => $this->generateUrl('GCore_AdminListJobSites')
                                  ));
        
        $request = $this->get('request');

        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em->persist($hospital);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Hospital "' . $hospital->__toString() . '" updated');

            return $this->redirect($this->generateUrl('GCore_AdminListJobSites'));
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * Deletes an Hospital
     *
     * @Route("/hospital/{id}/delete", name="GCore_AdminDeleteHospital", requirements={"id" = "\d+"}))
     * @Security("has_role('ROLE_ADMIN_HOSPITAL')")
     */
    public function deleteHospitalAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $hospital = $em->getRepository('GessehCoreBundle:Hospital')->find($id);

        if (!$hospital)
            throw $this->createNotFoundException("Unable to find this hospital");

        $em->remove($hospital);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Hospital "' . $hospital->__toString() . '" deleted');

        return $this->redirect($this->generateUrl('GCore_AdminListJobSites'));
    }

    /**
     * @Route("/department/new/{hospitalId}", name="GCore_AdminNewDepartment", requirements={"hospitalId" = "\d+"})
     * @Template("GessehCoreBundle:JobSiteAdmin:editDepartment.html.twig")
     * @Security("has_role('ROLE_ADMIN_DEPARTMENT')")
     */
    public function newDepartmentAction($hospitalId = -1)
    {
        $em = $this->getDoctrine()->getManager();

        $department = new Department();

        $hospital = $em->getRepository('GessehCoreBundle:Hospital')->find($hospitalId);
        if ($hospital) {
            $department->setHospital($hospital);
        }
        
        $form = $this->createForm(new DepartmentType(), $department, array(
                                      'cancel_action' => $this->generateUrl('GCore_AdminListJobSites')
                                  ));
        
        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($department);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Registration of department "' . $department->__toString() . '" completed');

            return $this->redirect($this->generateUrl('GCore_AdminListJobSites'));
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/department/{id}/edit", name="GCore_AdminEditDepartment", requirements={"id" = "\d+"})
     * @Template()
     * @Security("has_role('ROLE_ADMIN_DEPARTMENT')")
     */
    public function editDepartmentAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $department = $em->getRepository('GessehCoreBundle:Department')->find($id);

        if (null == $department) {
            throw $this->createNotFoundException("Unable to find this department");
        }
        
        $form = $this->createForm(new DepartmentType(), $department, array(
                                      'cancel_action' => $this->generateUrl('GCore_AdminListJobSites')
                                  ));
        
        $request = $this->get('request');

        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em->persist($department);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Department "' . $department->__toString() . '" updated');

            return $this->redirect($this->generateUrl('GCore_AdminListJobSites'));
        }

        return array(
            'form' => $form->createView(),
        );
    }

    
    /**
     * Deletes a Department
     *
     * @Route("/department/{id}/delete", name="GCore_AdminDeleteDepartment", requirements={"id" = "\d+"}))
     * @Security("has_role('ROLE_ADMIN_DEPARTMENT')")
     */
    public function deleteDepartmentAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $department = $em->getRepository('GessehCoreBundle:Department')->find($id);

        if (!$department)
            throw $this->createNotFoundException("Unable to find this department");

        $em->remove($department);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Department "' . $department->getName() . '" deleted');

        return $this->redirect($this->generateUrl('GCore_AdminListJobSites'));
    }

    /**
     * @Route("/job/new/{departmentId}", name="GCore_AdminNewJob", requirements={"departmentId" = "\d+"})
     * @Template("GessehCoreBundle:JobSiteAdmin:editJob.html.twig")
     * @Security("has_role('ROLE_ADMIN_JOB')")
     */
    public function newJobAction($departmentId = -1)
    {
        $em = $this->getDoctrine()->getManager();

        $job = new Job();

        $department = $em->getRepository('GessehCoreBundle:Department')->find($departmentId);
        if ($department) {
            $job->setDepartment($department);
        }
        
        $form = $this->createForm('job', $job, array(
                                      'cancel_action' => $this->generateUrl('GCore_AdminListJobSites')
                                  ));
        
        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($job);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Registration of job "' . $job->__toString() . '" completed');

            return $this->redirect($this->generateUrl('GCore_AdminListJobSites'));
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/job/{id}/edit", name="GCore_AdminEditJob", requirements={"id" = "\d+"})
     * @Template()
     * @Security("has_role('ROLE_ADMIN_JOB')")
     */
    public function editJobAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $job = $em->getRepository('GessehCoreBundle:Job')->find($id);

        if (null == $job) {
            throw $this->createNotFoundException("Unable to find this job");
        }
        
        $form = $this->createForm('job', $job, array(
                                      'cancel_action' => $this->generateUrl('GCore_AdminListJobSites')
                                  ));
        
        $request = $this->get('request');

        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em->persist($job);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Job "' . $job->__toString() . '" updated');

            return $this->redirect($this->generateUrl('GCore_AdminListJobSites'));
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * Deletes a Job
     *
     * @Route("/job/{id}/delete", name="GCore_AdminDeleteJob", requirements={"id" = "\d+"})
     * @Security("has_role('ROLE_ADMIN_JOB')")
     */
    public function deleteJobAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('GessehCoreBundle:Job')->find($id);

        if (!$job)
            throw $this->createNotFoundException("Unable to find this job");

        $em->remove($job);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Job "' . $job->__toString() . '" deleted');

        return $this->redirect($this->generateUrl('GCore_AdminListJobSites'));
    }

   
    /**
     * Displays a form to create a new Sector entity.
     *
     * @Route("/sector/new", name="GCore_AdminNewSector")
     * @Template("GessehCoreBundle:JobSiteAdmin:editSector.html.twig")
     * @Security("has_role('ROLE_ADMIN_JOB')")
     */
    public function newSectorAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sector = new Sector();
        $form = $this->createForm(new SectorType(), $sector, array(
                                      'cancel_action' => $this->generateUrl('GCore_AdminListSectors')
                                  ));
        
        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($sector);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Registration of sector "' . $sector->__toString() . '" completed');

            $nextAction = $form->get('save')->isClicked()?'GCore_AdminListSectors':'GCore_AdminNewSector';
            return $this->redirect($this->generateUrl($nextAction));
        }

        return array (
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Sector
     *
     * @Route("/sector/{id}/edit", name="GCore_AdminEditSector", requirements={"id" = "\d+"})
     * @Template()
     * @Security("has_role('ROLE_ADMIN_JOB')")
     */
    public function editSectorAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $sector = $em->getRepository('GessehCoreBundle:Sector')->find($id);

        if (null == $sector) {
            throw $this->createNotFoundException("Unable to find this sector");
        }

        $form = $this->createForm(new SectorType(), $sector, array(
                                      'cancel_action' => $this->generateUrl('GCore_AdminListSectors')
                                  ));
        
        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($sector);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Sector "' . $sector->__toString() . '" updated');

            return $this->redirect($this->generateUrl('GCore_AdminListSectors'));
        }

        return array (
            'form' => $form->createView()
        );
    }

    
    /**
     * Deletes a Sector
     *
     * @Route("/sector/{id}/delete", name="GCore_AdminDeleteSector")
     * @Security("has_role('ROLE_ADMIN_JOB')")
     */
    public function deleteSectorAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $sector = $em->getRepository('GessehCoreBundle:Sector')->find($id);

        if (null == $sector) {
            throw $this->createNotFoundException("Unable to find this sector");
        }

        $em->remove($sector);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Sector "' . $sector->__toString() . '" deleted');

        return $this->redirect($this->generateUrl('GCore_AdminListSectors'));
    }
}
