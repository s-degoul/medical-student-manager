<?php

namespace Gesseh\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gesseh\CoreBundle\Entity\Department;

class LoadDepartmentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $departments = array (
            "dechoc_med" => array("Déchoquage", "Girardie Patrick", null, "0320445959", null, "CHRU"),
            "rea_NC" => array("Réa Neurochir", "Bernard Riegel", null, "0320445980", null, "CHRU"),
            "rea_DK" => array("Réanimation", "Sébastien Beague", "sebastien.beague@chd.fr", "0328285960", "Très bien !", "CHD"),
            "urg_DK" => array("Urgences", "Cédric Couturier", null, "0328285970", null, "CHD")
        );
        
        foreach ($departments as $i => $department)
        {
            $listDepartments[$i] = new Department();
            
            $listDepartments[$i]->setName($department[0]);
            $listDepartments[$i]->setHead($department[1]);
            $listDepartments[$i]->setEmail($department[2]);
            $listDepartments[$i]->setPhone($department[3]);
            $listDepartments[$i]->setDescription($department[4]);
            
            $listDepartments[$i]->setHospital($this->getReference('hospital_'.$department[5]));
            
            $manager->persist($listDepartments[$i]);
            
            $this->addReference('department_'.$i, $listDepartments[$i]);
        }
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 2;
    }
}

