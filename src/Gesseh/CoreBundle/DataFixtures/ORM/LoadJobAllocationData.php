<?php

namespace Gesseh\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gesseh\CoreBundle\Entity\JobAllocation;

class LoadJobAllocationData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $jobAllocations = array (
            array(null, null, "admin", "int_dechoc_med"),
            array(new \DateTime("2015-11-01"), new \DateTime("2016-04-30"), "user0", "int_rea_NC"),
            array(new \DateTime("2016-05-01"), new \DateTime("2016-10-31"), "user0", "int_dechoc_med"),
            array(null, null, 'user1', "int_urg_DK")
        );
        
        foreach ($jobAllocations as $i => $jobAllocation)
        {
            $listJobAllocations[$i] = new JobAllocation();
            
            $listJobAllocations[$i]->setStartDate($jobAllocation[0]);
            $listJobAllocations[$i]->setEndDate($jobAllocation[1]);
            
            $listJobAllocations[$i]->setUser($this->getReference('user_'.$jobAllocation[2]));
            $listJobAllocations[$i]->setJob($this->getReference('job_'.$jobAllocation[3]));
            
            $manager->persist($listJobAllocations[$i]);
        }
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 4;
    }
}

