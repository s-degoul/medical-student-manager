<?php

namespace Gesseh\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gesseh\CoreBundle\Entity\Sector;

class LoadSectorData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $sectors = array(
            'rea_med' => 'réanimation médicale',
            'rea_chir' => 'réanimation chirurgicale',
            'anesth_tete_cou' => 'anesthésie tête et cou',
            'anesth_CCV' => 'anesthésie CCV',
            'anesth_chir_dig' => 'anesthésie chir dig',
            'urgences' => 'urgences'
        );

        foreach ($sectors as $i => $sector) {
            $listSectors[$i] = new Sector();

            $listSectors[$i]->setName($sector);

            $manager->persist($listSectors[$i]);

            $this->addReference('sector_'.$i, $listSectors[$i]);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}