<?php

namespace Gesseh\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gesseh\CoreBundle\Entity\JobAllocationPriority;

class LoadJobAllocationPriorityData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $jobAllocationPriorities = array (
            array(
                'user' => 'user0',
                'rank' => 2064
            ),
            array(
                'user' => 'user1',
                'rank' => 90
            ),
            array(
                'user' => 'user2',
                'rank' => 15
            ),
            array(
                'user' => 'user3',
                'rank' => 2
            ),
             array(
                'user' => 'user4',
                'rank' => 1065
            ),
             array(
                'user' => 'user5',
                'rank' => 2056
            ),
             array(
                'user' => 'user6',
                'rank' => 517
            ),
             array(
                'user' => 'user7',
                'rank' => 896
            ),
             array(
                'user' => 'user8',
                'rank' => 3001
            ),
             array(
                'user' => 'user9',
                'rank' => 56
            ),
             array(
                'user' => 'user10',
                'rank' => 1568
            ),
        );
        
        foreach ($jobAllocationPriorities as $i => $jobAllocationPriority)
        {
            $listJobAllocationPriority[$i] = new JobAllocationPriority();
            
            $listJobAllocationPriority[$i]->setUser($this->getReference('user_'.$jobAllocationPriority['user']));
            $listJobAllocationPriority[$i]->setRank($jobAllocationPriority['rank']);
            
            $manager->persist($listJobAllocationPriority[$i]);
        }
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 3;
    }
}

