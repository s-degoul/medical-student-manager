<?php

namespace Gesseh\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gesseh\CoreBundle\Entity\Job;

class LoadJobData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $jobs = array (
            "int_dechoc_med" => array(
                'name' => "Interne du déchoquage",
                'maxAllocationsNumber' => 2,
                'department' => "dechoc_med",
                'sectors' => array('rea_med'),
                'groups' => array('DES_anesth', 'DESC_rea_med')
            ),
            "int_rea_NC" => array(
                'name' => "Interne de réa neurochir",
                'maxAllocationsNumber' => 3,
                'department' => "rea_NC",
                'sectors' => array('rea_chir'),
                'groups' => array('DES_anesth')
            ),
            "int_rea_DK_anesth" => array(
                'name' => "Interne de réanimation anesth",
                'maxAllocationsNumber' => 2,
                'department' => "rea_DK",
                'sectors' => array('rea_med'),
                'groups' => array('DES_anesth', 'DESC_rea_med')
            ),
            "int_rea_DK_med_ge" => array(
                'name' => "Interne de réanimation med gé",
                'maxAllocationsNumber' => null,
                'department' => "rea_DK",
                'sectors' => array('rea_med'),
                'groups' => array('DESC_rea_med')
            ),
            "int_urg_DK" => array(
                'name' => "Interne des urgences",
                'maxAllocationsNumber' => 4,
                'department' => "urg_DK",
                'sectors' => array('urgences'),
                'groups' => array('DES_cardio')
            )
        );
        
        foreach ($jobs as $i => $job)
        {
            $listJobs[$i] = new Job();
            
            $listJobs[$i]->setName($job['name']);
            $listJobs[$i]->setMaxAllocationsNumber($job['maxAllocationsNumber']);
            
            $listJobs[$i]->setDepartment($this->getReference('department_'.$job['department']));
            foreach ($job['sectors'] as $sector) {
                $listJobs[$i]->addSector($this->getReference('sector_'.$sector));
            }

            foreach ($job['groups'] as $group) {
                $listJobs[$i]->addGroup($this->getReference('group_'.$group));
            }
            
            $manager->persist($listJobs[$i]);
            
            $this->addReference('job_'.$i, $listJobs[$i]);
        }
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 3;
    }
}

