<?php

namespace Gesseh\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gesseh\CoreBundle\Entity\Hospital;

class LoadHospitalData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $hospitals = array (
            "CHRU" => array("CHRU de Lille", "0320445962", "2 rue Oscar Lambret", "59000", "Lille", "France", null, "Plus grand CHU de France et de Navarre"),
            "CHD" => array("CH de Dunkerque", "0328285900", "1 rue Jeanne de Flandres", "59240", "Dunkerque", "France", null, "Hôpital le plus au nord de France")
        );
        
        foreach ($hospitals as $i => $hospital)
        {
            $listHospitals[$i] = new Hospital();
            $listHospitals[$i]->setName($hospital[0]);
            $listHospitals[$i]->setPhone($hospital[1]);
            $listHospitals[$i]->setStreet($hospital[2]);
            $listHospitals[$i]->setPostcode($hospital[3]);
            $listHospitals[$i]->setCity($hospital[4]);
            $listHospitals[$i]->setCountry($hospital[5]);
            $listHospitals[$i]->setWebsite($hospital[6]);
            $listHospitals[$i]->setDescription($hospital[7]);
            
            $manager->persist($listHospitals[$i]);
            
            $this->addReference('hospital_'.$i, $listHospitals[$i]);
        }
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 1;
    }
}

