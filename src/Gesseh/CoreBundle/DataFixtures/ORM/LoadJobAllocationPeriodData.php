<?php

namespace Gesseh\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gesseh\CoreBundle\Entity\JobAllocationPeriod;

class LoadJobAllocationPeriodData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $jobAllocationPeriods = array (
            array(
                'startDate' => new \DateTime("2015-11-01"),
                'endDate' => new \DateTime("2016-04-30"),
                'completed' => false,
                'groups' => array('anesth_S1', 'anesth_S2', 'anesth_S3')
            )
        );
        
        foreach ($jobAllocationPeriods as $i => $jobAllocationPeriod)
        {
            $listJobAllocationPeriods[$i] = new JobAllocationPeriod();
            
            $listJobAllocationPeriods[$i]->setStartDate($jobAllocationPeriod['startDate']);
            $listJobAllocationPeriods[$i]->setEndDate($jobAllocationPeriod['endDate']);
            $listJobAllocationPeriods[$i]->setCompleted($jobAllocationPeriod['completed']);
            foreach ($jobAllocationPeriod['groups'] as $group) {
                $listJobAllocationPeriods[$i]->addGroup($this->getReference('group_'.$group));
            }
            
            $manager->persist($listJobAllocationPeriods[$i]);
        }
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 2;
    }
}

