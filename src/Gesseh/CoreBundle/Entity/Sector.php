<?php

namespace Gesseh\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gesseh\CoreBundle\Entity\Job;

/**
 * Sector
 *
 * @ORM\Table(name="sector")
 * @ORM\Entity(repositoryClass="Gesseh\CoreBundle\Entity\SectorRepository")
 */
class Sector
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\CoreBundle\Entity\Job", mappedBy="sectors")
     */
    private $jobs;


    public function __construct()
    {
        $this->jobs = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Sector
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add a job
     *
     * @return Sector
     */
    public function addJob(Job $job)
    {
        if (! $this->jobs->contains($job)) {
            $this->jobs->add($job);
        }

        return $this;
    }


    /**
     * Remove a job
     *
     * @return Sector
     */
    public function removeJob(Job $job)
    {
        $this->jobs->removeElement($job);

        return $this;
    }


    /**
     * Get jobs
     *
     * @return ArrayCollection
     */
    public function getJobs()
    {
        return $this->jobs;
    }


    public function __toString()
    {
        return $this->getName();
    }
}
