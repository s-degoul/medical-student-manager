<?php

namespace Gesseh\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gesseh\CoreBundle\Service\PeriodHandler\PeriodInterface;
use Gesseh\UserBundle\Entity\Group;

/**
 * JobAllocationPeriod
 *
 * @ORM\Table(name="job_allocation_period")
 * @ORM\Entity(repositoryClass="Gesseh\CoreBundle\Entity\JobAllocationPeriodRepository")
 */
class JobAllocationPeriod implements PeriodInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime", nullable=false)
     */
    private $endDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="completed", type="boolean", nullable=false)
     */
    private $completed;

    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="job_allocation_period_group",
     joinColumns={@ORM\JoinColumn(name="job_allocation_period_id", referencedColumnName="id")},
     inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")})
    */
    private $groups;

    /**
     * @ORM\OneToMany(targetEntity="Gesseh\CoreBundle\Entity\JobAllocation", mappedBy="jobAllocationPeriod", cascade={"persist"})
     */
    private $jobAllocations;


    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->jobAllocations = new ArrayCollection();
    }

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return JobAllocationPeriod
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return JobAllocationPeriod
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set completion state
     *
     * @param boolean
     * @return self
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * Is completed ?
     *
     * @return boolean
     */
    public function isCompleted()
    {
        return $this->completed;
    }


    /**
     * Add group
     *
     * @return JobAllocationPeriod
     */
    public function addGroup(Group $group)
    {
        if (! $this->groups->contains($group)) {
            $this->groups->add($group);
        }

        return $this;
    }


    /**
     * Remove a group
     *
     * @return JobAllocationPeriod
     */
    public function removeGroup(Group $group)
    {
        $this->groups->removeElement($group);

        return $this;
    }


    /**
     * Get groups
     *
     * @return ArrayCollection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    
    /**
     * Get job allocations
     *
     * @return ArrayCollection
     */
    public function getJobAllocations()
    {
        return $this->jobAllocations;
    }
}
