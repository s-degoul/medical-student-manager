<?php

namespace Gesseh\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gesseh\CoreBundle\Entity\Department;
use Gesseh\CoreBundle\Entity\Sector;
use Gesseh\UserBundle\Entity\Group;

/**
 * Job
 *
 * @ORM\Table(name="job")
 * @ORM\Entity(repositoryClass="Gesseh\CoreBundle\Entity\JobRepository")
 */
class Job
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_allocations_number", type="integer", unique=false, length=4, nullable=true)
     */
    private $maxAllocationsNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="head", type="string", length=100, nullable=true)
     */
    private $head;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=25, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\CoreBundle\Entity\Department", inversedBy="jobs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $department;

    /**
     * @ORM\OneToMany(targetEntity="Gesseh\CoreBundle\Entity\JobAllocation", mappedBy="job", cascade={"persist","remove"})
     */
    private $jobAllocations;

    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\CoreBundle\Entity\Sector", inversedBy="jobs")
     * @ORM\JoinTable(name="job_sector",
     joinColumns={@ORM\JoinColumn(name="job_id", referencedColumnName="id")},
     inverseJoinColumns={@ORM\JoinColumn(name="sector_id", referencedColumnName="id")})
     */
    private $sectors;

    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="job_group",
     joinColumns={@ORM\JoinColumn(name="job_id", referencedColumnName="id")},
     inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")})
     */
    private $groups;
    

    public function __construct()
    {
        $this->jobAllocations = new ArrayCollection();
        $this->sectors = new ArrayCollection();
        $this->groups = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Job
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set max allocations number
     *
     * @param integer $maxAllocationsNumber
     * @return Job
     */
    public function setMaxAllocationsNumber($maxAllocationsNumber)
    {
        $this->maxAllocationsNumber = $maxAllocationsNumber;

        return $this;
    }

    /**
     * Get max allocations number
     *
     * @return integer
     */
    public function getMaxAllocationsNumber()
    {
        return $this->maxAllocationsNumber;
    }
    
    /**
     * Set head
     *
     * @param string $head
     * @return Job
     */
    public function setHead($head)
    {
        $this->head = $head;

        return $this;
    }

    /**
     * Get head
     *
     * @return string 
     */
    public function getHead()
    {
        return $this->head;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Job
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Job
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Job
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set department
     *
     * @param Department $department
     * @return Job
     */
    public function setDepartment(Department $department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }


    /**
     * Get job allocations
     *
     * @return ArrayCollection
     */
    public function getJobAllocations()
    {
        return $this->jobAllocations;
    }


    /**
     * Add sector
     *
     * @return Job
     */
    public function addSector(Sector $sector)
    {
        if (! $this->sectors->contains($sector)) {
            $this->sectors->add($sector);
            $sector->addJob($this);
        }

        return $this;
    }


    /**
     * Remove sector
     *
     * @return Job
     */
    public function removeSector(Sector $sector)
    {
        $this->sectors->removeElement($sector);

        return $this;
    }


    /**
     * Get sectors
     *
     * @return ArrayCollection
     */
    public function getSectors()
    {
        return $this->sectors;
    }


    /**
     * Add group
     *
     * @return Job
     */
    public function addGroup(Group $group)
    {
        if (! $this->groups->contains($group)) {
            $this->groups->add($group);
        }

        return $this;
    }


    /**
     * Remove a group
     *
     * @return Job
     */
    public function removeGroup(Group $group)
    {
        $this->groups->removeElement($group);

        return $this;
    }


    /**
     * Get groups
     *
     * @return ArrayCollection
     */
    public function getGroups()
    {
        return $this->groups;
    }


    public function __toString()
    {
        return $this->name;
    }
}
