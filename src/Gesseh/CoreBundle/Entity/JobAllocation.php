<?php

namespace Gesseh\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gesseh\CoreBundle\Entity\Job;
use Gesseh\UserBundle\Entity\User;
use Gesseh\CoreBundle\Service\PeriodHandler\PeriodInterface;

/**
 * JobAllocation
 *
 * @ORM\Table(name="job_allocation")
 * @ORM\Entity(repositoryClass="Gesseh\CoreBundle\Entity\JobAllocationRepository")
 */
class JobAllocation implements PeriodInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime", nullable=true)
     */
    private $endDate;
    
    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\CoreBundle\Entity\Job", inversedBy="jobAllocations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $job;
    
    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\CoreBundle\Entity\JobAllocationPeriod", inversedBy="jobAllocations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $jobAllocationPeriod;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return JobAllocation
     */
    public function setStartDate(\DateTime $startDate = null)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return JobAllocation
     */
    public function setEndDate(\DateTime $endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set job
     *
     * @param Job $job
     * @return JobAllocation
     */
    public function setJob(Job $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return Job 
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return JobAllocation
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User 
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set job allocation period
     *
     * @param JobAllocationPeriod $jobAllocationPeriod
     * @return JobAllocation
     */
    public function setJobAllocationPeriod(JobAllocationPeriod $jobAllocationPeriod)
    {
        $this->jobAllocationPeriod = $jobAllocationPeriod;

        return $this;
    }


    /**
     * Get job allocation period
     *
     * @return JobAllocationPeriod
     */
    public function getJobAllocationPeriod()
    {
        return $this->jobAllocationPeriod;
    }
}
