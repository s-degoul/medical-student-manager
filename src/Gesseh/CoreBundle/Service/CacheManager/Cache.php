<?php

namespace Gesseh\CoreBundle\Service\CacheManager;

class Cache
{
    private $type, $values;
    
    public function __construct($type, array $values = null)
    {
        $this->type = $type;
        if (null == $values)
            $this->values = array();
        else
            $this->values = $values;
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    
    public function getValues()
    {
        return $this->values;
    }
    
    public function setValues(array $values)
    {
        $this->values = $values;
        return $this;
    }
    
    public function addValue($value)
    {
        $this->values[] = $value;
        return $this;
    }
    
    public function addValues(array $values)
    {
        foreach ($values as $value)
        {
            $this->addValue($value);
        }
        return $this;
    }
    
}
