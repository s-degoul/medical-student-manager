<?php

namespace Gesseh\CoreBundle\Service\CacheManager;

class CacheManager
{
    private $cachesList;
    
    public function __construct()
    {
        $this->cachesList = array();
    }
    
    public function addCache($type, $values = null)
    {
        $cache = $this->getCache($type);
        if (null == $cache)
        {
            $cache = new Cache($type, $values);
            $this->cachesList[] = $cache;
        }
        elseif (null != $values)
        {
            $cache->addValues($values);
        }
        return $this;
    }
    
    public function getCache($type, $triggerError = false)
    {
        foreach ($this->cachesList as $cache)
        {
            if ($cache->getType() == $type)
                return $cache;
        }
        
        if ($triggerError)
            throw new \Exception(printf("No cache of type '%s' found", $type));
        else
            return null;
    }
    
    public function hasCache($type)
    {
        if (null == $this->getCache($type))
         return false;
        
        return true;
    }
}
