<?php

namespace Gesseh\CoreBundle\Service\PeriodHandler;

use Doctrine\ORM\EntityManager;

class Period implements PeriodInterface
{
    private $startDate, $endDate;
    
    public function __construct(\DateTime $startDate, \DateTime $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }
    
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }
    
    public function getStartDate()
    {
        return $this->startDate;
    }
    
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }
    
    public function getEndDate()
    {
        return $this->endDate;
    }

    public function __toString()
    {
        $format = 'Y-m-d H:i:s';
        
        return $this->startDate->format($format)." - ".$this->endDate->format($format);
    }
}
