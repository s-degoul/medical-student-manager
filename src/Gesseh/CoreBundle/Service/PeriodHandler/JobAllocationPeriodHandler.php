<?php

namespace Gesseh\CoreBundle\Service\PeriodHandler;

use Gesseh\CoreBundle\Entity\Job;


class JobAllocationPeriodHandler extends PeriodHandler
{
    private $jobAllocations;

    public function setJobAllocations(array $jobAllocations)
    {
        $this->jobAllocations = $jobAllocations;
    }

    public function getJobAllocations()
    {
        return $this->jobAllocations;
    }
    
    public function getJobAllocationsByJob(Job $job)
    {
        $jobAllocations = array();
        
        foreach ($this->getJobAllocations() as $jobAllocation) {
            if ($jobAllocation->getJob() == $job) {
                $jobAllocations[] = $jobAllocation;
            }
        }

        return $jobAllocations;
    }
}