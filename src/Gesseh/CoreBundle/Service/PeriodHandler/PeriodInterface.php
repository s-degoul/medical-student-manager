<?php

namespace Gesseh\CoreBundle\Service\PeriodHandler;

interface PeriodInterface
{
    public function setStartDate(\DateTime $startDate);

    public function getStartDate();

    public function setEndDate(\DateTime $endDate);

    public function getEndDate();
}