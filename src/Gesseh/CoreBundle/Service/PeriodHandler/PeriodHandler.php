<?php

namespace Gesseh\CoreBundle\Service\PeriodHandler;

class PeriodHandler
{
    private $period;

    public function __construct()
    {
        $this->period = new Period(new \DateTime(), new \DateTime());
    }

    
    private function createPeriod(\DateTime $startDate, \DateTime $endDate)
    {
        if ($endDate < $startDate) {
            throw new \Exception("End date must be posterior to start date");
        }
        
        return new Period($startDate, $endDate);
    }


    public function setPeriod(PeriodInterface $period)
    {
        $this->period = $this->createPeriod($period->getStartDate(), $period->getEndDate());

        return $this;
    }
    

    public function getPeriod()
    {
        return $this->period;
    }

    public static function getLastPeriods(array $periods, \DateTime $refDate = null)
    {
        $maxEndDate = null;
        if (null == $refDate) {
            $refDate = new \DateTime('now');
        }

        foreach ($periods as $period) {
            if (! $period instanceof PeriodInterface) {
                throw new \Exception("The array must contain objects that implement the Period interface");
            }

            if ($period->getStartDate() > $refDate) {
                continue;
            }
            
            if (null == $period->getEndDate()) {
                $maxEndDate = null;
                break;
            }
            elseif ($period->getEndDate() > $maxEndDate or null == $maxEndDate) {
                $maxEndDate = $period->getEndDate();
            }
        }

        $lastPeriods = array();
        foreach ($periods as $period){
            if ($period->getEndDate() == $maxEndDate) {
                $lastPeriods[] = $period;
            }
        }

        return $lastPeriods;
    }

    
    public function getDatesList(PeriodInterface $period, \DateInterval $dateInterval = null)
    {
        if (null == $dateInterval)
            $dateInterval = new \DateInterval('P1D');
        
        $datesList = array();
        $date = clone($period->getStartDate());
        
        do
        {
            $datesList[] = clone($date);
            $date->add($dateInterval);
        }
        while ($date <= $period->getEndDate());
        
        return $datesList;
    }
    
    public function getSubPeriodsList(PeriodInterface $period, $pattern = null)
    {
        $datesList = $this->getDatesList($period);
        $subPeriodsList = array();
        
        foreach ($datesList as $date)
        {
            $subPeriodsList[] = $this->createPeriod(clone($date), $date->add(new \DateInterval('PT14H')));
            //~ $subPeriodsList[] = array('startDate' => clone($date), 'endDate' => $date->add(new \DateInterval('PT14H')));
        }
        
        return $subPeriodsList;
    }
    
    public function enlargePeriod(PeriodInterface $period, \DateInterval $timeBefore, \DateInterval $timeAfter = null)
    {
        if (null == $timeAfter)
            $timeAfter = $timeBefore;
        
        $period->getStartDate()->sub($timeBefore);
        $period->getEndDate()->add($timeAfter);
        
        return $period;
    }


    /**
     * Does a period begin before a date ?
     *
     * @param PeriodInterface $period
     * @param \DateTime $refDate
     * @param bool $includeRefDate   Should a period beginning with date $refDate be considered as beginning before this date ?
     * 
     * @return bool
     */
    public static function beginBeforeDate(PeriodInterface $period, \DateTime $refDate, $includeRefDate = false)
    {
        if (null === $period->getStartDate()
            or ($refDate > $period->getStartDate() or ($includeRefDate and $refDate == $period->getStartDate()))) {
            return true;
        }

        return false;
    }

    /**
     * Does a period end after a date ?
     *
     * @param PeriodInterface $period
     * @param \DateTime $refDate
     * @param bool $includeRefDate   Should a period ending with date $refDate be considered as ending after this date ?
     * 
     * @return bool
     */
    public static function endAfterDate(PeriodInterface $period, \DateTime $refDate, $includeRefDate = false)
    {
        if (null === $period->getEndDate()
            or ($refDate < $period->getEndDate() or ($includeRefDate and $refDate == $period->getEndDate()))) {
            return true;
        }

        return false;
    }

    /**
     * Does a period contain a date ?
     *
     * @param PeriodInterface $period
     * @param \DateTime $refDate
     * @param bool $includeRefDateStart
     * @param bool $includeRefDateEnd
     *
     * @return bool
     */
    public static function containsDate(PeriodInterface $period, \DateTime $refDate, $includeRefDateStart = true, $includeRefDateEnd = false)
    {
        return (self::beginBeforeDate($period, $refDate, $includeRefDateStart)
                and self::endAfterDate($period, $refDate, $includeRefDateEnd));
    }
    
    
    public static function beginBefore(PeriodInterface $period, PeriodInterface $refPeriod)
    {
        if (($period->getStartDate() < $refPeriod->getStartDate()) or ($period->getStartDate() == $refPeriod->getStartDate() and $period->getEndDate() < $refPeriod->getEndDate())) {
            return true;
        }
            
        return false;
    }

    // rétrocompatibilité
    public static function isPrevious($period, $refPeriod)
    {
        return self::beginBefore($period, $refPeriod);
    }

    public static function beginAfter(PeriodInterface $period, PeriodInterface $refPeriod)
    {
        if (($period->getStartDate() > $refPeriod->getStartDate())
            or ($period->getStartDate() == $refPeriod->getStartDate() and $period->getEndDate() > $refPeriod->getEndDate())) {
            return true;
        }

        return false;
    }

    // rétrocompatibilité
    public function isNext($period, $refPeriod)
    {
        return self::beginAfter($period, $refPeriod);
    }
    
    
    public function getPreviousPeriods (PeriodInterface $refPeriod, array $periodsList, $returnPeriodsList = false)
    {
        $previousPeriods = array();
        
        foreach ($periodsList as $key => $period)
        {
            if (self::isPrevious($period, $refPeriod))
            {
                $previousPeriods[] = $period;
                unset ($periodsList[$key]);
            }
        }        
        
        if ($returnPeriodsList)
            return array($previousPeriods, $periodsList);
        else
            return $previousPeriods;
    }


    public static function containsPeriod(PeriodInterface $period, $startDate, $endDate)
    {
        /* if (($startDate < $period->getStartDate() and $endDate > $period->getStartDate()) */
        /*     or ($startDate < $period->getEndDate() and $endDate > $period->getEndDate()) */
        /*     or  */
        if ($startDate >= $period->getStartDate() and $endDate <= $period->getEndDate()) {
            return true;
        }

        return false;
    }


    public static function isSamePeriod(PeriodInterface $period1, PeriodInterface $period2)
    {
        if ($period1->getStartDate() == $period2->getStartDate() and $period1->getEndDate() == $period2->getEndDate()) {
            return true;
        }

        return false;
    }


    public static function isSubPeriod(PeriodInterface $period, PeriodInterface $refPeriod)
    {
        if (($period->getStartDate() >= $refPeriod->getStartDate() or null == $refPeriod->getStartDate()) and ($period->getEndDate() <= $refPeriod->getEndDate() or null == $refPeriod->getEndDate())) {
            return true;
        }

        return false;
    }

    public static function sortPeriods(array $periods, $order = "asc")
    {
        if ($order == "asc") {
            usort ($periods, function ($period1, $period2) {return self::beginBefore($period1, $period2) ? -1 : 1; });
        }
        elseif ($order == "desc") {
            usort ($periods, function ($period1, $period2) {return self::beginBefore($period1, $period2) ? 1 : -1; });
        }
    }


    public static function overlaps (PeriodInterface $period, PeriodInterface $refPeriod, $includeLimits = true) {
        if (self::isSamePeriod($period, $refPeriod)) {
            return true;
        }
        elseif (self::beginBefore($period, $refPeriod)
                and (($includeLimits and $period->getEndDate() >= $refPeriod->getStartDate())
                     or (! $includeLimits and $period->getEndDate() > $refPeriod->getStartDate())
                     or null == $period->getEndDate())) {
            return true;
        }
        elseif (self::beginAfter($period, $refPeriod)
                and (($includeLimits and $period->getStartDate() <= $refPeriod->getEndDate())
                     or (! $includeLimits and $period->getStartDate() < $refPeriod->getEndDate())
                     or null == $refPeriod->getEndDate())) {
            return true;
        }

        return false;
    }

    public static function manyPeriodsOverlap(array $periods, $includeLimits = true)
    {
        for ($key1 = 0; $key1 < (count($periods) -1); $key1 ++) {
            for ($key2 = $key1 + 1; $key2 < count($periods); $key2 ++) {
                if (self::overlaps($periods[$key1], $periods[$key2], $includeLimits)) {
                    return true;
                }
            }
        }

        return false;
    }


    public static function formContinuousPeriod(array $periods)
    {
        self::sortPeriods($periods);
        $previousPeriod = null;

        foreach ($periods as $period) {
            if (null != $previousPeriod) {
                if (! self::overlaps($period, $previousPeriod)) {
                    return false;
                }
            }

            $previousPeriod = $period;
        }

        return true;
    }


    public static function getPeriodsCoverage(array $periods)
    {
        $limitDates = array();

        foreach ($periods as $period) {
            if (! in_array ($period->getStartDate(), $limitDates)) {
                $limitDates[] = $period->getStartDate();
            }
            if (! in_array ($period->getEndDate(), $limitDates)) {
                $limitDates[] = $period->getEndDate();
            }
        }
        
        usort ($limitDates, function ($a, $b){ return ($a < $b) ? -1 : 1; });
        
        $periodsCoverage = array();

        foreach ($limitDates as $key => $limitDate) {
            if (isset ($limitDates[$key + 1])) {
                $periodCoverage = array('startDate' => $limitDate,
                                        'endDate' => $limitDates[$key + 1],
                                        'periodNumber' => 0);
                
                foreach ($periods as $period) {
                    if (self::containsPeriod ($period, $limitDate, $limitDates[$key + 1])) {
                        $periodCoverage['periodNumber'] ++;
                    }
                }

                $periodsCoverage[] = $periodCoverage;
            }
        }

        return $periodsCoverage;
    }


    public static function getMaxPeriodCoverage(array $periods)
    {
        $maxPeriodCoverage = 0;
        
        foreach ($periodsCoverage = self::getPeriodsCoverage($periods) as $periodCoverage) {
            if ($periodCoverage['periodNumber'] > $maxPeriodCoverage) {
                $maxPeriodCoverage = $periodCoverage['periodNumber'];
            }
        }

        return $maxPeriodCoverage;
    }


    public static function getNarrowestPeriod(array $periods)
    {
        $narrowestPeriod = $periods[0];

        foreach ($periods as $period) {
            if (self::isSubPeriod($period, $narrowestPeriod)) {
                $narrowestPeriod = $period;
            }
        }

        return $narrowestPeriod;
    }
}
