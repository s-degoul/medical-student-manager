<?php

namespace Gesseh\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * HospitalType
 */
class HospitalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                      'required' => true,
                      'label' => "Name"
                  ))
            ->add('phone', 'text', array(
                      'required' => false,
                      'label' => "Phone number"
                  ))
            ->add('street', 'text', array(
                      'required' => false,
                      'label' => "Street (& number)"
                  ))
            ->add('postcode', 'text', array(
                      'required' => false,
                      'label' => "Postcode"
                  ))
            ->add('city', 'text', array(
                      'required' => false,
                      'label' => "City"
                  ))
            ->add('country', 'country', array(
                      'required' => false,
                      'label' => "Country"
                  ))
            ->add('website', 'text', array(
                      'required' => false,
                      'label' => "Website"
                  ))
            ->add('description', 'textarea', array(
                      'required' => false,
                      'label' => "Description"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ))
             ->add('reset', 'reset', array(
                      'label' => "Reset"
                  ));
    }

    public function getName()
    {
        return 'hospital';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CoreBundle\Entity\Hospital',
            ));
    }
}
