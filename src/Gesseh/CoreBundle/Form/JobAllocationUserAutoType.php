<?php

namespace Gesseh\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class JobAllocationUserAutoType extends AbstractType
{
    private $jobs;

    public function __construct($jobs)
    {
        $this->jobs = $jobs;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('user')
            ->add('remove', 'submit', array(
                      'label' => "Delete"
                  ))
            ->add('saveAndAdd', 'submit', array(
                      'label' => "Add"
                  ))
            ->add('cancel', 'submit', array(
                      'label' => "Cancel"
                  ));
    }

    public function getParent()
    {
        return new JobAllocationType($this->jobs);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'job_allocation_user_auto';
    }
}