<?php

namespace Gesseh\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * JobAllocationPriorityType
 */
class JobAllocationPriorityType extends AbstractType
{
    private $users;
    
    public function __construct(array $users)
    {
        $this->users = $users;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', 'entity', array(
                      'class' => 'GessehUserBundle:User',
                      'choices' => $this->users,
                      'property' => 'name',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'label' => "User"
                  ))
            ->add('rank', 'integer', array(
                      'required' => true,
                      'precision' => 0,
                      'label' => "Rank"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ));
    }

    public function getName()
    {
        return 'job_allocation_priority';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CoreBundle\Entity\JobAllocationPriority',
            ));
    }
}
