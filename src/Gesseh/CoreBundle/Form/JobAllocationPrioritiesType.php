<?php

namespace Gesseh\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * JobAllocationPrioritiesType
 */
class JobAllocationPrioritiesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('priorities', 'collection', array(
                      'type' => new JobAllocationPriorityType(),
                      'allow_add' => true,
                      'allow_delete' => true,
                      'by_reference' => false,
                      'mapped' => false
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ));
    }

    public function getName()
    {
        return 'job_allocation_priorities';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array()
        );
    }
}
