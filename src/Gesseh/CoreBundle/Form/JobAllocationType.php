<?php

namespace Gesseh\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class JobAllocationType extends AbstractType
{
    private $jobs;

    public function __construct($jobs)
    {
        $this->jobs = $jobs;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', 'date', array(
                      'required' => false,
                      'label' => "Start date",
                      'widget' => 'single_text',
                      'format' => 'dd/MM/yyyy'
                  ))
            ->add('endDate', 'date', array(
                      'required' => false,
                      'label' => "End date",
                      'widget' => 'single_text',
                      'format' => 'dd/MM/yyyy'
                  ))
            ->add('user', 'entity', array(
                      'class' => 'GessehUserBundle:User',
                      'property' => 'name',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'label' => "User"
                  ))
            ->add('job', 'entity', array(
                      'class' => 'GessehCoreBundle:Job',
                      'choices' => $this->jobs,
                      'property' => 'name',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'label' => "Job"
                  ))
            /* ->add('confirm_exception', 'checkbox', array( */
            /*           'mapped' => false, */
            /*           'required' => false, */
            /*           'label' => 'force' */
            /*       )) */
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ));

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                if (! $form->isValid()) {
                    $form->add('confirm_exception', 'checkbox', array(
                                   'mapped' => false,
                                   'required' => false,
                                   'label' => 'force'
                               ));
                }
            });
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CoreBundle\Entity\JobAllocation',
                'validation_groups' => function (FormInterface $form) {

                    if ($form->get('confirm_exception')->getData() == true) {
                        return array('mandatory');
                    }
                    
                    return array('base');
                }
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'job_allocation';
    }
}