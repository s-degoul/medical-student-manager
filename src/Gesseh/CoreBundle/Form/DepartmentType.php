<?php

namespace Gesseh\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * DepartmentType
 */
class DepartmentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                      'required' => true,
                      'label' => "Name"
                  ))
            ->add('head', 'text', array(
                      'required' => false,
                      'label' => "Head"
                  ))
            ->add('email', 'email', array(
                      'required' => false,
                      'label' => "Email"
                  ))
            ->add('phone', 'text', array(
                      'required' => false,
                      'label' => "Phone number"
                  ))
            ->add('description', 'textarea', array(
                      'required' => false,
                      'label' => "Description"
                  ))
            ->add('hospital', 'entity', array(
                      'class' => 'GessehCoreBundle:Hospital',
                      'property' => 'name',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'label' => "Hospital"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ))
            ->add('reset', 'reset', array(
                      'label' => "Reset"
                  ));
    }

    public function getName()
    {
        return 'department';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CoreBundle\Entity\Department',
            ));
    }
}
