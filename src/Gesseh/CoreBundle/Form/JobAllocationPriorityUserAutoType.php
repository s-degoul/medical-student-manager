<?php

namespace Gesseh\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * JobAllocationPriorityUserAutoType
 */
class JobAllocationPriorityUserAutoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('user');
    }

    public function getParent()
    {
        return new JobAllocationPriorityType(array());
    }

    public function getName()
    {
        return 'job_allocation_priority_user_auto';
    }
}
