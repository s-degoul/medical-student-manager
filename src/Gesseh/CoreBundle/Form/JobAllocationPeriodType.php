<?php

namespace Gesseh\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;


/**
 * JobAllocationPeriodType
 */
class JobAllocationPeriodType extends AbstractType
{
    private $roleChecker;

    public function __construct(RoleChecker $roleChecker)
    {
        $this->roleChecker = $roleChecker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', 'date', array(
                      'required' => false,
                      'label' => "Start date",
                      'widget' => 'single_text',
                      'format' => 'dd/MM/yyyy'
                  ))
            ->add('endDate', 'date', array(
                      'required' => false,
                      'label' => "End date",
                      'widget' => 'single_text',
                      'format' => 'dd/MM/yyyy'
                  ))
            ->add('groups', 'entity', array(
                      'class' => 'GessehUserBundle:Group',
                      'choices' => $this->roleChecker->getReachableObjects('ROLE_ADMIN_JOB', 'Group'),
                      'property' => 'completeTitle',
                      'multiple' => true,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'label' => "Groups"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ))
            ->add('reset', 'reset', array(
                      'label' => "Reset"
                  ));
    }

    public function getName()
    {
        return 'job_allocation_period';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CoreBundle\Entity\JobAllocationPeriod',
            ));
    }
}
