<?php

namespace Gesseh\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class JobAllocationJobAutoType extends AbstractType
{
    private $job;

    public function __construct($job)
    {
        $this->job = $job;
    }

    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('job');
    }

    public function getParent()
    {
        return new JobAllocationType(array($this->job));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'job_allocation_job_auto';
    }
}