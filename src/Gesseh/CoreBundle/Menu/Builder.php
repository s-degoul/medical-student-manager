<?php

namespace Gesseh\CoreBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\ExpressionLanguage\Expression;


class Builder
{
    private $factory;
    private $authorizationChecker;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->factory = $factory;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createMainMenu()//array $options
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');


        /* submenu "home" */
        $menu->addChild('home', array(
                            'route' => 'GHome_Presentation',
                            'label' => " Welcome"
                        ));
        
        $menu['home']->setLinkAttribute('class', 'glyphicon glyphicon-home');
        

        if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            
            /* submenu "core" (jobs) */
            $menu->addChild('core', array(
                                'label' => " Jobs"
                            ));
            
            $menu['core']->addChild('jobList', array(
                                        'route' => 'GCore_ListJobSites',
                                        'label' => "List of job sites"
                                    ));

            if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_JOB','ROLE_ADMIN_JOB_ALLOCATION')"))) {
                $menu['core']->addChild('separator', array(
                                            'label' => ''
                                        ));
                $menu['core']['separator']->setAttribute('class', 'divider');
            }

            if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_JOB')"))) {
                $menu['core']->addChild('jobSitesManagement', array(
                                            'route' => 'GCore_AdminListJobSites',
                                            'label' => "Manage job sites"
                                        ));
                $menu['core']->addChild('sectorsManagement', array(
                                            'route' => 'GCore_AdminListSectors',
                                            'label' => "Manage sectors"
                                        ));
            }
            if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_JOB_ALLOCATION')"))) {
                $menu['core']->addChild('jobAllocationManagement', array(
                                            'route' => 'GCore_AdminJobAllocationPeriods',
                                            'label' => "Manage job allocations"
                                        ));
            }
            
            $menu['core']->setAttribute('class', 'dropdown');
            $menu['core']->setLabelAttribute('class', 'glyphicon glyphicon-blackboard dropdown-toggle');
            $menu['core']->setChildrenAttribute('class', 'dropdown-menu');


            
            /* submenu "user" */
            $menu->addChild('user', array(
                                'label' => " Users"
                            ));

            $menu['user']->addChild('usersList', array(
                                        'route' => 'GUser_ListUsers',
                                        'label' => "List of users"
                                    ));

            if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_USER','ROLE_ADMIN_PRIORITY')"))) {
                $menu['user']->addChild('separator', array(
                                            'label' => ''
                                        ));
                $menu['user']['separator']->setAttribute('class', 'divider');
            }

            if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_USER')"))) {
                $menu['user']->addChild('userManagement', array(
                                            'route' => 'GUser_AdminListUsers',
                                            'label' => "Manage users"
                                        ));
                $menu['user']->addChild('groupsManagement', array(
                                            'route' => 'GUser_AdminListGroups',
                                            'label' => "Manage groups"
                                        ));
            }
            if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_PRIORITY')"))) {
                $menu['user']->addChild('prioritiesManagement', array(
                                            'route' => 'GUser_AdminChoosePriorityType',
                                            'label' => "Manage priorities"
                                        ));
            }

            $menu['user']->setAttribute('class', 'dropdown');
            $menu['user']->setLabelAttribute('class', 'glyphicon glyphicon-user dropdown-toggle');
            $menu['user']->setChildrenAttribute('class', 'dropdown-menu');

        }
        

        
        /* submenu "courses" */
        
        if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $menu->addChild('course', array(
                                'label' => " Courses"
                            ));
                    
            $menu['course']->addChild('coursesList', array(
                                          'route' => 'GCourse_ListCourses',
                                          'label' => "List of courses"
                                      ));
         
            $menu['course']->addChild('newCourse', array(
                                          'route' => 'GCourse_NewCourse',
                                          'label' => "Add a course"
                                      ));

            if (! $this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_AUTHOR')"))) {
                $menu['course']->addChild('manageAuthors', array(
                                              'route' => 'GCourse_AdminManageAuthor',
                                              'label' => "My author informations"
                                          ));
            }

            if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_LICENCE','ROLE_ADMIN_TAGS','ROLE_ADMIN_AUTHOR')"))) {
                $menu['course']->addChild('separator', array(
                                            'label' => ''
                                        ));
                $menu['course']['separator']->setAttribute('class', 'divider');
            }
            
            if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_LICENCE')"))) {
                $menu['course']->addChild('manageLicences', array(
                                              'route' => 'GCourse_AdminManageLicence',
                                              'label' => "Manage licences"
                                          ));
            }

            if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_TAG')"))) {
                $menu['course']->addChild('manageTags', array(
                                              'route' => 'GCourse_AdminManageTag',
                                              'label' => "Manage tags"
                                          ));
            }

            if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_AUTHOR')"))) {
                $menu['course']->addChild('manageAuthors', array(
                                              'route' => 'GCourse_AdminManageAuthor',
                                              'label' => "Manage authors"
                                          ));
            }

            $menu['course']->setAttribute('class', 'dropdown');
            $menu['course']->setLabelAttribute('class', 'glyphicon glyphicon-education');
            $menu['course']->setChildrenAttribute('class', 'dropdown-menu');
        }
        else {
            $menu->addChild('course', array(
                                'route' => 'GCourse_ListCourses',
                                'label' => " Courses"
                            ));
            $menu['course']->setLinkAttribute('class', 'glyphicon glyphicon-education');
        }
        

        /* submenu "contact" */

        $menu->addChild('contact', array(
                            'route' => 'GHome_Contact',
                            'label' => " Contact"
                        ));
        $menu['contact']->setLinkAttribute('class', 'glyphicon glyphicon-envelope');

        return $menu;
    }

    /* public function createUserMenu(array $options) */
    /* { */
    /*     $menu = $this->factory->createItem('root'); */
    /*     $menu->setChildrenAttribute('class', 'nav nav-pills nav-stacked'); */

    /*     $menu->addChild('List of users', array( */
    /*                         'route' => 'GUser_ListUsers' */
    /*                     )); */

    /*     if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_USER')"))) { */
    /*         $menu->addChild('Manage users', array( */
    /*                             'route' => 'GUser_AdminListUsers' */
    /*                         )); */
    /*         $menu->addChild('Manage groups', array( */
    /*                             'route' => 'GUser_AdminListGroups' */
    /*                         )); */
    /*     } */
    /*     if ($this->authorizationChecker->isGranted(new Expression("has_role('ROLE_ADMIN_PRIORITY')"))) { */
    /*         $menu->addChild('Manage priorities', array( */
    /*                             'route' => 'GUser_AdminChoosePriorityType' */
    /*                         )); */
    /*     } */
        
    /*     return $menu; */
    /* } */
}