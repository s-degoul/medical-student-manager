<?php

namespace Gesseh\CoreBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PeriodDates extends Constraint
{
    public $message = "The end date of the period must be posterior or equal than start date";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return 'period_dates';
    }
}