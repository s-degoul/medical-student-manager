<?php

namespace Gesseh\CoreBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class EndAfterStartPeriodValidator extends ConstraintValidator
{    
    public function validate($period, Constraint $constraint)
    {
        if (null != $period->getStartDate() and null != $period->getEndDate()
            and $period->getEndDate() < $period->getStartDate()) {
            $this->context->addViolation($constraint->message, array());
        }
    }
}
