<?php

namespace Gesseh\CoreBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
/* use Doctrine\Common\Persistence\ObjectManager; */
use Gesseh\CoreBundle\Service\PeriodHandler\JobAllocationPeriodHandler;


class NotTooManyJobAllocationsValidator extends ConstraintValidator
{
    /* private $om; */
    private $jobAllocationPeriodHandler;
    
    public function __construct(JobAllocationPeriodHandler $jobAllocationPeriodHandler)
    {
        $this->jobAllocationPeriodHandler = $jobAllocationPeriodHandler;
    }
    
    
    public function validate($jobAllocation, Constraint $constraint)
    {
        $job = $jobAllocation->getJob();
        
        if (null == $job) {
            return;
        }
        
        $jobAllocations = $this->jobAllocationPeriodHandler->getJobAllocationsByJob($job);

        if (! in_array($jobAllocation, $jobAllocations)) {
            $jobAllocations[] = $jobAllocation;
        }

        if ($this->jobAllocationPeriodHandler->getMaxPeriodCoverage($jobAllocations) > $job->getMaxAllocationsNumber()) {
            $this->context->addViolation($constraint->message, array());
        }
    }
}
