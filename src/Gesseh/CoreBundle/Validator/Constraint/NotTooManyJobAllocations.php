<?php

namespace Gesseh\CoreBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotTooManyJobAllocations extends Constraint
{
    public $message = "The job has to many users allocations";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
    public function validatedBy()
    {
        return 'not_too_many_job_allocations';
    }
}
