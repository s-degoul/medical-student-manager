<?php

namespace Gesseh\CoreBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EndAfterStartPeriod extends Constraint
{
    public $message = "The period must end after it starts";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
    public function validatedBy()
    {
        return 'end_after_start_period';
    }
}
