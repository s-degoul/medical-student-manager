<?php

namespace Gesseh\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class SameGroupAllocationsNotOverlap extends Constraint
{
    public $message = "Groups allocations concerning the same group must not overlap";

    public function validatedBy()
    {
        return 'same_group_allocations_not_overlap';
    }
}