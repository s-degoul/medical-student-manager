<?php

namespace Gesseh\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class GroupsNumberNotNull extends Constraint
{
    public $message = "The number of groups have to be greater than zero";
    
    public function validatedBy()
    {
        return 'groups_number_not_null';
    }
}
