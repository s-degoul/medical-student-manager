<?php

namespace Gesseh\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class GroupAllocationsOverlap extends Constraint
{
    public $message = "Groups allocations period must be continuous";

    public function validatedBy()
    {
        return 'group_allocations_overlap';
    }
}