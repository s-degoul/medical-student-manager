<?php

namespace Gesseh\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;
use Gesseh\UserBundle\Service\GroupsHierarchy\GroupsHierarchy;

class GroupRecursiveHierarchyValidator extends ConstraintValidator
{
    /* private $em; */
    private $groupsHierarchy;
    
    public function __construct(GroupsHierarchy $groupsHierarchy)//EntityManager $em, 
    {
        /* $this->em = $em; */
        $this->groupsHierarchy = $groupsHierarchy;
    }
    
    public function validate($group, Constraint $constraint)
    {
        if (null != $group->getParent()) {
            if (in_array($group->getParent(), $this->groupsHierarchy->getReachableGroups(array($group)))) {
                $this->context->addViolation($constraint->message, array());
            }
        }
    }
}
