<?php

namespace Gesseh\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class AllowedToAllocateGroup extends Constraint
{
    public $message = "You cannot assign group(s) that you are not allowed to manage : %nonAdminGroups%";

    
    public function validatedBy()
    {
        return 'allowed_allocate_group';
    }
}
