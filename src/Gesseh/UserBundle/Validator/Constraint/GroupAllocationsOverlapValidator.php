<?php

namespace Gesseh\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Gesseh\CoreBundle\Service\PeriodHandler\PeriodHandler;

class GroupAllocationsOverlapValidator extends ConstraintValidator
{
    private $periodHandler;

    public function __construct(PeriodHandler $periodHandler)
    {
        $this->periodHandler = $periodHandler;
    }
    
    public function validate($groupAllocations, Constraint $constraint)
    {
        if (! $this->periodHandler->formContinuousPeriod($groupAllocations->toArray())) {
            $this->context->addViolation($constraint->message, array());
        }
    }
}
