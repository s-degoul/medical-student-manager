<?php

namespace Gesseh\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;
use Gesseh\UserBundle\Entity\Group;

class AllowedToAllocateGroupValidator extends ConstraintValidator
{
    private $roleChecker;
    
    public function __construct(RoleChecker $roleChecker)
    {
        $this->roleChecker = $roleChecker;
    }
    
    public function validate($groups, Constraint $constraint)
    {
        $nonAdminGroups = array();

        if (is_array ($groups)) {
            foreach ($groups as $groupAllocation) {
                $group = $groupAllocation->getGroup();
            
                if (! $this->roleChecker->hasRole('ROLE_ADMIN_USER', $group)) {
                    $nonAdminGroups[] = $group->__toString();
                }
            }
        }
        elseif ($groups instanceof Group) {
            if (! $this->roleChecker->hasRole('ROLE_ADMIN_USER', $groups)) {
                $nonAdminGroups[] = $groups->__toString();
            }
        }

        if (!empty ($nonAdminGroups)) {
            $this->context->addViolation($constraint->message, array('%nonAdminGroups%' => implode(', ', $nonAdminGroups)));
        }
    }
}
