<?php

namespace Gesseh\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class GroupRecursiveHierarchy extends Constraint
{
    public $message = "This group hierarchy is not logical";
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
    public function validatedBy()
    {
        return 'group_recursive_hierarchy';
    }
}
