<?php

namespace Gesseh\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Gesseh\CoreBundle\Service\PeriodHandler\PeriodHandler;

class SameGroupAllocationsNotOverlapValidator extends ConstraintValidator
{
    private $periodHandler;

    public function __construct(PeriodHandler $periodHandler)
    {
        $this->periodHandler = $periodHandler;
    }
    
    public function validate($groupAllocations, Constraint $constraint)
    {
        $groups = array();
        foreach ($groupAllocations as $groupAllocation) {
            if (! in_array($groupAllocation->getGroup(), $groups)) {
                $groups[] = $groupAllocation->getGroup();
            }
        }

        $overlapedGroups = array();
        
        foreach ($groups as $group) {
            $concernedGroupAllocations = array();
            foreach ($groupAllocations as $groupAllocation) {
                if ($groupAllocation->getGroup() == $group) {
                    $concernedGroupAllocations[] = $groupAllocation;
                }
            }

            if ($this->periodHandler->manyPeriodsOverlap($concernedGroupAllocations, false)) {
                $overlapedGroups[] = $group;
            }
        }

        
        if (count ($overlapedGroups) > 0) {
            $this->context->addViolation($constraint->message, array());
        }
    }
}
