<?php

namespace Gesseh\UserBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Gesseh\CoreBundle\Service\PeriodHandler\PeriodHandler;

class GroupsNumberNotNullValidator extends ConstraintValidator
{
    /* private $PeriodHandler; */

    /* public function __construct(PeriodHandler $periodHandler) */
    /* { */
    /*     $this->periodHandler = $periodHandler; */
    /* } */
    
    public function validate($groups, Constraint $constraint)
    {
        /* if ($groups instanceof ArrayCollection and count ($groups) == 0) { */
        if (count ($groups) < 1) {
            $this->context->addViolation($constraint->message, array());
        }
        /* else { */
        /*     $actualGroups = false; */
            
        /*     foreach ($groups as $group) { */
        /*         if ($this->periodHandler->containsPeriod($group, new \DateTime(), new \DateTime())) { */
        /*             $actualGroups = true; */
        /*         } */
        /*     } */

        /*     if (! $actualGroups) { */
        /*         $this->context->addViolation($constraint->message, array()); */
        /*     } */
        /* } */
    }
}
