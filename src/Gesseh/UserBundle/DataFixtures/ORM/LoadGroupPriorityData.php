<?php

namespace Gesseh\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Gesseh\UserBundle\Entity\GroupPriority;


class LoadGroupPriorityData extends AbstractFixture implements OrderedFixtureInterface,ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $groupPriorities = array(
            array(
                'group' => 'anesth_S1',
                'priority' => 3,
                'type' => 'jobAllocation',
                'startDate' => null,
                'endDate' => null
            ),
            array(
                'group' => 'anesth_S2',
                'priority' => 2,
                'type' => 'jobAllocation',
                'startDate' => null,
                'endDate' => null
            ),
            array(
                'group' => 'anesth_S3',
                'priority' => 1,
                'type' => 'jobAllocation',
                'startDate' => null,
                'endDate' => null
            ),
            array(
                'group' => 'anesth_dispo',
                'priority' => -1,
                'type' => 'jobAllocation',
                'startDate' => null,
                'endDate' => null
            ),
            array(
                'group' => 'cardio_S1',
                'priority' => 2,
                'type' => 'jobAllocation',
                'startDate' => null,
                'endDate' => null
            ),
            array(
                'group' => 'cardio_S2',
                'priority' => 1,
                'type' => 'jobAllocation',
                'startDate' => null,
                'endDate' => null
            ),
        );


        foreach ($groupPriorities as $groupPriority) {
            $newGroupPriority = new GroupPriority;

            $newGroupPriority->setGroup($this->getReference('group_'.$groupPriority['group']));
            $newGroupPriority->setPriority($groupPriority['priority']);
            $newGroupPriority->setType($groupPriority['type']);
            $newGroupPriority->setStartDate($groupPriority['startDate']);
            $newGroupPriority->setEndDate($groupPriority['endDate']);

            $manager->persist($newGroupPriority);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}