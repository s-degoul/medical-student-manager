<?php

namespace Gesseh\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Gesseh\UserBundle\Entity\RoleApplication;


class LoadRoleApplicationData extends AbstractFixture implements OrderedFixtureInterface,ContainerAwareInterface
{
    private $container;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        $roleApplications = array (
            array(
                'user' => 'admin',
                'role' => 'ROLE_ADMIN',
                'objectType' => 'Group',
                'objectId' => 'base_group'
            ),
            array(
                'user' => 'admin',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'base_group'
            ),
            array(
                'user' => 'user0',
                'role' => 'ROLE_ADMIN',
                'objectType' => 'Group',
                'objectId' => 'DES_anesth'
            ),
            array(
                'user' => 'user0',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DES_anesth'
            ),
            array(
                'user' => 'user0',
                'role' => 'ROLE_USER_DISPLAY_USER',
                'objectType' => 'Group',
                'objectId' => 'DESC'
            ),
            array(
                'user' => 'user0',
                'role' => 'ROLE_ADMIN_JOBSITE',
                'objectType' => 'Group',
                'objectId' => 'DES_anesth'
            ),
            array(
                'user' => 'user1',
                'role' => 'ROLE_ADMIN_COURSE',
                'objectType' => 'Group',
                'objectId' => 'DES_cardio'
            ),
            array (
                'user' => 'user1',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DES_cardio'
            ),
            array (
                'user' => 'user2',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DES_cardio'
            ),
            array (
                'user' => 'user3',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DES_anesth'
            ),
            array (
                'user' => 'user4',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DES_anesth'
            ),
            array (
                'user' => 'user5',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DESC_rea_med'
            ),
            array (
                'user' => 'user6',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DES_anesth'
            ),
            array (
                'user' => 'user7',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DES_anesth'
            ),
            array (
                'user' => 'user8',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DES_cardio'
            ),
            array (
                'user' => 'user9',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DES_anesth'
            ),
            array (
                'user' => 'user10',
                'role' => 'ROLE_USER',
                'objectType' => 'Group',
                'objectId' => 'DESC_rea_med'
            ),
        );

        $additionalUsers = $this->container->get('gesseh_converter.csv_array')->convert($this->container->get('kernel')->getRootDir().'/../utilitaires/new_users.csv');

        foreach ($additionalUsers as $additionalUser) {
            $roles = explode('|', $additionalUser['roles_groups']);
            foreach ($roles as $role) {
                $ra = explode(',', $role);
                $roleApplications[] = array(
                    'user' => $additionalUser['username'],
                    'role' => $ra[0],
                    'objectType' => 'Group',
                    'objectId' => $ra[1]
                );
            }
        }
        
        $listRoleApplications = array();
        foreach ($roleApplications as $i => $roleApplication)
        {
            $listRoleApplications[$i] = new RoleApplication();
            $listRoleApplications[$i]->setUser($this->getReference('user_'.$roleApplication['user']));
            $listRoleApplications[$i]->setRole($this->getReference('role_'.$roleApplication['role']));

            if (isset ($roleApplication['objectType']) and !is_null ($roleApplication['objectType'])) {
                $listRoleApplications[$i]->setObjectType($roleApplication['objectType']);
            }
            else {
                $listRoleApplications[$i]->setObjectType(null);
            }

            if (isset ($roleApplication['objectId']) and !is_null ($roleApplication['objectId'])) {
                $listRoleApplications[$i]->setObjectId($this->getReference(strtolower($roleApplication['objectType']).'_'.$roleApplication['objectId'])->getId());
            }
            else {
                $listRoleApplications[$i]->setObjectId(null);
            }
            
            $manager->persist($listRoleApplications[$i]);
        }
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 4;
    }
}

