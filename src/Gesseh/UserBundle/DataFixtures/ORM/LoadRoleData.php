<?php

namespace Gesseh\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gesseh\UserBundle\Entity\Role;


class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $roles = array (
            array(
                'name' => 'ROLE_ADMIN',
                'title' => "all administration",
                'parent' => null
            ),            
            array(
                'name' => 'ROLE_ADMIN_USER',
                'title' => "administration of users",
                'parent' => 'ROLE_ADMIN'
            ),
            array(
                'name' => 'ROLE_ADMIN_DISPLAY_USER',
                'title' => "show user profile",
                'parent' => 'ROLE_ADMIN_USER'
            ),
            array(
                'name' => 'ROLE_ADMIN_PRIORITY',
                'title' => "administration of priorities",
                'parent' => 'ROLE_ADMIN'
            ),
            array(
                'name' => 'ROLE_ADMIN_DUTY',
                'title' => "duties' administration",
                'parent' => 'ROLE_ADMIN'
            ),
            array(
                'name' => 'ROLE_ADMIN_JOBSITE',
                'title' => "job sites's administration",
                'parent' => 'ROLE_ADMIN'
            ),
            array(
                'name' => 'ROLE_ADMIN_HOSPITAL',
                'title' => "hospital's administration",
                'parent' => 'ROLE_ADMIN_JOBSITE'
            ),
            array(
                'name' => 'ROLE_ADMIN_DEPARTMENT',
                'title' => "department's administration",
                'parent' => 'ROLE_ADMIN_HOSPITAL'
            ),
            array(
                'name' => 'ROLE_ADMIN_JOB',
                'title' => "job's administration",
                'parent' => 'ROLE_ADMIN_DEPARTMENT'
            ),
            array(
                'name' => 'ROLE_ADMIN_COURSE',
                'title' => "course's administration",
                'parent' => 'ROLE_ADMIN'
            ),
            array(
                'name' => 'ROLE_ADMIN_JOB_ALLOCATION',
                'title' => "job allocations administration",
                'parent' => 'ROLE_ADMIN'
            ),
            array(
                'name' => 'ROLE_ADMIN_LICENCE',
                'title' => "licence's management",
                'parent' => 'ROLE_ADMIN'
            ),
            array(
                'name' => 'ROLE_ADMIN_TAG',
                'title' => "tag's management",
                'parent' => 'ROLE_ADMIN'
            ),
            array(
                'name' => 'ROLE_ADMIN_AUTHOR',
                'title' => "author's management",
                'parent' => 'ROLE_ADMIN'
            ),
            
            array(
                'name' => 'ROLE_USER',
                'title' => "user",
                'parent' => null
            ),
            array(
                'name' => 'ROLE_USER_DISPLAY_USER',
                'title' => "show users list",
                'parent' => 'ROLE_USER'
            ),
            array(
                'name'=> 'ROLE_USER_UPDATE_PROFILE',
                'title' => "update self profile",
                'parent' => 'ROLE_USER'
            ),
            array(
                'name' => 'ROLE_USER_SHOW_PROFILE',
                'title' => "show self profile",
                'parent' => 'ROLE_USER_UPDATE_PROFILE'
            ),
            array(
                'name' => 'ROLE_USER_JOBSITE',
                'title' => "show jobsites",
                'parent' => 'ROLE_USER'
            ),
            array(
                'name' => 'ROLE_USER_JOB_ALLOCATION',
                'title' => "show job allocations",
                'parent' => 'ROLE_USER'
            ),
            array(
                'name' => 'ROLE_USER_DISPLAY_COURSE',
                'title' => "show courses",
                'parent' => 'ROLE_USER'
            ),
            array(
                'name' => 'ROLE_USER_MANAGE_COURSE',
                'title' => "manage courses",
                'parent' => 'ROLE_USER'
            ),
            array(
                'name' => 'ROLE_USER_ADD_COURSE',
                'title' => "add and manage self course",
                'parent' => 'ROLE_USER'
            ),
        );

        $listRoles = array();
        foreach ($roles as $i => $role)
        {
            $listRoles[$i] = new Role($role['name']);
            $listRoles[$i]->setTitle($role['title']);
            if (null != $role['parent']) {
                $listRoles[$i]->setParent($this->getReference('role_'.$role['parent']));
            }
            
            $manager->persist($listRoles[$i]);
            
            $this->addReference('role_'.$role['name'], $listRoles[$i]);
        }
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 1;
    }
}

