<?php

namespace Gesseh\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Gesseh\UserBundle\Entity\User;


class LoadUserData extends AbstractFixture implements OrderedFixtureInterface,ContainerAwareInterface
{
    private $container;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('gesseh_user.user_manager');
        
        $users = array (
            array(
                'username' => 'admin',
                'email' => 'admin@admin.com',
                'password' => "abcde",
                'firstname' => null,
                'surname' => "Admin"
            ),
            array(
                'username' => 'user0',
                'email' => 'user0@gmail.com',
                'password' => "abcde",
                'firstname' => "Samuel",
                'surname' => "Degoul"
            ),
            array(
                'username' => 'user1',
                'email' => 'user1@mail.org',
                'password' => "abcde",
                'firstname' => "Benoît",
                'surname' => "Dupond"
            ),
            array(
                'username' => 'user2',
                'email' => 'user2@mail.org',
                'password' => "abcde",
                'firstname' => "Marion",
                'surname' => "Durand"
            ),
            array(
                'username' => 'user3',
                'email' => 'user3@mail.org',
                'password' => "abcde",
                'firstname' => "Philippe",
                'surname' => "Grosjean"
            ),
            array(
                'username' => 'user4',
                'email' => 'user4@mail.org',
                'password' => "abcde",
                'firstname' => "Amandine",
                'surname' => "Dusart"
            ),
            array(
                'username' => 'user5',
                'email' => 'user5@mail.org',
                'password' => "abcde",
                'firstname' => "Aurélie",
                'surname' => "Bidulle"
            ),
            array(
                'username' => 'user6',
                'email' => 'user6@mail.org',
                'password' => "abcde",
                'firstname' => "Jules",
                'surname' => "Tintin"
            ),
            array(
                'username' => 'user7',
                'email' => 'user7@mail.org',
                'password' => "abcde",
                'firstname' => "Rémi",
                'surname' => "Mossa"
            ),
            array(
                'username' => 'user8',
                'email' => 'user8@mail.org',
                'password' => "abcde",
                'firstname' => "Laurel",
                'surname' => "Hardy"
            ),
            array(
                'username' => 'user9',
                'email' => 'user9@mail.org',
                'password' => "abcde",
                'firstname' => "Bernadette",
                'surname' => "Lefebvre"
            ),
            array(
                'username' => 'user10',
                'email' => 'user10@mail.org',
                'password' => "abcde",
                'firstname' => "Ginette",
                'surname' => "Nemo"
            ),
        );

        $additionalUsers = $this->container->get('gesseh_converter.csv_array')->convert($this->container->get('kernel')->getRootDir().'/../utilitaires/new_users.csv');

        $users = array_merge($users, $additionalUsers);

        $listUsers = array();
        
        foreach ($users as $i => $user)
        {
            $listUsers[$i] = new User();
            $listUsers[$i]->hydrate($user);

            if (!isset ($user['username']) or null == $user['username']) {
                $listUsers[$i]->setUsername($userManager->generateUsername($listUsers[$i]));
            }

            if (!isset ($user['password']) or null == $user['password']) {
                $password = substr($this->container->get('fos_user.util.token_generator')->generateToken(), 0, 12);
            }
            else {
                $password = $user['password'];
            }
            $listUsers[$i]->setPlainPassword($password);

            $listUsers[$i]->setEnabled(true);
            
            $userManager->updateUser($listUsers[$i], false);
            
            $this->addReference('user_'.$listUsers[$i]->getUsername(), $listUsers[$i]);
        }

        $userManager->flush();
    }
    
    public function getOrder()
    {
        return 2;
    }
}

