<?php

namespace Gesseh\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Gesseh\UserBundle\Entity\GroupAllocation;


class LoadGroupAllocationData extends AbstractFixture implements OrderedFixtureInterface,ContainerAwareInterface
{
    private $container;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('gesseh_user.user_manager');
        
        $groupAllocations = array (
            array(
                'user' => 'admin',
                'group' => 'cardio_S2',
                'startDate' => new \DateTime("2014-11-01"),
                'endDate' => null
            ),
            array(
                'user' => 'user1',
                'group' => 'DESC_rea_med',
                'startDate' => new \DateTime("2014-11-01"),
                'endDate' => null
            ),
            array(
                'user' => 'user3',
                'group' => 'DESC_rea_med',
                'startDate' => new \DateTime("2015-05-01"),
                'endDate' => null
            ),
            array(
                'user' => 'user10',
                'group' => 'DESC_rea_med',
                'startDate' => new \DateTime("2014-11-01"),
                'endDate' => new \DateTime("2015-04-30")
            ),
            array(
                'user' => 'user0',
                'group' => 'anesth_dispo',
                'startDate' => new \DateTime("2014-11-01"),
                'endDate' => new \DateTime("2015-04-30")
            ),
            array(
                'user' => 'user0',
                'group' => 'anesth_S3',
                'startDate' => new \DateTime("2015-05-01"),
                'endDate' => new \DateTime("2015-10-30")
            ),
            array(
                'user' => 'user0',
                'group' => 'anesth_dispo',
                'startDate' => new \DateTime("2015-11-01"),
                'endDate' => null
            ),
            array(
                'user' => 'user10',
                'group' => 'cardio_S2',
                'startDate' => new \DateTime("2014-11-01"),
                'endDate' => new \DateTime("2015-04-30")
            ),
            array(
                'user' => 'user1',
                'group' => 'cardio_S1',
                'startDate' => new \DateTime("2014-11-01"),
                'endDate' => new \DateTime("2015-04-30")
            ),
            array(
                'user' => 'user1',
                'group' => 'cardio_S2',
                'startDate' => new \DateTime("2015-05-01"),
                'endDate' => null
            ),
            array(
                'user' => 'user2',
                'group' => 'cardio_S2',
                'startDate' => new \DateTime("2014-11-01"),
                'endDate' => new \DateTime("2015-04-30")
            ),
            array(
                'user' => 'user2',
                'group' => 'cardio_dispo',
                'startDate' => new \DateTime("2015-05-01"),
                'endDate' => null
            ),
            array(
                'user' => 'user8',
                'group' => 'cardio_S2',
                'startDate' => new \DateTime("2014-11-01"),
                'endDate' => new \DateTime("2015-04-30")
            ),
            array(
                'user' => 'user3',
                'group' => 'anesth_S1',
                'startDate' => new \DateTime("2014-11-01"),
                'endDate' => new \DateTime("2015-04-30")
            ),
            array(
                'user' => 'user3',
                'group' => 'anesth_S2',
                'startDate' => new \DateTime("2015-05-01"),
                'endDate' => null
            ),
            array(
                'user' => 'user4',
                'group' => 'anesth_S3',
                'startDate' => new \DateTime("2015-05-03"),
                'endDate' => null
            ),
            array(
                'user' => 'user5',
                'group' => 'DESC_rea_med',
                'startDate' => new \DateTime("2014-09-01"),
                'endDate' => new \DateTime("2016-10-31")
            ),
            array(
                'user' => 'user6',
                'group' => 'anesth_S3',
                'startDate' => new \DateTime("2015-05-03"),
                'endDate' => null
            ),
            array(
                'user' => 'user7',
                'group' => 'anesth_S2',
                'startDate' => new \DateTime("2015-05-03"),
                'endDate' => new \DateTime("2015-10-31")
            ),
            array(
                'user' => 'user9',
                'group' => 'anesth_S1',
                'startDate' => new \DateTime("2015-05-03"),
                'endDate' => new \DateTime("2015-10-31")
            ),
        );

        $additionalUsers = $this->container->get('gesseh_converter.csv_array')->convert($this->container->get('kernel')->getRootDir().'/../utilitaires/new_users.csv');

        foreach ($additionalUsers as $additionalUser) {
            $groups = explode('|', $additionalUser['groups']);
            foreach ($groups as $group) {
                $ga = explode(',', $group);
                $groupAllocations[] = array(
                    'user' => $additionalUser['username'],
                    'group' => $ga[0],
                    'startDate' => new \DateTime($ga[1]),
                    'endDate' => new \DateTime($ga[2])
                );
            }
        }
        
        $listGroupAllocations = array();
        
        foreach ($groupAllocations as $i => $groupAllocation)
        {
            $user = $this->getReference('user_'.$groupAllocation['user']);
            $listGroupAllocations[$i] = new GroupAllocation();
            $listGroupAllocations[$i]->setGroup($this->getReference('group_'.$groupAllocation['group']));

            $listGroupAllocations[$i]->setStartDate($groupAllocation['startDate']);
            $listGroupAllocations[$i]->setEndDate($groupAllocation['endDate']);
            $user->addGroupAllocation($listGroupAllocations[$i]);

            $userManager->updateUser($user);
            /* $manager->persist($listGroupAllocations[$i]); */
        }

        $manager->flush();
    }
    
    public function getOrder()
    {
        return 3;
    }
}

