<?php

namespace Gesseh\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Gesseh\UserBundle\Entity\UserPriority;


class LoadUserPriorityData extends AbstractFixture implements OrderedFixtureInterface,ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $userPriorities = array(
            array(
                'user' => 'user0',
                'priority' => 1065,
                'type' => null,
                'startDate' => null,
                'endDate' => null
            ),
            array(
                'user' => 'user1',
                'priority' => 505,
                'type' => null,
                'startDate' => null,
                'endDate' => null
            ),
            array(
                'user' => 'user2',
                'priority' => 1068,
                'type' => null,
                'startDate' => null,
                'endDate' => null
            ),
            array(
                'user' => 'user3',
                'priority' => 4007,
                'type' => null,
                'startDate' => null,
                'endDate' => null
            ),
            array(
                'user' => 'user4',
                'priority' => 42,
                'type' => 'jobAllocation',
                'startDate' => new \DateTime("2015-05-01"),
                'endDate' => new \DateTime("2015-11-25")
            ),
        );


        foreach ($userPriorities as $userPriority) {
            $newUserPriority = new UserPriority;

            $newUserPriority->setUser($this->getReference('user_'.$userPriority['user']));
            $newUserPriority->setPriority($userPriority['priority']);
            $newUserPriority->setType($userPriority['type']);
            $newUserPriority->setStartDate($userPriority['startDate']);
            $newUserPriority->setEndDate($userPriority['endDate']);

            $manager->persist($newUserPriority);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}