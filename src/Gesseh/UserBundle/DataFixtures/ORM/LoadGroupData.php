<?php

namespace Gesseh\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gesseh\UserBundle\Entity\Group;


class LoadGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $groups = array (
            array('base_group', 'all', null, 0, null),
            array('DES_cardio', "Internes de cardio", 'base_group', 1, null),
            array('DES_anesth', "Internes d'anesthésie", 'base_group', 1, null),
            array('cardio_S1', "1er semestre", 'DES_cardio', 0, 1),
            array('cardio_S2', "2ème semestre", 'DES_cardio', 0, 2),
            array('cardio_dispo', "disponibilité", 'DES_cardio', 0, null),
            array('anesth_S1', "1er semestre", 'DES_anesth', 0, 1),
            array('anesth_S2', "2ème semestre", 'DES_anesth', 0, 2),
            array('anesth_S3', "3ème semestre", 'DES_anesth', 0, 3),
            array('anesth_dispo', "disponibilité", 'DES_anesth', 0, null),
            array('DESC', "DESC", 'base_group', 0, null),
            array('DESC_rea_med', "DESC réa méd", 'DESC', 1, null),
            array('DESC_hépato', "DESC hépato", 'DESC', 1, null)
        );

        $listGroups = array();
        foreach ($groups as $i => $group)
        {
            $listGroups[$i] = new Group($group[0]);
            $listGroups[$i]->setTitle($group[1]);
            if (null != $group[2]) {
                $listGroups[$i]->setParent($this->getReference('group_'.$group[2]));
            }
            $listGroups[$i]->setFamily($group[3]);
            $listGroups[$i]->setStage($group[4]);
            
            $manager->persist($listGroups[$i]);
            
            $this->addReference('group_'.$group[0], $listGroups[$i]);
        }
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 1;
    }
}

