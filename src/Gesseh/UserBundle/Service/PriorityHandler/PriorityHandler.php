<?php

namespace Gesseh\UserBundle\Service\PriorityHandler;

use Doctrine\ORM\EntityManager;
use Gesseh\CoreBundle\Service\PeriodHandler\PeriodInterface;
use Gesseh\CoreBundle\Service\PeriodHandler\PeriodHandler;
use Gesseh\CoreBundle\Service\PeriodHandler\Period;
use Gesseh\UserBundle\Entity\User;
use Gesseh\UserBundle\Entity\UserPriority;
use Gesseh\UserBundle\Entity\Group;
use Gesseh\UserBundle\Entity\GroupPriority;


class PriorityHandler
{
    private $em;
    private $periodHandler;
    private $allUsersPeriod;
    private $allGroupsPeriod;


    public function __construct(EntityManager $em, PeriodHandler $periodHandler)
    {
        $this->em = $em;
        $this->periodHandler = $periodHandler;
        $this->allUsersPeriod = $this->allGroupsPeriod = array ('period' => null, 'objects' => null);
    }

    
    private function getObjectsPriorityByPeriod($objectClass, array $objects, PeriodInterface $period, $type, $order = 'asc')
    {
        $getFunction = 'getBy'.$objectClass.'sTypeAndPeriod';
        $allObjectsVariable = 'all'.$objectClass.'sPeriod';
        if (null == $this->{$allObjectsVariable}['period'] or ! $this->periodHandler->isSamePeriod($period, $this->{$allObjectsVariable}['period'])) {
            $this->{$allObjectsVariable}['period'] = new Period($period->getStartDate(), $period->getEndDate());
            $this->{$allObjectsVariable}['objects'] = $this->em->getRepository('GessehUserBundle:'.$objectClass.'Priority')->$getFunction($objects, $type, $period);
        }

        $allObjectsPriorities = $this->{$allObjectsVariable}['objects'];

        $objectsPriorities = array();

        foreach ($objects as $object) {
            $objectPriorities = array();
            $typeDefined = false;
            foreach ($allObjectsPriorities as $objectPriority) {
                $getFunction = 'get'.$objectClass;
                if ($object == $objectPriority->$getFunction()) {
                    $objectPriorities[] = $objectPriority;
                    if (null != $objectPriority->getType()) {
                        $typeDefined = true;
                    }
                }
            }

            if ($typeDefined) {
                foreach ($objectPriorities as $key => $objectPriority) {
                    if (null == $objectPriority->getType()) {
                        unset ($objectPriorities[$key]);
                    }
                }
            }

            if (count ($objectPriorities) < 1) {
                $newClass = 'Gesseh\UserBundle\Entity\\'.$objectClass.'Priority';
                $newObjectPriority = new $newClass();
                $setFunction = 'set'.$objectClass;
                $newObjectPriority->$setFunction($object);
                $newObjectPriority->setType($type);
                $newObjectPriority->setStartDate($period->getStartDate());
                $newObjectPriority->setEndDate($period->getEndDate());
                $newObjectPriority->setPriority(0);
                $objectsPriorities[] = $newObjectPriority;
            }
            else {
                $objectsPriorities[] = $this->periodHandler->getNarrowestPeriod($objectPriorities);
            }
        }

        usort($objectsPriorities, function($a, $b) use ($order){
                if ($a->getPriority() == 0 and $b->getPriority() != 0) {
                    return 1;
                }
                elseif ($a->getPriority() != 0 and $b->getPriority() == 0) {
                    return -1;
                }

                if ($order == 'asc') {
                    return $a->getPriority() > $b->getPriority() ? 1 : -1;
                }
                else {
                    return $a->getPriority() > $b->getPriority() ? -1 : 1;
                }
            });

        
        return $objectsPriorities;
    }


    public function getUsersPriorityByPeriod(array $users, PeriodInterface $period, $type, $order = 'asc')
    {
        return $this->getObjectsPriorityByPeriod('User', $users, $period, $type, $order);
    }


    public function getGroupsPriorityByPeriod(array $groups, PeriodInterface $period, $type, $order = 'asc')
    {
        return $this->getObjectsPriorityByPeriod('Group', $groups, $period, $type, $order);
    }


    public function getUsersGroupsPriorityByPeriod(array $users, array $groups, PeriodInterface $period, $type, $userPriorityOrder = 'asc', $groupPriorityOrder = 'asc')
    {
        $groupPriorities = $this->getGroupsPriorityByPeriod($groups, $period, $type, $groupPriorityOrder);
        $orderedGroups = array();
        foreach ($groupPriorities as $groupPriority){
            $orderedGroups[] = $groupPriority->getGroup();
        }
        /* foreach($orderedGroups as $o){echo $o->getGroup()->__toString().': '.$o->getPriority().'<br>';}; */
        
        $orderedUsers = array();

        foreach ($orderedGroups as $group) {
            $concernedUsers = array();
            foreach ($users as $user) {
                foreach ($user->getGroupAllocations() as $groupAllocation) {
                    if ($group == $groupAllocation->getGroup() and ! in_array($user, $orderedUsers)) {
                        $concernedUsers[] = $user;
                    }
                }
            }
            if (count ($concernedUsers) > 0) {
                $orderedUsers = array_merge($orderedUsers, $this->getUsersPriorityByPeriod($concernedUsers, $period, $type, $userPriorityOrder));
            }
        }

        /* foreach ($orderedUsers as $u){echo $u->getUser()->__toString(); } */

        return $orderedUsers;
    }

    public function getUsersByPriorityPeriod(array $users, array $groups, PeriodInterface $period, $type, $userPriorityOrder = 'asc', $groupPriorityOrder = 'asc')
    {
        $usersPriority = $this->getUsersGroupsPriorityByPeriod($users, $groups, $period, $type, $userPriorityOrder, $groupPriorityOrder);

        $users = array();
        foreach ($usersPriority as $userPriority) {
            if (! in_array($userPriority->getUser(), $users)) {
                $users[] = $userPriority->getUser();
            }
        }

        return $users;
    }
}