<?php

namespace Gesseh\UserBundle\Service\RoleChecker;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Doctrine\ORM\EntityManager;
use Gesseh\UserBundle\Entity\User;
use Gesseh\UserBundle\Entity\Group;
use Gesseh\UserBundle\Service\GroupsHierarchy\GroupsHierarchy;
use Gesseh\UserBundle\Service\RolesHierarchy\RolesHierarchy;

/***************************************/
/* se limiter à la gestion des groupes */
/***************************************/

/**
 * Check roles of the token
 */
class RoleChecker
{
    private $roleApplications;
    private $groupsHierarchy;
    private $rolesHierarchy;
    private $em;


    /**
     * Constructor
     *
     * @param Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $securityContext
     * @param Gesseh\UserBundle\Service\GroupsHierarchy\GroupsHierarchy $groupsHierarchy
     * @param Gesseh\UserBundle\Service\RolesHierarchy\RolesHierarchy $rolesHierarchy
     * @param Doctrine\ORM\EntityManager $em
     */
    public function __construct(TokenStorage $securityContext, GroupsHierarchy $groupsHierarchy, RolesHierarchy $rolesHierarchy, EntityManager $em)
    {
        $this->roleApplications = array();
        if (null != $securityContext->getToken()) {
            // The token for anonymous user is the string "anon."
            if ($securityContext->getToken()->getUser() != 'anon.') {
                $this->roleApplications = $securityContext->getToken()->getUser()->getRoleApplications();
            }
        }

        $this->groupsHierarchy = $groupsHierarchy;
        $this->rolesHierarchy = $rolesHierarchy;
        $this->em = $em;
    }


    /* /\** */
    /*  * Provides the GroupsHierarchy service */
    /*  * */
    /*  * @return GroupsHierarchy $groupsHierarchy */
    /*  *\/ */
    /* public function getGroupsHierarchy() */
    /* { */
    /*     return $this->groupshierarchy; */
    /* } */

    public static function getObjectClasses()
    {
        return array(
            'Group' => 'Gesseh\UserBundle\Entity\Group',
            'User' => 'Gesseh\UserBundle\Entity\User'
        );
    }

    private static function getObjectTypes()
    {
        $objectTypes = array();
        foreach (self::getObjectClasses() as $objectType => $objectClass) {
            $objectTypes[] = $objectType;
        }
        
        return $objectTypes;
    }


    private function getRoleAppsByObjectType($objectType)
    {
        $roleApplications = array();

        foreach ($this->roleApplications as $roleApplication) {
            if ($roleApplication->getObjectType() == $objectType) {
                $roleApplications[] = $roleApplication;
            }
        }

        return $roleApplications;
    }


    private function getRoleAppsByRoles(array $roles)
    {
        $roleApplications = array();

        foreach ($this->roleApplications as $roleApplication) {
            if (in_array ($roleApplication->getRole(), $roles)) {
                $roleApplications[] = $roleApplication;
            }
        }

        return $roleApplications;
    }


    private function getRoleAppsByRolesAndObjectType(array $roles, $objectType)
    {
        $roleApplications = array();
        /* echo count ($this->roleApplications).'<br/>'; */

        foreach ($this->roleApplications as $roleApplication) {
            if (in_array($roleApplication->getRole(), $roles) and $roleApplication->getObjectType() == $objectType) {
                $roleApplications[] = $roleApplication;
            }
        }

        return $roleApplications;
    }


    private function hasRoleForGroup(array $roles, Group $group)
    {
        $roleApplications = $this->getRoleAppsByRolesAndObjectType($roles, 'Group');

        foreach ($roleApplications as $roleApplication) {
            /* print_r($this->groupsHierarchy->getParentGroupsId(array($group))); */
            if (in_array ($roleApplication->getObjectId(), $this->groupsHierarchy->getParentGroupsId(array($group)))) {
                return true;
            }
        }

        return false;
    }


    private function hasRoleForUser(array $roles, User $user)
    {
        $roleApplications = $this->getRoleAppsByRolesAndObjectType($roles, 'User');

        foreach ($roleApplications as $roleApplication) {
            if ($roleApplication->getObjectId() == $user->getId()) {
                return true;
            }
        }

        foreach ($user->getGroupAllocations() as $groupAllocation) {
            if ($this->hasRoleForGroup($roles, $groupAllocation->getGroup())) {
                return true;
            }
        }

        return false;
    }


    public function hasRole($role, $object)
    {
        $parentRoles = $this->rolesHierarchy->getParentRoles(array($role));
        /* foreach ($parentRoles as $r) {echo $r->getRole().'<br/>';} */
        if ($object instanceof Group) {
            return $this->hasRoleForGroup($parentRoles, $object);
        }
        elseif ($object instanceof User) {
            return $this->hasRoleForUser($parentRoles, $object);
        }

        return false;
    }


    public function getReachableObjects($role = null, $objectType = null)
    {
        if (null != $objectType and ! in_array ($objectType, self::getObjectTypes())) {
            throw new \InvalidArgumentException("Argument ObjectType has to be one of the following : ".implode(", ", self::getObjectTypes()));
        }

        if (null != $role) {
            $parentRoles = $this->rolesHierarchy->getParentRoles(array ($role));
            if (null != $objectType) {
                $roleApplications = $this->getRoleAppsByRolesAndObjectType($parentRoles, $objectType);
            }
            else {
                $roleApplications = $this->getRoleAppsByRoles($parentRoles);
            }
        }
        else {
            if (null != $objectType)
                $roleApplications = $this->getRoleAppsByObjectType($objectType);
            else
                $roleApplications = $this->roleApplications;
        }
        /* echo count($roleApplications); */

        if (null != $objectType) {
            $objectsId = array();

            foreach ($roleApplications as $roleApplication) {
                $objectsId[] = $roleApplication->getObjectId();
            }

            switch ($objectType) {
              case 'Group' :
                  return $this->groupsHierarchy->getReachableGroupsByIds($objectsId);
                  break;
              case 'User' :
                  return $this->em->getRepository('GessehUserBundle:User')->getByIds($objectsId);
                  break;
            }
        }
        else {
            $reachableObjects = array();

            foreach (self::getObjectTypes() as $objectType) {
                $reachableObjects = array_merge($reachableObjects, $this->getReachableObjects($role, $objectType));
            }

            return array_unique($reachableObjects);
        }

        return null;
    }


    /* public function getReachableObjectsId($role = null, $objectType = null) */
    /* { */
    /*     $objectsId = array(); */
    /*     $objects = $this->getReachableObjects($role, $objectType); */

    /*     foreach ($objects as $object) { */
    /*         $objectsId[] = $object->getId(); */
    /*     } */

    /*     return $objectsId; */
    /* } */
    

    public function getReachableObjectsTitle($role = null, $objectType = null)
    {
        $objectsTitle = array();
        $objects = $this->getReachableObjects($role, $objectType);

        foreach ($objects as $object) {
            $objectsTitle[] = $object->getCompleteTitle();
        }

        return $objectsTitle;
    }


    public function getReachableRoles()
    {
        $roles = array();

        foreach ($this->roleApplications as $roleApplication) {
            $roles[] = $roleApplication->getRole();
        }

        return $this->rolesHierarchy->getReachableRoles($roles);
    }

    public function isUserReachable(User $user, $role)
    {
        $reachableGroups = $this->getReachableObjects($role, 'Group');

        $userGroupAllocations = $user->getCurrentGroupAllocations();
        foreach($userGroupAllocations as $userGroupAllocation) {
            if (in_array ($userGroupAllocation->getGroup(), $reachableGroups)) {
                return true;
            }
        }

        return false;
    }
}
