<?php

namespace Gesseh\UserBundle\Service\MemberGroup;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Gesseh\UserBundle\Service\GroupsHierarchy\GroupsHierarchy;
use Gesseh\UserBundle\Service\RolesHierarchy\RolesHierarchy;

class MemberGroup
{
    private $tokenMemberGroups;
    private $groupsHierarchy;
    /* private $rolesHierachy; */

    public function __construct(SecurityContextInterface $securityContext, GroupsHierarchy $groupsHierarchy, RolesHierarchy $rolesHierarchy, $requiredRoles = array())//ENLEVER "requiredRoles" et rolesHierarchy
    {
        $this->groupsHierarchy = $groupsHierarchy;
        /* $this->rolesHierarchy = $rolesHierarchy; */

        $this->tokenMemberGroups = array();
        if (null != $securityContext->getToken()) {
            if ($securityContext->getToken()->getUser() != "anon.") {
                $this->tokenMemberGroups = $securityContext->getToken()->getUser()->getMemberGroups();
                
                /* foreach ($securityContext->getToken()->getUser()->getMemberGroups() as $memberGroup) { */
                    /* if (empty ($requiredRoles)) { */
                    /*     $this->tokenMemberGroups[] = $memberGroup->getGroup(); */
                    /* } */
                    /* else { */
                    /*     foreach ($memberGroup->getRoles()->toArray() as $role) { */
                    /*         foreach ($this->rolesHierarchy->getReachableRoles(array($role)) as $reachableRole) { */
                    /*             if (in_array ($reachableRole->getRole(), $requiredRoles)) { */
                    /* $this->tokenMemberGroups[] = $memberGroup->getGroup(); */
                                /* } */
                    /*         } */
                    /*     } */
                    /* } */
                /* } */
            }
        }
    }

    
    public function getMemberGroups($onlyCurrentGroups = true)
    {
        $memberGroups = array();
        
        $currentDate = new \DateTime();
        foreach ($this->tokenMemberGroups as $tokenMemberGroup) {
            if (!$onlyCurrentGroups or ($onlyCurrentGroups and ($tokenMemberGroup->getStartDate() > $currentDate or $tokenMemberGroup->getEndDate() < $currentDate))) {
                $memberGroups[] = $tokenMemberGroup->getGroup();
            }
        }
        
        return $memberGroups;
    }
    

    public function getFamiliesGroups()
    {
        return $this->groupsHierarchy->getFamiliesGroups($this->getMemberGroups());
    }
}