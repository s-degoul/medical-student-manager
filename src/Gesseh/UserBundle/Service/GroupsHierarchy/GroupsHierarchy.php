<?php

namespace Gesseh\UserBundle\Service\GroupsHierarchy;

use Doctrine\ORM\EntityManager;
use Gesseh\UserBundle\Entity\Group;

class GroupsHierarchy
{
    private $map;
    private $hierarchy;
    private $allGroups;


    /**
     * Constructor
     *
     * @param Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->allGroups = $em->getRepository('GessehUserBundle:Group')->findAll();
        $this->buildGroupsHierarchy();
        $this->buildGroupsMap();
    }



    /*
     * FOR TESTS (to be removed)
     */
    public function getMap()
    {
        return $this->map;
    }

    /*
     * FOR TESTS (to be removed)
     */
    public function getHierarchy()
    {
        return $this->hierarchy;
    }


    /**
     * Get groups reachable from a set of groups
     *
     * "Reachable" mean direct or distant children. Groups are reachable from themselves.
     *
     * @param array $groups   array of Gesseh\UserBundle\Entity\Group Entities
     *
     * @return array $reachableGroups   array of reachable groups
     */
    public function getReachableGroups(array $groups)
    {
        $reachableGroups = $groups;
        foreach ($groups as $group) {
            if (! $group instanceof Group) {
                throw \InvalidArgumentException("The argument $groups must be an array of Gesseh\UserBundle\Entity\Group entities");
            }
            
            if (!isset ($this->map[$group->getId()])) {
                continue;
            }

            foreach ($this->map[$group->getId()] as $groupId) {
                $reachableGroup = $this->getGroupById($groupId);
                if (! in_array($reachableGroup, $reachableGroups)) {
                    $reachableGroups[] = $reachableGroup;
                }
            }
        }

        return $reachableGroups;
    }


    /**
     * Get groups reachable from a set of groups defined by their Id
     *
     * @param array $groups   array of Gesseh\UserBundle\Entity\Group Entities
     *
     * @return array $reachableGroups   array of reachable groups
     */
    public function getReachableGroupsByIds(array $groupsId)
    {
        $groups = array();
        foreach ($groupsId as $groupId) {
            if (! is_int($groupId)) {
                throw \InvalidArgumentException("The argument $groupIds must be an array of integers");
            }
            $groups[] = $this->getGroupById($groupId);
        }

        return $this->getReachableGroups($groups);
    }

    
    /**
     * Get Ids of groups reachable from a set of groups
     *
     * @param array $groups   array of Gesseh\UserBundle\Entity\Group Entities
     *
     * @return array $reachableGroupsId   array of reachable groups Ids
     */
    public function getReachableGroupsId(array $groups)
    {
        $reachableGroupsId = array();
        foreach ($this->getReachableGroups($groups) as $reachableGroup) {
            if (! $reachableGroup instanceof Group) {
                throw \InvalidArgumentException("The argument $reachableGroup must be an instance of Gesseh\UserBundle\Entity\Group");
            }
            $reachableGroupsId[] = $reachableGroup->getId();
        }

        return $reachableGroupsId;
    }

    
    /**
     * Get direct recheable groups of a group
     *
     * @param Gesseh\UserBundle\Entity\Group $group   
     *
     * @return array $directReachableGroups   array of groups
     */
    public function getDirectReachableGroups(Group $group)
    {
        if (array_key_exists($group->getId(), $this->hierarchy)) {
            $directReachableGroupsId = $this->hierarchy[$group->getId()];
            $directReachableGroups = array();
            foreach ($directReachableGroupsId as $childGroupId) {
                $directReachableGroups[] = $this->getGroupById($childGroupId);
            }
            return $directReachableGroups;
        }

        return null;
    }


    /**
     * Get all parent groups (direct and distant)
     *
     * @param Gesseh\UserBundle\Entity\Group or array of groups
     *
     * @return array $parentGroups   array of groups
     */
    public function getParentGroups($group)
    {
        $parentGroups = array();

        if (is_array ($group)) {
            $parentGroups = $group;
            foreach ($group as $g) {
                $parentGroups = array_merge($parentGroups, $this->getParentGroups($g));
            }
        }
        elseif ($group instanceof Group) {
            $parentGroups[] = $group;
            foreach ($this->map as $mainGroupId => $groupsId) {
                if (in_array ($group->getId(), $groupsId)) {
                    $parentGroups[] = $this->getGroupById($mainGroupId);
                }
            }
        }
        else {
            throw \InvalidArgumentException("The argument $group must be an instance of Gesseh\UserBundle\Entity\Group or an array of Group");
        }

        return array_unique($parentGroups);
    }


    /**
     * Get Ids of all parent groups (direct and distant)
     *
     * @param Gesseh\UserBundle\Entity\Group or array of groups
     *
     * @return array $parentGroupsId   array of Ids (integer)
     */
    public function getParentGroupsId($group)
    {
        $parentGroupsId = array();
        foreach ($this->getParentGroups($group) as $parentGroup) {
            $parentGroupsId[] = $parentGroup->getId();
        }

        return $parentGroupsId;
    }
    

    /**
     * Get family groups of a set of groups
     *
     * @param array $groups   array of Gesseh\UserBundle\Entity\Group Entities
     *
     * @return array $families   array of family groups
     */
    public function getFamilies(array $groups)
    {
        $families = array();
        foreach($groups as $group) {
            if (! $group instanceof Group) {
                throw \InvalidArgumentException("The argument $groups must be an array of Gesseh\UserBundle\Entity\Group entities");
            }
            
            if ($group->isFamily() and !in_array($group, $families)) {
                $families[] = $group;
            }
            // if the group is a family group, there is no other family group in the same branch
            elseif (! $group->isFamily()) {
                foreach ($this->map as $mainGroupId => $groupsId) {
                    if (in_array($group->getId(), $groupsId)) {
                        $mainGroup = $this->getGroupById($mainGroupId);
                        if ($mainGroup->isFamily() and ! in_array($mainGroup, $families)) {
                            $families[] = $mainGroup;
                        }
                    }
                }
            }
        }
        
        return $families;
    }
  

    /**
     * Get the groups which belong to the same family(ies) to a set of groups
     *
     * @param array $groups   array of Gesseh\UserBundle\Entity\Group Entities
     *
     * @return array $families   array of groups
     */
    public function getFamiliesGroups(array $groups)
    {
        $families = $this->getFamilies($groups);

        return $this->getReachableGroups($families);
    }


    /**
     * Get groups with a non null "stage", reachable from a set of groups
     *
     * @param array $groups   array of Gesseh\UserBundle\Entity\Group Entities
     *
     * @return array $reachableGroups   array of reachable groups
     */
    public function getReachableStagedGroups(array $groups)
    {
        $reachableGroups = $this->getReachableGroups($groups);
        foreach ($reachableGroups as $key => $group) {
            if (null == $group->getStage()) {
                unset ($reachableGroups[$key]);
            }
        }

        return $reachableGroups;
    }

    
    /**
     * Get the next staged group
     *
     * The group with the minimum value of "stage" above the reference group
     *
     * @param Gesseh\UserBundle\Entity\Group $group  the reference group
     *
     * @return Gesseh\UserBundle\Entity\Group $nextStagedGroup
     */
    public function getNextStagedGroup(Group $group)
    {
        $familyGroups = $this->getFamiliesGroups(array ($group));
        $minStage = null;
        $nextStagedGroup = null;

        foreach ($familyGroups as $familyGroup) {
            $stage = $familyGroup->getStage();
            if (null != $stage and $stage > $group->getStage() and ($stage < $minStage or null == $minStage)) {
                $minStage = $stage;
                $nextStagedGroup = $familyGroup;
            }
        }

        return $nextStagedGroup;
    }


    /**
     * Get groups sorted by their stage
     *
     * @param array $groups   array of Gesseh\UserBundle\Entity\Groups entities
     * @param string $order  direction of the sort ('asc' or 'desc')
     * @param bool $withoutStageAfter  should the groups with a empty stage value be added after or before the list ?
     *
     * @return array $sortedGroups
     */
    public function sortByStage(array $groups, $order = 'asc', $withoutStageAfter = true)
    {
        $sortedGroups = array();
        $groupsWithStage = $groupsWithoutStage = array();

        foreach ($groups as $group) {
            if (! $group instanceof Group) {
                throw \InvalidArgumentException("The argument $groups must be an array of Gesseh\UserBundle\Entity\Group entities");
            }
            
            if (null == $group->getStage()) {
                $groupsWithoutStage[] = $group;
            }
            else {
                $groupsWithStage[] = $group;
            }
        }

        usort($groupsWithStage, function($a, $b) use ($order) {
                if ($order == 'asc') {
                    return $a->getStage() > $b->getStage() ? 1 : -1;
                }
                else {
                    return $a->getStage() > $b->getStage() ? -1 : 1;
                }
            });

        if ($withoutStageAfter) {
            $sortedGroups = array_merge($groupsWithStage, $groupsWithoutStage);
        }
        else {
            $sortedGroups = array_merge($groupsWithoutStage, $groupsWithStage);
        }

        return $sortedGroups;
    }


    /**
     * Get groups sorted by their stage and their family group
     *
     * Groups which do not belong to a "family" are added at the end of the list
     *
     * @param array $groups   array of Gesseh\UserBundle\Entity\Groups entities
     * @param string $order  direction of the sort ('asc' or 'desc')
     * @param bool $withoutStageAfter  should the groups with a empty stage value be added after or before the list ?
     *
     * @return array $sortedGroups
     */
    public function sortByStageByFamily(array $groups, $order = 'asc', $withoutStageAfter = true)
    {
        $sortedGroups = array();

        $families = $this->sortByStage($this->getFamilies($groups), $order, $withoutStageAfter);

        $groupsWithoutFamily = $groups;

        foreach ($families as $family) {
            $familyGroups = $this->getReachableGroups(array($family));
            $groupsToSort = array();

            foreach ($familyGroups as $familyGroup) {
                if (in_array ($familyGroup, $groups)) {
                    $groupsToSort[] = $familyGroup;
                }

                foreach ($groupsWithoutFamily as $key => $groupWithoutFamily) {
                    if ($groupWithoutFamily == $familyGroup) {
                        unset ($groupsWithoutFamily[$key]);
                    }
                }
            }
            $sortedGroups = array_merge($sortedGroups, $this->sortByStage($groupsToSort, $order, $withoutStageAfter));
        }

        return array_merge($sortedGroups, $groupsWithoutFamily);
    }


    /**
     * Get reachable groups sorted by their stage and their family group
     *
     * @param array $groups   array of Gesseh\UserBundle\Entity\Groups entities
     * @param string $order  direction of the sort ('asc' or 'desc')
     * @param bool $withoutStageAfter  should the groups with a empty stage value be added after or before the list ?
     *
     * @return array
     */ 
    public function getSortedReachableGroups(array $groups, $order = 'asc', $withoutStageAfter = true)
    {
        return $this->sortByStageByFamily($this->getReachableGroups($groups, $order, $withoutStageAfter));
    }


    /**
     * Get a group entity having an id
     *
     * @param int $id
     *
     * @return Gesseh\UserBundle\Entity\Group
     */
    private function getGroupById($id)
    {
        foreach ($this->allGroups as $group) {
            if ($id === $group->getId()) {
                return $group;
            }
        }

        throw new \Exception("GroupHierarchy:getGroupById() : unable to find a group with id ".$id);
    }
        

    /**
     * Builds an array linking parent groups id with their children groups id. Only groups which belongs to a hierarchy (either parent or children) are taken into account
     *
     * @return Gesseh\UserBundle\Service\GroupHierarchy\GroupsHierarchy
     */
    private function buildGroupsHierarchy()
    {
        $this->hierarchy = array();
        foreach ($this->allGroups as $group){
            if (null != $group->getParent()){
                if (!array_key_exists ($group->getParent()->getId(), $this->hierarchy)){
                    $this->hierarchy[$group->getParent()->getId()] = array();
                }
                $this->hierarchy[$group->getParent()->getId()][] = $group->getId();
            }
        }

        return $this;
    }


    /**
     * Builds an array linking parent groups with all their children (direct or distant) groups.
     *
     * @return Gesseh\UserBundle\Service\GroupHierarchy\GroupsHierarchy
     */
    private function buildGroupsMap()
    {
        $this->map = array();
        foreach ($this->hierarchy as $mainGroup => $childrenGroups) {
            $this->map[$mainGroup] = $childrenGroups;
            
            $exploredGroups = array();
            $additionalGroups = $childrenGroups;
            while ($additionalGroup = array_shift($additionalGroups)) {
                if (!isset($this->hierarchy[$additionalGroup])) {
                    continue;
                }

                $exploredGroups[] = $additionalGroup;
                $this->map[$mainGroup] = array_unique(array_merge($this->map[$mainGroup], $this->hierarchy[$additionalGroup]));
                
                $additionalGroups = array_merge($additionalGroups, array_diff($this->hierarchy[$additionalGroup], $exploredGroups));
            }
        }

        return $this;
    }
}