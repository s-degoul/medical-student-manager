<?php

namespace Gesseh\UserBundle\Service\Converter;

class CSVArrayConverter
{
    /* public function __construct() */
    /* { */
    /* } */
    const WRONG_FILE_FORMAT = 1;
    const FILE_ERROR = 2;

    public function convert($filename, $delimiter = ',', $header = array())
    {
        if (! file_exists ($filename) or ! is_readable ($filename)) {
            return false;
        }

        $data = array();

        if ($file = fopen($filename, 'r')) {
            while (($row = fgetcsv ($file, 0, $delimiter)) !== FALSE) {
                if (null == $row) {
                    return self::WRONG_FILE_FORMAT;
                }
                
                if (! $header) {
                    $header = $row;
                }
                else {
                    if (count($row) !== count($header)) {
                        return self::WRONG_FILE_FORMAT;
                    }
                    $data[] = array_combine($header, $this->emptyToNull($row));
                }
            }
            
            fclose ($file);    
        }
        else {
            return self::FILE_ERROR;
        }

        return $data;
    }

    
    private function emptyToNull(array $data)
    {
        foreach($data as $key => $value) {
            if ($value === "") {
                $data[$key] = null;
            }
        }

        return $data;
    }
}