<?php

namespace Gesseh\UserBundle\Service\RequiredRoles;

class RequiredRoles
{
    private $requiredRoles;

    public function __construct()
    {
        $this->requiredRoles = array();
    }

    public function setRequiredRoles(array $requiredRoles)
    {
        $this->requiredRoles = $requiredRoles;

        return $this;
    }

    public function getRequiredRoles()
    {
        return $this->requiredRoles;
    }
}