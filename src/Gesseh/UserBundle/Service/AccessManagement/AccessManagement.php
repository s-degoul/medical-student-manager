<?php

namespace Gesseh\UserBundle\Service\AccessManagement;

use Doctrine\ORM\EntityManager;
use Gesseh\UserBundle\Service\GroupsHierarchy\GroupsHierarchy;
use Gesseh\UserBundle\Service\MemberGroup\MemberGroup;


class AccessManagement
{
    private $roleApplications;
    private $user;
    private $groupsHierarchy;
    private $memberGroups;
    private $concernedRoleApplications;

    
    public function __construct(EntityManager $em, SecurityContextInterface $securityContext, GroupsHierarchy $groupsHierarchy, MemberGroup $memberGroup)
    {
        $this->roleApplications = $em->getRepository('GessehUserBundle:RoleApplication')->findAll();
        if ($securityContext->getToken()->getUser() instanceof Gesseh\UserBundle\Entity\User) {
            $this->user = $securityContext->getToken()->getUser();
        }
        $this->groupshierarchy = $groupsHierarchy;
        $this->memberGroups = $memberGroup->getMemberGroups();
        $this->concernedRoleApplications = $this->getConcernedRoleApplications();
    }

    
    private function getConcernedRoleApplications()
    {
        $concernedRoleApplications = array();
        
        $concernedGroupIds = array();
        foreach ($this->groupshierarchy->getParentGroups($this->memberGroups) as $concernedGroup) {
            $concernedGroupIds[] = $concernedGroup->getId();
        }
        
        foreach ($this->roleApplications as $roleApplication) {
            $actorId = $roleApplication->getActorId();

            switch ($roleApplication->getActorType()) {
              case 'user':
                  if ($actorId == $this->user->getId()) {
                      $concernedRoleApplications[] = $roleApplication;
                  }
                  break;
              case 'group':
                  if($actorId == $roleApplication::ALL_ID or $actorId == $roleApplication::FAMILY_ID or in_array($actorId, $concernedGroupIds)) {
                      $concernedRoleApplications[] = $roleApplication;
                  }
                  break;
              case 'job':
                  break;
            }
        }

        return $concernedRoleApplications;
    }


    public function getRoleApplicationsByRole($roleName)
    {
        $rolesApplications = array();

        foreach ($this->concernedRoleApplications as $concernedRoleApplication) {
            if ($concernedRoleApplication->getRole()->getRole() == $roleName) {
                $rolesApplications[] = $concernedRoleApplication;
            }
        }

        return $rolesApplications;
    }


    public function getRoles()
    {
        $roles = array();

        foreach ($this->concernedRoleApplications as $concernedRoleApplication) {
            $roles[] = $concernedRoleApplication->getRole()->getRole();
        }

        return array_unique($roles);
    }
}