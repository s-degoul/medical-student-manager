<?php

namespace Gesseh\UserBundle\Service\RolesHierarchy;

use Symfony\Component\Security\Core\Role\RoleHierarchy as BaseRoleHierarchy;
use Doctrine\ORM\EntityManager;
use Gesseh\UserBundle\Entity\Role;

class RolesHierarchy extends BaseRoleHierarchy
{
    private $em;
    private $allRoles;


    /**
     * Constructor
     *
     * @param Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->allRoles = $this->em->getRepository('GessehUserBundle:Role')->findAll();
        parent::__construct($this->buildRolesArray());
    }


    /* public function getMap() */
    /* { */
    /*     return $this->map; */
    /* } */


    /**
     * Provides an array of all reachable roles by the given ones
     *
     * Reachable roles are the roles directly assigned but also all roles that
     * are transitively reachable from them in the role hierarchy.
     *
     * @param Gesseh\UserBundle\Entity\Role[] $roles  an array of directly assigned roles
     *
     * @return Gesseh\UserBundle\Entity\Role[] or array of strings (role names) $reachableRoles  an array of all reachable roles
     */
    public function getReachableRoles(array $roles)
    {
        $reachableRoles = array();

        foreach ($roles as $role) {
            if (is_string ($role)) {
                $roleName = $role;
                $reachableRoles[] = $this->getRoleByName($role);
            }
            elseif ($role instanceof Role) {
                $roleName = $role->getRole();
                $reachableRoles[] = $role;
            }
            else {
                throw \InvalidArgumentException("The argument $roles must be an array of Gesseh\UserBundle\Entity\Role entities");
            }

            if (!isset($this->map[$roleName])) {
                continue;
            }

            foreach ($this->map[$roleName] as $r) {
                $reachableRoles[] = $this->getRoleByName($r);
            }
        }

        return $reachableRoles;
    }



    /**
     * Get parent roles (direct or indirect) of given ones
     *
     * @param Gesseh\UserBundle\Entity\Role[] $roles  an array of roles
     * @param bool $include  should the given roles be included in the result ?
     *
     * @return Gesseh\UserBundle\Entity\Role[] $parentRoles
     */
    public function getParentRoles(array $roles, $include = true)
    {
        $parentRoles = array();

        foreach ($roles as $role) {
            if (is_string ($role)) {
                $roleName = $role;
                if ($include) {
                    $parentRoles[] = $this->getRoleByName($role);
                }
            }
            elseif ($role instanceof Role) {
                $roleName = $role->getName();
                if ($include) {
                    $parentRoles[] = $role;
                }
            }
            else {
                throw \InvalidArgumentException("The argument $roles must be an array of Gesseh\UserBundle\Entity\Role entities");
            }

            foreach ($this->map as $mainRole => $r) {
                if (in_array ($roleName, $r)) {
                    $parentRoles[] = $this->getRoleByName($mainRole);
                }
            }
        }

        return $parentRoles;
    }


    /**
     * Builds an array linking parent roles name with their children roles name
     *
     * @return array $hierarchy
     */
    private function buildRolesArray()
    {
        $hierarchy = array();

        foreach ($this->allRoles as $role){
            if (null != $role->getParent()){
                if (!array_key_exists ($role->getParent()->getName(), $hierarchy)){
                    $hierarchy[$role->getParent()->getName()] = array();
                }
                $hierarchy[$role->getParent()->getName()][] = $role->getName();
            }
            else {
                if (!array_key_exists ($role->getName(), $hierarchy)){
                    $hierarchy[$role->getName()] = array();
                }
            }
        }

        return $hierarchy;
    }


    /**
     * Get Role entity from its name
     *
     * @param string $name
     *
     * @return Gesseh\UserBundle\Entity\Role $role
     */
    private function getRoleByName($name)
    {
        foreach ($this->allRoles as $role) {
            if ($role->getName() == $name) {
                return $role;
            }
        }

        throw new \Exception("Unable to find role ".$name);
        return null;
    }
}