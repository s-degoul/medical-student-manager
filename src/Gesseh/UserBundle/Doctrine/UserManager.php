<?php

namespace Gesseh\UserBundle\Doctrine;

use FOS\UserBundle\Doctrine\UserManager as BaseUserManager;
use FOS\UserBundle\Util\CanonicalizerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use FOS\UserBundle\Model\UserInterface;

use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;
use Gesseh\UserBundle\Entity\Group;
use Gesseh\UserBundle\Entity\GroupAllocation;
use Gesseh\UserBundle\Entity\RoleApplication;
use Gesseh\UserBundle\Validator\Constraint\GroupsNumberNotNull;
use Gesseh\CoreBundle\Service\PeriodHandler\PeriodHandler;


/**
 * Service for management of user entity
 */
class UserManager extends BaseUserManager
{
    const USER_DELETED = 0;
    const USER_NOT_DELETED = 1;
    const NO_ERROR = 0;
    const NO_ACTION = 1;

    const NB_CHAR_SURNAME_PART = 8;
    const SEP_CHAR = '.';


    private $roleChecker;
    private $validator;
    private $periodHandler;

    
    /**
     * Constructor function
     *
     * @param EncoderFactoryInterface $encoderFactory
     * @param CanonicalizerInterface $usernameCanonicalizer
     * @param ObjectManager $objectManager
     * @param RoleChecker $roleChecker
     * @param RecursiveValidator $validator
     */
    public function __construct(EncoderFactoryInterface $encoderFactory, CanonicalizerInterface $usernameCanonicalizer, CanonicalizerInterface $emailCanonicalizer, ObjectManager $objectManager, RoleChecker $roleChecker, RecursiveValidator $validator, PeriodHandler $periodHandler)
    {
        parent::__construct($encoderFactory, $usernameCanonicalizer, $emailCanonicalizer, $objectManager, 'GessehUserBundle:User');
        $this->roleChecker = $roleChecker;
        $this->validator = $validator;
        $this->periodHandler = $periodHandler;
    }


    /**
     * Storage of entities in the database
     */
    public function flush()
    {
        return $this->objectManager->flush();
    }


    /**
     * Update of the user
     *
     * with update of its expiration date
     *
     * @param UserInterface $user
     * @param bool $andFlush
     */
    public function updateUser(UserInterface $user, $andFlush = true)
    {
        $this->updateExpirationDate($user);

        parent::updateUser($user, $andFlush);
    }


    /**
     * Get the repository of user entity
     */
    public function getRepository()
    {
        return $this->repository;
    }


    /**
     * Delete the user entity from the database
     *
     * @param UserInterface $user
     * @param bool $andFlush
     */
    public function deleteUser(UserInterface $user, $andFlush = true)
    {
        foreach ($user->getGroupAllocations() as $groupAllocation) {
            if ($this->roleChecker->hasRole('ROLE_ADMIN_USER', $groupAllocation->getGroup())) {
                $user->removeGroupAllocation($groupAllocation);
                $this->objectManager->remove($groupAllocation);
            }
        }
        
        if (! $user->getGroupAllocations()->toArray()) {
            $this->objectManager->remove($user);
            if ($andFlush) {
                $this->objectManager->flush();
            }
            return self::USER_DELETED;
        }
        else {
            $this->updateUser($user, $andFlush);
            return self::USER_NOT_DELETED;
        }
    }


    /**
     * Delete many user entities from the database
     *
     * @param array $users
     * @param bool $andFlush
     */
    public function deleteUsers(array $users, $andFlush = true)
    {
        $deletedUsersName = array();
        $notDeletedUsersName = array();

        foreach ($users as $user) {
            if ($user instanceof UserInterface) {
                if ($this->deleteUser($user, false) == self::USER_DELETED) {
                    $deletedUsersName[] = $user->__toString();
                }
                else {
                    $notDeletedUsersName[] = $user->__toString();
                }
            }
        }

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return array(
            'deletedUsersName' => $deletedUsersName,
            'notDeletedUsersName' => $notDeletedUsersName
        );
    }

    
    public function addGroupAllocationToUser(Group $group, UserInterface $user, \DateTime $startDate = null, \DateTime $endDate = null, $andFlush = true)
    {
        $newGroupAllocation = new GroupAllocation();
        $newGroupAllocation->setGroup($group);
        $newGroupAllocation->setStartDate($startDate);
        $newGroupAllocation->setEndDate($endDate);
        $user->addGroupAllocation($newGroupAllocation);

        $validation = $this->validator->validate($newGroupAllocation, array('admin'));

        if ($validation->count() > 0) {
            $user->removeGroupAllocation($newGroupAllocation);
            $errors = array();
            
            foreach ($validation as $error) {
                $errors[] = $error->getMessage();//__toString();
            }

            return implode('; ', $errors);
        }

        if ($andFlush) {
            $this->ObjectManager->flush();
        }

        return self::NO_ERROR;
    }

    public function addGroupAllocationToUsers(Group $group, array $users)
    {
        foreach ($users as $user) {
            $this->addGroupAllocationToUser($group, $user, false);
        }

        $this->objectManager->flush();
    }

    public function removeGroupAllocationToUser($group, UserInterface $user, \DateTime $endDate = null)
    {
        $groupAllocationToDelete = null;

        if ($group instanceof Group) {
            foreach ($user->getGroupAllocationsForDate($endDate) as $groupAllocation) {
                if ($groupAllocation->getGroup() == $group) {
                    $groupAllocationToDelete = $groupAllocation;
                }
            }
        }
        elseif ($group instanceof GroupAllocation) {
            if ($this->periodHandler->containsDate($group, $endDate)) {// null == $group->getEndDate() or $endDate < $group->getEndDate()) {
                $groupAllocationToDelete = $group;
            }
        }
        else {
            throw new \Exception("Group must be an instance of Group or GroupAllocation");
        }

        if (null == $groupAllocationToDelete) {
            return self::NO_ACTION;
        }

        if (null == $endDate) {
            $user->removeGroupAllocation($groupAllocationToDelete);
        }
        else {
            $groupAllocationToDelete->setEndDate($endDate);
        }

        $validation = $this->validator->validate($groupAllocationToDelete, array('admin'));
        
        foreach ($this->validator->validateValue($user->getGroupAllocations(), new GroupsNumberNotNull()) as $violation) {
            $validation->add($violation);
        }

        if ($validation->count() > 0){
            /* foreach ($groupAllocationsToDelete as $groupAllocationToDelete) { */
            $this->ObjectManager->detach($groupAllocationToDelete);
            $this->ObjectManager->detach($user);

            $errors = array();
            foreach ($validation as $error) {
                $errors[] = $error->getMessage();
            }

            return implode('; ', $errors);
        }

        if (null == $endDate) {
            $this->objectManager->remove($groupAllocationToDelete);
        }

        return self::NO_ERROR;
    }

    public function removeGroupAllocationToUsers(Group $group, array $users)
    {
        foreach ($users as $user) {
            $this->removeGroupAllocationToUser($group, $user, false);
        }

        $this->objectManager->flush();
    }


    /**
     * Generate a username from user's surname and firstname
     *
     * Construction : part of firstname, SEP_CHAR, NB_CHAR_SURNAME_PART first letters of surname +- a number, so the username is unique in the database
     *
     * @param UserInterface $user
     *
     * @return string
     */
    public function generateUsername(UserInterface $user)
    {
        $i = 1;
        
        do
        {
            $username = strtolower(substr($user->getSurname(), 0, self::NB_CHAR_SURNAME_PART));
            if (null != $user->getFirstname())
            {
                if ($i <= strlen($user->getFirstname())) {
                    $username = strtolower(substr($user->getFirstname(),0,$i)).self::SEP_CHAR.$username;
                }
                else {
                    $username = strtolower($user->getFirstname()).self::SEP_CHAR.$username.$i;
                }
            }
            elseif ($i > 1)
            {
                $username = $username.$i;
            }

            $username = trim($username);

            /************************************************************/
            /* /!\ OPTIMISATION (one request to database for each loop) */
            /************************************************************/
            $nbUsersSameUsername = $this->repository->getCountByUsername($username);
            
            $i ++;
        }
        while ($nbUsersSameUsername > 0);

        return $username;
    }

    
    private function updateExpirationDate(UserInterface $user)//, $andFlush  =true)
    {
        $maxEndDate = null;

        foreach ($user->getGroupAllocations() as $groupAllocation) {
            $endDate = $groupAllocation->getEndDate();
            if (null != $endDate) {
                if ($endDate > $maxEndDate or null == $maxEndDate) {
                    $maxEndDate = $endDate;
                }
            }
            else {
                $maxEndDate = null;
                break;
            }
        }

        $user->setExpiresAt($maxEndDate);

        return $this;
    }


    /**
     * Allocation of some role(s) for some group(s) to a user
     *
     * @param UserInterface $user
     * @param array $roleNames
     * @param array $groups
     */
    public function allocateRolesToUser(UserInterface $user, array $roleNames, array $groups)
    {
        $roles = $this->objectManager->getRepository('GessehUserBundle:Role')->getByNames($roleNames);
        
        foreach ($groups as $group) {
            foreach ($roles as $role) {
                $roleApplication = new RoleApplication();
                $roleApplication->setRole($role);
                $roleApplication->setObjectType(implode('', array_slice(explode('\\', get_class($group)), -1)));
                $roleApplication->setObjectId($group->getId());
            
                $user->addRoleApplication($roleApplication);
            }
        }

        return $this;
    }
}