<?php

namespace Gesseh\UserBundle\Doctrine;

use Gesseh\UserBundle\Entity\RoleApplication;


class RoleApplicationManager
{
    private $groupManager;


    public function __construct(GroupManager $groupManager)
    {
        $this->groupManager = $groupManager;
    }

    
    private function getObject(RoleApplication $roleApplication)
    {
        switch ($roleApplication->getObjectType()) {
          case 'Group':
              return $this->groupManager->getRepository()->find($roleApplication->getObjectId()); 
              break;

          case 'User':
              break;
        }

        return null;
    }


    private function getObjectTitle(RoleApplication $roleApplication)
    {
        $object = $this->getObject($roleApplication);

        switch ($roleApplication->getObjectType()) {
          case 'Group':
              return $object->getCompleteTitle();
              break;
          case 'User':
              break;
        }

        return null;
    }


    /* public function getRoleAppsDescription(array $roleApplications) */
    /* { */
    /*     $roleAppsDescription = array(); */

    /*     foreach ($roleApplications as $roleApplication) { */
    /*         $roleAppsDescription[$roleApplication->getId()] = $this->getObjectDescription($roleApplication)." : ".$roleApplication->getRole()->getTitle(); */
    /*     } */

    /*     return $roleAppsDescription; */
    /* } */

    public function getRoleAndObject(RoleApplication $roleApplication, $title = false)
    {
        if ($title) {
            return array(
                'role' => $roleApplication->getRole()->getTitle(),
                'object' => $this->getObjectTitle($roleApplication)
            );
        }
        else {
            return array(
                'role' => $roleApplication->getRole(),
                'object' => $this->getObject($roleApplication)
            );
        }
    }


    public function getRolesAndObjects(array $roleApplications, $title = false)
    {
        $rolesAndObjects = array();

        foreach ($roleApplications as $roleApplication) {
            $rolesAndObjects[$roleApplication->getId()] = $this->getRoleAndObject($roleApplication, $title);
        }

        return $rolesAndObjects;
    }
}