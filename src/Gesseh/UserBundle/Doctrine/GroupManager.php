<?php

namespace Gesseh\UserBundle\Doctrine;

use FOS\UserBundle\Doctrine\GroupManager as BaseGroupManager;
use Doctrine\Common\Persistence\ObjectManager;
use Gesseh\UserBundle\Service\GroupsHierarchy\GroupsHierarchy;
use Gesseh\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\GroupInterface;
use Gesseh\UserBundle\Entity\UserGroupAllocation;

class GroupManager extends BaseGroupManager
{
    private $groupsHierarchy;
    private $userManager;

    public function __construct(ObjectManager $om, GroupsHierarchy $groupsHierarchy, UserManager $userManager)
    {
        parent::__construct($om, "Gesseh\UserBundle\Entity\Group");
        $this->groupsHierarchy = $groupsHierarchy;
        $this->userManager = $userManager;
    }

    /**
     * Parent function redefined
     */
    public function deleteGroup(GroupInterface $group, $andFlush = true, $deleteUsers = false, GroupInterface $newGroup = null)
    {
        if ($deleteUsers) {
            foreach ($group->getGroupAllocations() as $groupAllocation) {
                $user = $groupAllocation->getUser();
                $this->userManager->deleteUser($user);
            }
        }
        else {
            if (null == $newGroup) {
                $newGroup = $group->getParent();
            }
            foreach ($group->getGroupAllocations() as $groupAllocation) {
                $user = $groupAllocation->getUser();
                $user->removeGroupAllocation($groupAllocation);
                $this->userManager->addGroupAllocationToUser($newGroup, $user, false);
                /* $newUserMemberGroup = new UserMemberGroup(); */
                /* $newUserMemberGroup->setGroup($newMemberGroup); */
                /* /\* $newUserMemberGroup->setUser($user); *\/ */
                /* $user->addMemberGroup($newUserMemberGroup); */
            }
        }
        
        $this->objectManager->remove($group);
        if ($andFlush) {
            $this->objectManager->flush();
        }
    }

    /**
     * Parent function redefined
     * Updates a group
     *
     * @param GroupInterface $group
     * @param Boolean        $andFlush Whether to flush the changes (default true)
     */
    public function updateGroup(GroupInterface $group, $andFlush = true)
    {
        if (null == $group->getTitle()) {
            $group->setTitle($group->getName());
        }
        $this->objectManager->persist($group);
        if ($andFlush) {
            $this->objectManager->flush();
        }
    }
    
    /**
     * Cascading deletion of groups 
     *
     * @return string
     */
    public function deleteGroupsCascade(GroupInterface $group, $andFlush = true, $deleteUsers = false, GroupInterface $newGroup)
    {
        $childrenGroups = $this->groupsHierarchy->getDirectReachableGroups($group);
        $deletedGroupsName = '';
        $separator = '';
        if (null != $childrenGroups) {
            foreach ($childrenGroups as $childGroup) {
                $deletedGroupsName .= $separator.$this->deleteGroupsCascade($childGroup, false, $deleteUsers, $newGroup);
                $separator = ', ';
            }
        }
        $deletedGroupsName .= $separator.$group->__toString();
        $this->deleteGroup($group, $andFlush, $deleteUsers, $newGroup);

        return $deletedGroupsName;
    }

    public function releaseChildrenGroups(GroupInterface $group, $andFlush = true)
    {
        $childrenGroups = $this->groupsHierarchy->getDirectReachableGroups($group);
        if (null != $childrenGroups) {
            foreach ($childrenGroups as $childGroup) {
                $childGroup->setParent($group->getParent());
            }
        }

        return $this;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    /* public function getGroupsTree(array $groups, Group $originGroup = null) */
    /* { */
    /*     $groupsTree = array(); */

    /*     foreach ($groups as $group) { */
    /*         if ($originGroup == $group->getParent()) { */
    /*             $groupsTree[] = array ( */
    /*                 'node' => $group, */
    /*                 'branch' => self::getGroupsTree($groups, $group) */
    /*             ); */
    /*         } */
    /*     } */

    /*     return $groupsTree; */
    /* } */
}