<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Gesseh\UserBundle\Entity\UserPriority;

/**
 * UserPriorityType
 */
class UserPriorityType extends AbstractType
{
    private $users;
    
    public function __construct(array $users)
    {
        $this->users = $users;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', 'entity', array(
                      'class' => 'GessehUserBundle:User',
                      'choices' => $this->users,
                      'property' => 'name',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'label' => "User"
                  ))
            ->add('type', 'choice', array(
                      'choices' => UserPriority::getPriorityTypes(),
                      'required' => true,
                      'multiple' => false,
                      'expanded' => false,
                      'empty_value' => "--none--",
                      'label' => "Type"
                  ))
            ->add('priority', 'integer', array(
                      'required' => true,
                      'precision' => 0,
                      'label' => "Priority"
                  ))
            ->add('startDate', 'date', array(
                      'required' => false,
                      'widget' => 'single_text',
                      'format' => 'dd/MM/yyyy',
                      'label' => "Start date"
                  ))
            ->add('endDate', 'date', array(
                      'required' => false,
                      'widget' => 'single_text',
                      'format' => 'dd/MM/yyyy',
                      'label' => "End date"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ))
            ->add('reset', 'reset', array(
                      'label' => "Reset"
                  ));
    }

    public function getName()
    {
        return 'user_priority';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\UserBundle\Entity\UserPriority',
            ));
    }
}
