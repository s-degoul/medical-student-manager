<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ConfirmDeleteGroupType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('deleteChildrenGroups', ChoiceType::class, array(
                      'choices' => array(
                          "No" => false,
                          "Yes" => true
                      ),
                      'choices_as_values' => true,
                      'required' => true,
                      'multiple' => false,
                      'expanded' => true,
                      'mapped' => false,
                      'placeholder' => false,
                      'data' => false,
                      'label' => "Delete the children groups ?"
                  ))
            ->add('deleteUsers', ChoiceType::class, array(
                      'choices' => array(
                          "No" => false,
                          "Yes" => true
                      ),
                      'choices_as_values' => true,
                      'required' => true,
                      'multiple' => false,
                      'expanded' => true,
                      'mapped' => false,
                      'placeholder' => false,
                      'data' => false,
                      'label' => "Delete the users of these groups ?"
                  ))
            ->add('confirm', 'submit', array('label' => "Confirm"));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        /* $resolver->setDefaults(array( */
        /*     'data_class' => 'Gesseh\UserBundle\Entity\Group' */
        /* )); */
        return true;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'confirm_delete_group';
    }
}
