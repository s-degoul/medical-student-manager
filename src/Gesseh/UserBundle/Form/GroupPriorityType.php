<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Gesseh\UserBundle\Entity\GroupPriority;

/**
 * GroupPriorityType
 */
class GroupPriorityType extends AbstractType
{
    private $groups;
    
    public function __construct(array $groups)
    {
        $this->groups = $groups;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('group', 'entity', array(
                      'class' => 'GessehUserBundle:Group',
                      'choices' => $this->groups,
                      'property' => 'completeTitle',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'label' => "Group"
                  ))
            ->add('type', 'choice', array(
                      'choices' => GroupPriority::getPriorityTypes(),
                      'required' => false,
                      'multiple' => false,
                      'expanded' => false,
                      'empty_value' => "none",
                      'label' => "Type"
                  ))
            ->add('priority', 'integer', array(
                      'required' => true,
                      'precision' => 0,
                      'label' => "Priority"
                  ))
            ->add('startDate', 'date', array(
                      'required' => false,
                      'widget' => 'single_text',
                      'format' => 'dd/MM/yyyy',
                      'label' => "Start date"
                  ))
            ->add('endDate', 'date', array(
                      'required' => false,
                      'widget' => 'single_text',
                      'format' => 'dd/MM/yyyy',
                      'label' => "End date"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ));
    }

    public function getName()
    {
        return 'group_priority';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\UserBundle\Entity\GroupPriority',
            ));
    }
}
