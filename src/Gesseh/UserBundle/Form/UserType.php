<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('surname', 'text', array (
                      'required' => true,
                      'label' => "Surname"
                  ))
            ->add('firstname', 'text', array(
                      'required' => false,
                      'label' => "Firstname"
                  ))
            ->add('title', ChoiceType::class, array(
                      'choices' => array(
                          "M" => 'M',
                          "Mme" => 'Mme',
                          "Dr" => 'Dr',
                          "Pr" => 'Pr'
                      ),
                      'choices_as_values' => true,
                      'required' => false,
                      'multiple' => false,
                      'expanded' => false,
                      'label' => "Title"
                  ))
            ->add('username', 'text', array(
                      'label' => 'Username',
                      'required' => false,
                      'translation_domain' => 'FOSUserBundle'
                  ))
            ->add('plainPassword', 'repeated', array(
                      'type' => 'password',
                      'options' => array('translation_domain' => 'FOSUserBundle'),
                      'first_options' => array('label' => 'Password'),
                      'second_options' => array('label' => 'Password confirmation'),
                      'invalid_message' => 'fos_user.password.mismatch',
                  ))
            ->add('email', 'email', array(
                      'label' => 'Email',
                      'translation_domain' => 'FOSUserBundle'
                  ))
            ->add('displayEmail', 'checkbox', array(
                      'required' => false,
                      'label' => "Display email ?"
                  ))            
            ->add('phone', 'text', array(
                      'required' => false,
                      'label' => "Phone number"
                  ))
            ->add('displayPhone', 'checkbox', array(
                      'required' => false,
                      'label' => "Display phone number ?"
                  ))
            ->add('street', 'text', array(
                      'required' => false,
                      'label' => "Street (& number)"
                  ))
            ->add('postcode', 'text', array(
                      'required' => false,
                      'label' => "Postcode"
                  ))
            ->add('city', 'text', array(
                      'required' => false,
                      'label' => "City"
                  ))
            ->add('country', 'country', array(
                      'required' => false,
                      'label' => "Country"
                  ))
            ->add('groupAllocations', 'collection', array(
                      'type' => 'group_allocation',
                      'allow_add' => true,
                      'allow_delete' => true,
                      'by_reference' => false,
                  ))
            ->add('enabled', ChoiceType::class, array(
                      'choices'  => array(
                          'Yes' => true,
                          'No' => false,
                      ),
                      'choices_as_values' => true,
                      'required' => true,
                      'placeholder' => false,
                      'expanded' => true,
                      'multiple' => false,
                      'label' => "Active ?"
                  ))
            ->add('defaultUserRoles', ChoiceType::class, array(
                      'choices'  => array(
                          'Yes' => true,
                          'No' => false,
                      ),
                      'choices_as_values' => true,
                      'mapped' => false,
                      'required' => false,
                      'placeholder' => false,
                      'expanded' => true,
                      'multiple' => false,
                      'data' => true,
                      'label' => "Allocate default user roles ?"
                  ))
            ->add('sendEmail', ChoiceType::class, array(
                      'choices'  => array(
                          'Yes' => true,
                          'No' => false,
                      ),
                      'choices_as_values' => true,
                      'mapped' => false,
                      'required' => false,
                      'placeholder' => false,
                      'expanded' => true,
                      'multiple' => false,
                      'data' => true,
                      'label' => "Send an email ?"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ))
            ->add('saveAndAdd', 'submit', array(
                      'label' => "Save and add"
                  ))
            ->add('reset', 'reset', array(
                      'label' => "Reset"
                  ));
    }

    /* public function getParent() */
    /* { */
    /*     return 'fos_user_registration'; */
    /* } */
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\UserBundle\Entity\User',
                'validation_groups' => array('admin')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user';
    }
}
