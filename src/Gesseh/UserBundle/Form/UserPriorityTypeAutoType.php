<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


/**
 * UserPriorityTypeAutoType
 */
class UserPriorityTypeAutoType extends AbstractType
{
    private $users;

    public function __construct(array $users)
    {
        $this->users = $users;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('type');
    }

    public function getParent()
    {
        return new UserPriorityType($this->users);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_priority_type_auto';
    }
}