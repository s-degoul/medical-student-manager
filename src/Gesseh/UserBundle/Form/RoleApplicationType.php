<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

use Gesseh\UserBundle\Entity\Role;
use Gesseh\UserBundle\Entity\RoleApplication;
use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;


class RoleApplicationType extends AbstractType
{
    private $roleChecker;

    public function __construct(RoleChecker $roleChecker)
    {
        $this->roleChecker = $roleChecker;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('role', 'entity', array(
                      'class' => 'GessehUserBundle:Role',
                      'choices' => $this->roleChecker->getReachableRoles(),
                      'property' => 'title',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'empty_data' => null,
                      'label' => "Role"
                  ));


        $formModifier = function (FormInterface $form, Role $role = null) {
            $form->add('group', 'entity', array(
                           'class' => 'GessehUserBundle:Group',
                           'choices' => $this->roleChecker->getReachableObjects($role, 'Group'),
                           'property' => 'completeTitle',
                           'required' => true,
                           'mapped' => false,
                           'multiple' => false,
                           'expanded' => false,
                           'empty_value' => '--choose--',
                           'empty_data' => null,
                           'label' => "Group"
                           ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $role = null;
                if ($event->getData() instanceof RoleApplication) {
                    $role = $event->getData()->getRole();
                }
                $formModifier($event->getForm(), $role);
            }
        );

        $builder->get('role')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $role = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $role);
            }
        );
                
        $builder->add('save', 'submit', array('label' => "Save"));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\UserBundle\Entity\RoleApplication',
                'validation_groups' => array('admin')
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'role_application';
    }
}
