<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;


class ImportUsersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', 'file', array(
                      'required' => true,
                      'label' => "File"
                  ))
            ->add('groupAllocation', 'group_allocation', array(
                      'required' => true,
                      'label' => "Group allocation"
                  ))
            ->add('defaultUserRoles', ChoiceType::class, array(
                      'choices'  => array(
                          'Yes' => true,
                          'No' => false,
                      ),
                      'choices_as_values' => true,
                      'mapped' => false,
                      'required' => false,
                      'placeholder' => false,
                      'expanded' => true,
                      'multiple' => false,
                      'data' => true,
                      'label' => "Allocate default user roles ?"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ))
            ->add('saveAndAdd', 'submit', array(
                      'label' => "Save and add"
                  ))
            ->add('reset', 'reset', array(
                      'label' => "Reset"
                  ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        /* $resolver->setDefaults( */
        /*     array( */
        /*         'data_class' => 'Gesseh\UserBundle\Entity\User'             */
        /* )); */
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return 'import_users';
    }
}
