<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('username')
            ->remove('surname')
            ->remove('firstname')
            ->remove('plainPassword')
            ->remove('groupAllocations')
            ->remove('enabled')
            ->remove('sendEmail')
            ->remove('defaultUserRoles')
            ->remove('saveAndAdd')
            ->remove('reset')
            ;
    }

    public function getParent()
    {
        return new UserType();
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\UserBundle\Entity\User',
                'validation_groups' => array('profile')                   
        ));
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return 'user_username_password_group_auto';
    }
}
