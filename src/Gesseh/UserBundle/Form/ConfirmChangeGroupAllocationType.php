<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;

class ConfirmChangeGroupAllocationType extends AbstractType
{
    private $roleChecker;

    public function __construct(RoleChecker $roleChecker)
    {
        $this->roleChecker = $roleChecker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('group', 'entity', array(
                      'class' => 'GessehUserBundle:Group',
                      'mapped' => false,
                      'choices' => $this->roleChecker->getReachableObjects('ROLE_ADMIN_USER', 'Group'),
                      'property' => 'completeTitle',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'empty_data' => null,
                      'label' => "Group"
                  ))
            /* ->add('memberGroups', 'collection', array( */
            /*           'type' => 'user_member_group', */
            /*           'allow_add' => true, */
            /*           'allow_delete' => false, */
            /*           'by_reference' => false */
            /*       )) */
            ->add('action', 'choice', array(
                      'required' => true,
                      'choices' => array(
                          'add' => "add this group",
                          'remove' => "remove this group"
                      ),
                      'multiple' => false,
                      'expanded' => true,
                      'mapped' => false,
                      'label' => "Action to perform"
                  ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'confirm_change_group_allocation';
    }
}
