<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;

class ChangeUsersGroupAllocationType extends AbstractType
{
    private $roleChecker;
    private $users;

    public function __construct(RoleChecker $roleChecker, array $users)
    {
        $this->roleChecker = $roleChecker;
        $this->users = $users;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $currentGroups = array();
        foreach ($this->users as $user) {
            foreach ($user->getGroupAllocationsForDate(new \DateTime('now')) as $groupAllocation) {
                if (! in_array($groupAllocation->getGroup(), $currentGroups)) {
                    $currentGroups[] = $groupAllocation->getGroup();
                }
            }
        }

        $defaultDate = new \DateTime('now');
        
        $builder
            ->add('oldGroup', 'entity', array(
                      'class' => 'GessehUserBundle:Group',
                      'mapped' => false,
                      'choices' => $currentGroups,
                      'property' => 'completeTitle',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => false,
                      'empty_value' => '--choose--',
                      'empty_data' => null,
                      'label' => "Group to removed"
                  ))
            ->add('deleteGroup', 'checkbox', array(
                      'mapped' => false,
                      'required' => false,
                      'data' => false,
                      'label' => "Delete this group allocation ?"
                  ))
            ->add('newGroup', 'entity', array(
                      'class' => 'GessehUserBundle:Group',
                      'mapped' => false,
                      'choices' => $this->roleChecker->getReachableObjects('ROLE_ADMIN_USER', 'Group'),
                      'property' => 'completeTitle',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => false,
                      'empty_value' => '--choose--',
                      'empty_data' => null,
                      'label' => "New group"
                  ))
            ->add('dateChange', 'date', array(
                      'widget' => 'single_text',
                      'format' => 'dd/MM/yyyy',
                      'data' => $defaultDate,
                      'mapped' => false,
                      'required' => false,
                      'label' => "Date of change"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Submit"
                  ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'change_users_group_allocation';
    }
}
