<?php

namespace Gesseh\UserBundle\Form\Handler;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use Gesseh\UserBundle\Entity\User;

class UserHandler
{
    private $form;
    private $request;
    private $em;
    private $um;
    
    public function __construct(Form $form, Request $request, EntityManager $em, UserManager $um)
    {
        $this->form = $form;
        $this->request = $request;
        $this->em = $em;
        $this->um = $um;
    }

    public function process()
    {
        if ($this->request->getMethod() == 'POST')
        {
            $this->form->bind($this->request);

            if ($this->form->isValid())
            {
                $this->onSuccess($this->form->getData());
                return true;
            }
        }

        return false;
    }

    public function onSuccess(User $user)
    {
        if (null == $user->getId())
        {
            $user->setUsername($this->generateUsername($user));
            $user->setPlainPassword($this->generatePassword(8));
        }
        
        $this->um->updateUser($user);
    }

    private function generateUsername(User $user)
    {
        
    }

    private function generatePwd($length)
    {
        $lowerCaseLetters = array ('a','z','e','r','t','y','u','i','o','p','q','s','d','f','g','h','j','k','l','m','w','x','c','v','b','n');
        $numerals = array('0','1','2','3','4','5','6','7','8','9');
        $capitalLetters = array('A','Z','E','R','T','Y','U','I','O','P','S','D','F','G','H','J','K','L','M','W','X','C','V','B','N');
        $specialCharacters = array(',',';',':','!','*','µ','%','é','è','à');
        
        $password = '';

        for ($i = 0 ; $i < $length ; $i++) {
            $rand = array_rand($characters);
            $password .= $characters[$rand];
        }

        return $password;
    }
}