<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserNewType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('plainPassword')
            ;
    }

    public function getParent()
    {
        return UserType::class;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_username_password_auto';
    }
}
