<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;
use Gesseh\UserBundle\Service\GroupsHierarchy\GroupsHierarchy;

class UpgradeGroupAllocationType extends AbstractType
{
    private $roleChecker;
    private $groupsHierarchy;
    private $users;

    public function __construct(RoleChecker $roleChecker, GroupsHierarchy $groupsHierarchy, array $users)
    {
        $this->roleChecker = $roleChecker;
        $this->groupsHierarchy = $groupsHierarchy;
        $this->users = $users;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $groups = array();
        foreach ($this->users as $user) {
            foreach ($user->getGroupAllocations() as $groupAllocation) {
                if (! in_array($groupAllocation->getGroup(), $groups)) {
                    $groups[] = $groupAllocation->getGroup();
                }
            }
        }

        $familyGroups = $this->groupsHierarchy->getFamilies($groups);

        foreach ($familyGroups as $key => $familyGroup) {
            if (! $this->roleChecker->hasRole('ROLE_ADMIN_USER', $familyGroup)) {
                unset ($familyGroups[$key]);
            }
        }

        $defaultDate = new \DateTime('now');
        
        $builder
            ->add('familyGroup', 'entity', array(
                      'class' => 'GessehUserBundle:Group',
                      'mapped' => false,
                      'choices' => $familyGroups,
                      'property' => 'completeTitle',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => true,
                      /* 'empty_value' => '--choose--', */
                      'label' => "Family group concerned"
                  ))
            ->add('dateChange', 'date', array(
                      'widget' => 'single_text',
                      'format' => 'dd/MM/yyyy',
                      'data' => $defaultDate,
                      'mapped' => false,
                      'required' => false,
                      'label' => "Date of change"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Submit"
                  ));;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'upgrade_users_group_allocation';
    }
}
