<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('plainPassword')
            ->remove('sendEmail')
            ->remove('defaultUserRoles')
            ->remove('saveAndAdd')
            ->remove('reset')
            ;
    }

    public function getParent()
    {
        return new UserType();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_password_auto';
    }
}
