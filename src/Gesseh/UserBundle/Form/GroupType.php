<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;

class GroupType extends AbstractType
{
    private $roleChecker;

    public function __construct(RoleChecker $roleChecker)
    {
        $this->roleChecker = $roleChecker;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('required' => true, 'label' => "Name"))
            ->add('title', 'text', array('required' => false, 'label' => "Title"))
            ->add('parent', 'entity', array(
                      'class' => 'GessehUserBundle:Group',
                      'choices' => $this->roleChecker->getReachableObjects('ROLE_ADMIN_USER', 'Group'),
                      'property' => 'completeTitle',
                      'multiple' => false,
                      'expanded' => false,
                      'label' => "Parent group",
                      'required' => true,
                      'empty_value' => '--choose--'
                  ))
            ->add('family', ChoiceType::class, array(
                      'choices'  => array(
                          'Yes' => true,
                          'No' => false,
                      ),
                      'choices_as_values' => true,
                      'required' => false,
                      'expanded' => true,
                      'multiple' => false,
                      'label' => "Family group ?"
                  ))
            ->add('stage', 'integer', array(
                      'required' => false,
                      'precision' => 0,
                      'label' => "Rank in the family"
                  ))
            ->add('save', 'submit', array('label' => "Save"))
            ->add('saveAndAdd', 'submit', array('label' => "Save and add"))
            ->add('reset', 'reset', array('label' => "Reset"))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gesseh\UserBundle\Entity\Group'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'group';
    }
}
