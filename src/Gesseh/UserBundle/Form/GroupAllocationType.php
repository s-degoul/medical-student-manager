<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
/* use Symfony\Component\Form\FormEvents; */
/* use Symfony\Component\Form\FormEvent; */
use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;


class GroupAllocationType extends AbstractType
{
    private $roleChecker;

    public function __construct(RoleChecker $roleChecker)
    {
        $this->roleChecker = $roleChecker;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('group', 'entity', array(
                      'class' => 'GessehUserBundle:Group',
                      'choices' => $this->roleChecker->getReachableObjects('ROLE_ADMIN_USER', 'Group'),
                      'property' => 'completeTitle',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => false,
                      'empty_value' => '--choose--',
                      'empty_data' => null,
                      'label' => "Group"
                  ))
            -> add('startDate', 'date', array(
                       'widget' => 'single_text',
                       /* 'time_widget' => 'choice', */
                       'format' => 'dd/MM/yyyy',
                       /* 'with_seconds' => false, */
                       'required' => false,
                       'label' => "Start"
                   ))
            -> add('endDate', 'date', array(
                       'widget' => 'single_text',
                       'format' => 'dd/MM/yyyy',
                       /* 'data' => new \DateTime(), */
                       'required' => false,
                       'label' => "End"
                   ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\UserBundle\Entity\GroupAllocation',
                'validation_groups' => array('admin')
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'group_allocation';
    }
}
