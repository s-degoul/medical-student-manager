<?php

namespace Gesseh\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


/**
 * GroupPriorityTypeAutoType
 */
class GroupPriorityTypeAutoType extends AbstractType
{
    private $groups;

    public function __construct(array $groups)
    {
        $this->groups = $groups;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('type');
    }

    public function getParent()
    {
        return new GroupPriorityType($this->groups);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'group_priority_type_auto';
    }
}