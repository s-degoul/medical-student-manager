<?php

namespace Gesseh\UserBundle\Twig\Extensions;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
/* use Gesseh\UserBundle\Service\AdminGroup\AdminGroup; */
use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;
use Gesseh\UserBundle\Entity\Group;
use Gesseh\CoreBundle\Service\PeriodHandler\PeriodHandler;

class GroupsTreeExtension extends \Twig_Extension
{
    private $urlGenerator;
    /* private $adminGroup; */
    private $roleChecker;
    private $userDisplayExtension;
    private $periodHandler;

    public function __construct(UrlGeneratorInterface $urlGenerator, RoleChecker $roleChecker, UserDisplayExtension $userDisplayExtension, PeriodHandler $periodHandler)
    {
        $this->urlGenerator = $urlGenerator;
        /* $this->adminGroup = $adminGroup; */
        $this->roleChecker = $roleChecker;
        $this->userDisplayExtension = $userDisplayExtension;
        $this->periodHandler = $periodHandler;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('groups_tree', array($this, 'GroupsTree'), array('is_safe' => array('html')))
        );
    }

    public function getName()
    {
        return 'groups_tree_extension';
    }

    public function GroupsTree(array $groups, Group $originGroup = null, array $users = null, $adminGroups = true, $adminUsers = true, $displayBaseGroup = true, \DateTime $date = null)
    {
        if (null == $date) {
            $date = new \DateTime('now');
        }
        
        $displayedTree = '';
        if (! $displayBaseGroup) {
            foreach ($groups as $key => $group) {
                if ($group->getName() == 'base_group') {
                    unset ($groups[$key]);
                    break;
                }
            }
        }

        foreach ($groups as $group) {
            if ($originGroup == $group->getParent() or (null == $originGroup and !in_array($group->getParent(), $groups))) {
                $css = $group->isFamily() ? 'tree_group_family' : '';
                $displayedTree .= '<li class="tree_group '.$css.'">'."\n";
                $displayedTree .= '<input type="checkbox" name="group_'.$group->getId().'" id="group_'.$group->getId().'" value=1 />'."\n";
                $displayedTree .= '<span>'."\n";
                $displayedTree .= '<label for="group_'.$group->getId().'">'.$group->__toString().'</label>'."\n";
                
                if ($adminGroups and null != $group->getParent() and $this->roleChecker->hasRole('ROLE_ADMIN_USER',$group->getParent())) {
                    $displayedTree .= '<span class="action">'."\n";
                    
                    $displayedTree .= '<a href = "'.$this->urlGenerator->generate('GUser_AdminEditGroup', array('id' => $group->getId())).'" title="Edit">'
                .'<span class="glyphicon glyphicon-edit"></span>'
                .'</a>'."\n";

                    $displayedTree .= '<a href="'.$this->urlGenerator->generate('GUser_AdminDeleteGroup', array('id' => $group->getId())).'" title="Delete">'
                .'<span class="glyphicon glyphicon-trash"></span>'
                .'</a>'."\n";
                    
                    $displayedTree .= '</span>'."\n";
                }

                $displayedTree .= '</span>'."\n";

                if (null != $users) {
                    $branch = '';
                    foreach ($users as $user) {
                        foreach ($this->periodHandler->getLastPeriods($user->getGroupAllocations()->toArray(), $date) as $groupAllocation) {
                            if ($groupAllocation->getGroup() == $group) {
                                if ($user->getGroupAllocationsForDate($date)->contains($groupAllocation)) {
                                    $branch .= '<li class="tree_user">'.$this->userDisplayExtension->UserDisplay($user, $adminUsers).'</li>';
                                }
                                elseif ($adminUsers) {
                                    $branch .= '<li class = "tree_user inactive">'.$this->userDisplayExtension->UserDisplay($user, $adminUsers).'</li>'."\n";
                                }
                            }
                        }
                    }
                    if ($branch != '') {
                        $displayedTree .= '<ul class="expanded">'."\n".$branch.'</ul>'."\n";
                    }
                }

                $branch = $this->GroupsTree($groups, $group, $users, $adminGroups, $adminUsers);
                if ($branch != '') {
                    $displayedTree .= $branch;
                }

                $displayedTree .= '</li>'."\n";
            }
        }

        if ($displayedTree != '') {
            $id = null;
            if (null == $originGroup) {
                $id = 'id = "tree"';
            }
            /* else { */
            /*     $id = 'expanded'; */
            /* } */
            $displayedTree = '<ul '.$id.'>'."\n".$displayedTree.'</ul>'."\n";
        }

        return $displayedTree;
    }
}