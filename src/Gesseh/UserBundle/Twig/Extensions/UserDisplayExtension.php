<?php

namespace Gesseh\UserBundle\Twig\Extensions;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Gesseh\UserBundle\Entity\User;

class UserDisplayExtension extends \Twig_Extension
{
    private $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('user_display', array($this, 'UserDisplay'), array('is_safe' => array('html')))
        );
    }

    public function getName()
    {
        return 'user_display_extension';
    }

    public function UserDisplay($user, $admin = false)
    {
        $displayedUser = '<input type = "checkbox" name = "user_'.$user->getId().'" id = "user_'.$user->getId().'" value = 1 />'."\n";
        $displayedUser .= '<label for = "user_'.$user->getId().'">'.$user->getName().'</label>'."\n";

        $displayedUser .= '<span class="action">'."\n";
            
        $displayedUser .= '<a href = "'.$this->urlGenerator->generate($admin?'GUser_AdminShowProfile':'GUser_ShowProfile', array('id' => $user->getId())).'" title="Show">'
            .'<span class="glyphicon glyphicon-info-sign"></span>'
            .'</a>';
        
        if (!$admin and $user->getDisplayEmail()) {
            $displayedUser .= '<a href = "mailto:'.$user->getEmail().'">'
                .'<span class="glyphicon glyphicon-envelope"> '.$user->getEmail().'</span>'
                .'</a>';
        }

        /* $separator = ' '; */
        /* foreach ($user->getGroups as $group) { */
        /*     $displayedUser .= $separator.$group->__toString(); */
        /*     $separator = ','; */
        /* } */

        if ($admin) {
            $displayedUser .= '<a href = "'.$this->urlGenerator->generate('GUser_AdminEditUser', array('id' => $user->getId())).'" title="Edit">'
                .'<span class="glyphicon glyphicon-edit"></span>'
                .'</a>';
            $displayedUser .= '<a href="'.$this->urlGenerator->generate('GUser_AdminDeleteUser', array('id' => $user->getId())).'" onclick=\'return confirm("Are you sure ?")\' title="Delete">'
                .'<span class="glyphicon glyphicon-trash"></span>'
                .'</a>';
            $displayedUser .= '<a href = "'.$this->urlGenerator->generate('GUser_AdminEditRoleApplications', array('id' => $user->getId())).'" title="Manage roles">'
                .'<span class="glyphicon glyphicon-lock"></span>'
                .'</a>';
        }

        $displayedUser .= '</span>'."\n";

        return $displayedUser;
    }
}