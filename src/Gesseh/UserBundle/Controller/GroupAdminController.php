<?php

namespace Gesseh\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Gesseh\UserBundle\Entity\Group;
use Gesseh\UserBundle\Form\GroupType;
use Gesseh\UserBundle\Form\ConfirmDeleteGroupType;


/**
 * @Route("/admin/group")
 * @Security("has_role('ROLE_ADMIN_USER')")
 */
class GroupAdminController extends Controller
{
    /**
     * @Route("/groups", name="GUser_AdminListGroups")
     * @Template()
     */
    public function listGroupsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $groups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_ADMIN_USER', 'Group');
        $groups = $this->get('gesseh_user.groups_hierarchy')->sortByStageByFamily($groups, 'asc');
        
        return array(
            'groups' => $groups
        );
    }


    /**
     * @Route("/new", name="GUser_AdminNewGroup")
     * @Template("GessehUserBundle:GroupAdmin:editGroup.html.twig")
     */
    public function newGroupAction(Request $request)
    {
        $group = new Group('');
        $form = $this->createForm(new GroupType($this->get('gesseh_user.role_checker')), $group, array(
                                      'cancel_action' => $this->generateUrl('GUser_AdminListGroups')
                                  ));
        $gm = $this->get('gesseh_user.group_manager');

        $form->handleRequest($request);

        if ($form->isValid()) {
            /* if (! $this->get('gesseh_user.admin_group')->checkAdminGroup($group->getParent())) { */
            /*     throw new AccessDeniedException("You are not an administrator of the group ".$group->getParent()->__toString()); */
            /* } */
                
            $gm->updateGroup($group);

            $this->get('session')->getFlashBag()->add('success', 'Registration of group "'.$group->__toString().'" completed');

            $nextAction = $form->get('save')->isClicked()?'GUser_AdminListGroups':'GUser_AdminNewGroup';
            return $this->redirect($this->generateUrl($nextAction));
        }

        return array(
            'form' => $form->createView()
        );
    }


    /**
     * @Route("/{id}/delete", name="GUser_AdminDeleteGroup", requirements={"id" = "\d+"})
     * @Template()
     */
    public function deleteGroupAction(Request $request, $id)
    {
        $gm = $this->get('gesseh_user.group_manager');
        
        $group = $gm->getRepository()->find($id);
        if (null == $group)
            throw $this->createNotFoundException("Unable to find this group");

        if (null == $group->getParent()) {
            throw new AccessDeniedException("You cannot modify the base group \"".$group->__toString()."\"");
        }
        elseif (! $this->get('gesseh_user.role_checker')->hasRole('ROLE_ADMIN_USER', $group->getParent())) {
            throw new AccessDeniedException("You are not an administrator of the group \"".$group->getParent()->__toString()."\"");
        }
        
        $form = $this->createForm(new ConfirmDeleteGroupType(), null, array(
                                      'cancel_action' => $this->generateUrl('GUser_AdminListGroups')
                                  ));
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            if ($form->get('deleteChildrenGroups')->getData() == false) {                    
                $gm->releaseChildrenGroups($group, false);
                $deletedGroupsName = $group->__toString();
                $gm->deleteGroup($group, true, $form->get('deleteUsers')->getData());
            }
            else {
                $deletedGroupsName = $gm->deleteGroupsCascade($group, true, $form->get('deleteUsers')->getData(), $group->getParent());
            }
                    
            $this->get('session')->getFlashBag()->add('success', 'Group(s) "'.$deletedGroupsName.'" removed from the database');

            return $this->redirect($this->generateUrl('GUser_AdminListGroups'));
        }

        return array(
            'form' => $form->createView()
        );
    }


    /**
     * @Route("/{id}/edit", name="GUser_AdminEditGroup", requirements={"id" = "\d+"})
     * @Template("GessehUserBundle:GroupAdmin:editGroup.html.twig")
     */
    public function editGroupAction(Request $request, $id)
    {
        $gm = $this->get('gesseh_user.group_manager');
        $group = $gm->getRepository()->find($id);
        
        if (null == $group) {
            throw $this->createNotFoundException("Unable to find this group");
        }

        if (null == $group->getParent()) {
            throw new AccessDeniedException("You cannot modify the base group \"".$group->__toString()."\"");
        }
        elseif (! $this->get('gesseh_user.role_checker')->hasRole('ROLE_ADMIN_USER', $group->getParent())) {
            throw new AccessDeniedException("You are not an administrator of the group \"".$group->getParent()->__toString()."\"");
        }
                
        $form = $this->createForm(new GroupType($this->get('gesseh_user.role_checker')), $group, array(
                                      'cancel_action' => $this->generateUrl('GUser_AdminListGroups')
                                  ));
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            if (null == $form->get('title')->getData()) {
                $group->setTitle($form->get('name')->getData());
            }
            $gm->updateGroup($group);

            $this->get('session')->getFlashBag()->add('success', 'Group "' . $group->__toString().'" updated');
            return $this->redirect($this->generateUrl('GUser_AdminListGroups'));
        }

        return array(
            'form' => $form->createView(),
            'group' => $group
        );
    }
}
