<?php

namespace Gesseh\UserBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Gesseh\UserBundle\Entity\UserPriority;
use Gesseh\UserBundle\Entity\GroupPriority;
use Gesseh\UserBundle\Form\UserPriorityType;
use Gesseh\UserBundle\Form\GroupPriorityTypeAutoType;


/**
 * @Route("/admin/priority")
 * @Security("has_role('ROLE_ADMIN_PRIORITY')")
 */
class PriorityAdminController extends Controller
{
    /**
     * @Route("/choose", name="GUser_AdminChoosePriorityType")
     * @Template()
     */
    public function choosePriorityTypeAction()
    {
        $userPriorityTypes = UserPriority::getPriorityTypes();
        $groupPriorityTypes = GroupPriority::getPriorityTypes();

        return array(
            'userPriorityTypes' => $userPriorityTypes,
            'groupPriorityTypes' => $groupPriorityTypes
        );
    }
    
    
    /**
     * @Route("/users/{userPriorityType}", name="GUser_AdminUsersPriorities", defaults={"userPriorityType" = 0})
     * @Template()
     */
    public function manageUsersPrioritiesAction(Request $request, $userPriorityType)
    {
        $em = $this->getDoctrine()->getManager();

        $postRequest = $request->request;

        $priorityType = $postRequest->get('userPriorityType', null);
        if (null == $priorityType) {
            $priorityType = $userPriorityType === 0 ? null : $userPriorityType;
        }

        if (null != $priorityType and ! array_key_exists ($priorityType, UserPriority::getPriorityTypes())) {
            throw new \Exception("'".$priorityType."' is not a valid type of priority");
        }
        

        $groups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_ADMIN_PRIORITY', 'Group');

        $users = $em->getRepository('GessehUserBundle:User')->getCompleteByGroupAndName($groups);

        $priorities = $em->getRepository('GessehUserBundle:UserPriority')->getByUsersTypeAndPeriod($users);

        
        $usersPriorities = array();
        foreach ($priorities as $priority) {
            $user = $priority->getUser();
            if (! array_key_exists($user->getId(), $usersPriorities)) {
                $usersPriorities[$user->getId()] = array('user' => $user,
                                                         'priorities' => array());
            }

            $usersPriorities[$user->getId()]['priorities'][] = $priority;
        }


        $newUserPriority = new UserPriority();
        $newUserPriority->setType($priorityType);

        $form = $this->createForm(new UserPriorityType($users), $newUserPriority);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($newUserPriority);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', "User priority registered for user ".$newUserPriority->getUser()->__toString());
            
            return $this->redirect($this->generateUrl('GUser_AdminUsersPriorities', array('userPriorityType' => $priorityType)));
        }

        return array(
            'usersPriorities' => $usersPriorities,
            'priorityType' => $priorityType,
            'form' => $form->createView()
        );
    }


    /**
     * @Route("/user/{id}/delete/{priorityType}", name="GessehUser_AdminDeleteUserPriority", requirements={"id" = "\d+"}, defaults={"priorityType" = 0})
     */
    public function deleteUserPriorityAction($id, $priorityType)
    {
        $em = $this->getDoctrine()->getManager();

        $userPriority = $em->getRepository('GessehUserBundle:UserPriority')->find($id);
        if (null == $userPriority) {
            throw $this->createNotFoundException("Unable to find this user priority");
        }

        $em->remove($userPriority);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', "User priority deleted");

        return $this->redirect($this->generateUrl('GUser_AdminUsersPriorities', array('userPriorityType' => $priorityType)));
    }


    /**
     * @Route("/groups/{groupPriorityType}", name="GUser_AdminGroupsPriorities", defaults={"groupPriorityType" = 0})
     * @Template()
     */
    public function manageGroupsPrioritiesAction(Request $request, $groupPriorityType)
    {
        $em = $this->getDoctrine()->getManager();

        $postRequest = $request->request;

        $priorityType = $postRequest->get('groupPriorityType', null);
        if (null == $priorityType) {
            $priorityType = $groupPriorityType === 0 ? null : $groupPriorityType;
        }

        if (null != $priorityType and ! in_array ($priorityType, GroupPriority::getPriorityTypes())) {
            throw new \Exception("'".$priorityType."' is not a valid type of priority");
        }

        $groups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_ADMIN_PRIORITY', 'Group');

        $priorities = $em->getRepository('GessehUserBundle:GroupPriority')->getByGroupsTypeAndPeriod($groups, $priorityType);

        $groupsPriorities = array();
        foreach ($priorities as $priority) {
            $group = $priority->getGroup();
            if (! array_key_exists($group->getId(), $groupsPriorities)) {
                $groupsPriorities[$group->getId()] = array('group' => $group,
                                                         'priorities' => array());
            }

            $groupsPriorities[$group->getId()]['priorities'][] = $priority;
        }


        $newGroupPriority = new GroupPriority();
        $newGroupPriority->setType($priorityType);

        $form = $this->createForm(new GroupPriorityTypeAutoType($groups), $newGroupPriority);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($newGroupPriority);
            $em->flush();

            return $this->redirect($this->generateUrl('GUser_AdminGroupsPriorities'));
        }

        return array(
            'groupsPriorities' => $groupsPriorities,
            'priorityType' => $priorityType,
            'form' => $form->createView()
        );
    }


    /**
     * @Route("/group/{id}/delete/{priorityType}", name="GessehUser_AdminDeleteGroupPriority", requirements={"id" = "\d+"})
     */
    public function deleteGroupPriorityAction($id, $priorityType)
    {
        $em = $this->getDoctrine()->getManager();

        $groupPriority = $em->getRepository('GessehUserBundle:GroupPriority')->find($id);
        if (null == $groupPriority) {
            throw $this->createNotFoundException("Unable to find this group priority");
        }

        $em->remove($groupPriority);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', "Group priority deleted");

        return $this->redirect($this->generateUrl('GUser_AdminGroupsPriorities', array('groupPriorityType' => $priorityType)));
    }
}