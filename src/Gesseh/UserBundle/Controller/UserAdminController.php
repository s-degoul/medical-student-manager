<?php

namespace Gesseh\UserBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/* use Symfony\Component\ExpressionLanguage\Expression; */

use Gesseh\UserBundle\Entity\User;
use Gesseh\UserBundle\Entity\GroupAllocation;
use Gesseh\UserBundle\Entity\Group;
use Gesseh\UserBundle\Entity\RoleApplication;
use Gesseh\UserBundle\Doctrine\UserManager;
use Gesseh\UserBundle\Form\UserNewType;
use Gesseh\UserBundle\Form\UserEditType;
use Gesseh\UserBundle\Form\ImportUsersType;
use Gesseh\UserBundle\Form\ChangeUsersGroupAllocationType;
use Gesseh\UserBundle\Form\UpgradeGroupAllocationType;
use Gesseh\UserBundle\Form\RoleApplicationType;
use Gesseh\UserBundle\Validator\Constraint\GroupsNumberNotNull;
use Gesseh\UserBundle\Service\Converter\CSVArrayConverter;


/**
 * Administration of the users
 *
 * @Route("/admin/user")
 * @Security("has_role('ROLE_ADMIN_USER')")
 */
class UserAdminController extends Controller
{
    // Number of characters in an system-generated password
    const NB_PASSWORD_CHAR = 12;
    // Max number of user's name to display in notice
    const MAX_NB_USERS_FLASHBAG = 3;
    // Max number of objects to persist before a flush toward the database
    const NB_USERS_FLUSH_BLOCK = 20;


    /**
     * List of the users for administration
     * 
     * Displays a list of the users that the token is allowed to manage, with proposed actions of administration
     *
     * @param Request $request
     *
     * @return array
     *
     * @Route("/users", name="GUser_AdminListUsers")
     * @Template()
     */
    public function listUsersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $postRequest = $request->request;
        
        // Groups reachable by the token, with the role ROLE_ADMIN_USER
        $groups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_ADMIN_USER', 'Group');
        $groups = $this->get('gesseh_user.groups_hierarchy')->sortByStageByFamily($groups, 'asc');

        // Id of groups to display
        $groupsIdDisplay = array();
        foreach ($groups as $group) {
            if (null != $postRequest->get('group'.$group->getId(), null)) {
                $groupsIdDisplay[] = $group->getId();
            }
        }
        if (empty ($groupsIdDisplay)) {
            foreach ($groups as $group) {
                $groupsIdDisplay[] = $group->getId();
            }
        }

        // Criteria of search
        $searchText = $postRequest->get('searchText', null);
        $availableSearchCriteria = array('surname', 'firstname', 'username', 'email');
        $searchCriteria = array();
        $order = $postRequest->get('displaySorting', null);
        if (! in_array($order, array('asc', 'desc'))) {
            $order = 'asc';
        }
        
        if (null != $searchText)
        {
            foreach ($availableSearchCriteria as $criterion)
            {
                if (null != $postRequest->get($criterion.'Criterion', null)) {
                    $searchCriteria[$criterion] = $order;
                }
            }
        }
        // Default criteria for search of users
        if (empty($searchCriteria)) {
            $searchCriteria = array('surname' => $order, 'firstname' => $order);
        }

        // Selection of users by criteria and groups
        $users = $em->getRepository('GessehUserBundle:User')->getCompleteByGroupAndName($groupsIdDisplay, $searchText, $searchCriteria);

        return array(
            'users' => $users,
            'searchText' => $searchText,
            'searchCriteria' => $searchCriteria,
            'groups' => $groups,
            'groupsIdDisplay' => $groupsIdDisplay
        );
    }


    /**
     * Form to add a new user
     *
     * @param Request $request
     *
     * @return array
     *
     * @Route("/new", name="GUser_AdminNewUser")
     * @Template()
     */
    public function newUserAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new UserNewType, $user, array(
                                      'cancel_action' => $this->generateUrl('GUser_AdminListUsers')
                                  ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            // Insertion of the user in database
            $um = $this->get('gesseh_user.user_manager');
            
            $username = $um->generateUsername($user);
            $user->setUsername($username);
            $password = substr($this->get('fos_user.util.token_generator')->generateToken(), 0, self::NB_PASSWORD_CHAR);
            $user->setPlainPassword($password);

            $um->updateUser($user, false);

            // Allocation of default roles
            if ($form->get('defaultUserRoles')->getData()) {
                /*****************************************/
                /* default role for user : to be adapted */
                /*****************************************/
                $um->allocateRolesToUser($user, array('ROLE_USER'), $this->get('gesseh_user.groups_hierarchy')->getFamilies($user->getGroups()));
            }

            $um->flush();

            // Send of an email
            if ($form->get('sendEmail')->getData()) {
                $emailMessage = \Swift_Message::newInstance()
                    ->setSubject("Registration")
                    /*************************************************/
                    /* Email to be managed with a configuration tool */
                    /*************************************************/
                    ->setFrom("associlar@yahoo.fr")
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            'GessehUserBundle:Email:registration.txt.twig',
                            array('user' => $user)
                        ),
                        'text/plain'
                    );

                $this->get('mailer')->send($emailMessage);
            }

                    
            $this->get('session')->getFlashBag()->add('success', 'Registration of "'.$user->__toString().'" completed');

            /*****************/
            /* TEMPORAIRE !! */
            /*****************/
            file_put_contents($this->get('kernel')->getRootDir().'/../src/Gesseh/UserBundle/passwords', $username.' :   '.$password."\n", FILE_APPEND);

            // Next action : go back to users list or add a new user
            $nextAction = $form->get('save')->isClicked()?'GUser_AdminListUsers':'GUser_AdminNewUser';
            return $this->redirect($this->generateUrl($nextAction));
        }

        return array(
            'form' => $form->createView()
        );
    }


    /**
     * Form to import new users from a file
     *
     * Importation from a CSV file with predefined headers for columns. Some are mandatory, others are optional
     *
     * @param Request $request
     *
     * @return array
     *
     * @Route("/users/import", name="GUser_AdminImportUsers")
     * @Template()
     */
    public function importUsersAction(Request $request)
    {
        $um = $this->get('gesseh_user.user_manager');
        $em = $this->getDoctrine()->getManager();

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $form = $this->createForm(new ImportUsersType(), null, array(
                                      'cancel_action' => $this->generateUrl('GUser_AdminListUsers')
                                  ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            // Conversion from CSV file to PHP array
            $CSVArrayConverter = $this->get('gesseh_converter.csv_array');
            $data = $CSVArrayConverter->convert($form->get('file')->getData());
            if (! is_array($data)) {
                $message = "";
                switch ($data) {
                  case CSVArrayConverter::WRONG_FILE_FORMAT:
                      $message = "Bad file format";
                      break;
                  case CSVArrayConverter::FILE_ERROR:
                      $message = "The file could not be opened";
                      break;
                }

                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirect($this->generateUrl('GUser_AdminImportUsers'));
            }
            
            $dataSize = count($data);

            $nbRegisteredUsers = 0;

            // Group to allocate to each user, and it's family group
            $group = $form->get('groupAllocation')->get('group')->getData();
            if (null == $group) {
                $this->get('session')->getFlashBag()->add('error', "No group selected");
                return $this->redirect($this->generateUrl('GUser_AdminImportUsers'));
            }
            
            $familyGroups = $this->get('gesseh_user.groups_hierarchy')->getFamilies( array($group));

            // each row corresponds to one user
            foreach ($data as $i => $row) {
                $user = new User();

                // hydration from the data contained in the row
                $user->hydrate($row);

                if (!isset ($row['username']) or null == $row['username']) {
                    $user->setUsername($um->generateUsername($user));
                }

                if (!isset ($row['password']) or null == $row['password']) {
                    $password = substr($this->get('fos_user.util.token_generator')->generateToken(), 0, self::NB_PASSWORD_CHAR);
                }
                else {
                    $password = $row['password'];
                }
                $user->setPlainPassword($password);


                // Allocation to the choosen group
                $groupAllocation = new GroupAllocation();
                $groupAllocation->setGroup($group);
                $groupAllocation->setStartDate($form->get('groupAllocation')->get('startDate')->getData());
                $groupAllocation->setEndDate($form->get('groupAllocation')->get('endDate')->getData());
                $user->addGroupAllocation($groupAllocation);

                // Application of default role for some group
                if ($form->get('defaultUserRoles')->getData()) {
                    $um->allocateRolesToUser($user, array('ROLE_USER'), $familyGroups);
                }

                /*****************/
                /* TEMPORAIRE !! */
                /*****************/
                file_put_contents($this->get('kernel')->getRootDir().'/../src/Gesseh/UserBundle/passwords', $user->getUsername().' :   '.$password."\n", FILE_APPEND);

                // Management of errors
                $errorList = $this->get('validator')->validate($user, array('admin'));

                if (count ($errorList) > 0) {
                    $errors = array();
                    foreach($errorList as $error) {
                        $errors[] = $error->__toString();
                    }
                    $this->get('session')->getFlashBag()->add('error', 'Registration of user "'.$user->__toString().'" failed because of following error(s) : '.implode(', ', $errors));

                    continue;
                }

                // persistence in doctrine +- flush in database (each NB_USERS_FLUSH_BLOCK users)
                $um->updateUser($user, false);
                $nbRegisteredUsers ++;
                
                if (($i > 0 and $i % self::NB_USERS_FLUSH_BLOCK === 0) or $i == $dataSize - 1) {
                    $em->flush();
                    //$em->clear();
                }
            }

            if ($nbRegisteredUsers > 0) {
                $this->get('session')->getFlashBag()->add('success', 'Registration of '.$nbRegisteredUsers.' users completed');
            }

            // Next action : go back to users list or import further users
            $nextAction = $form->get('save')->isClicked()?'GUser_AdminListUsers':'GUser_AdminImportUsers';
            //return $this->redirect($this->generateUrl($nextAction));
        }

        return array(
            'form' => $form->createView()
        );
    }


    /**
     * Deletion of a user
     *
     * @param int $id the key of user entity in the database
     *
     * @Route("/{id}/delete", name="GUser_AdminDeleteUser", requirements={"id" = "\d+"})
     */
    public function deleteUserAction($id)
    {
        $um = $this->get('gesseh_user.user_manager');
        
        $user = $um->getRepository()->find($id);
        if (null == $user) {
            throw $this->createNotFoundException("Unable to find this user");
        }

        if ($um->deleteUser($user) == $um::USER_DELETED) {
            $this->get('session')->getFlashBag()->add('success', 'User "'.$user->__toString().'" removed from the database');
        }
        else {
            $this->get('session')->getFlashBag()->add('warning', 'User "'.$user->__toString().'" has only been removed from the group(s) you are allowed to manage');
        }
        
        return $this->redirect($this->generateUrl('GUser_AdminListUsers'));
    }

    
    /**
     * Form for edition of a user
     *
     * @param Request $request
     * @param int $id the key of user entity in the database
     *
     * @return array
     *
     * @Route("/{id}/edit", name="GUser_AdminEditUser", requirements={"id" = "\d+"})
     * @Template()
     */
    public function editUserAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $um = $this->get('gesseh_user.user_manager');
        $user = $um->getRepository()->getCompleteUser($id);
        
        if (null == $user) {
            throw $this->createNotFoundException("Unable to find this user");
        }
        elseif (! $this->get('gesseh_user.role_checker')->isUserReachable($user, 'ROLE_ADMIN_USER')) {
            throw $this->createAccessDeniedException("You are not allowed to manage this user");
        }

        /* foreach ($user->getGroupAllocations() as $groupAllocation) { */
        /*     if (! $this->get('gesseh_user.role_checker')->hasRole('ROLE_ADMIN_USER', $groupAllocation->getGroup())) { */
        /*         $this->get('session')->getFlashBag()->add('error', 'You cannot modify the user "'.$user->__toString().'" because he/she belongs to group(s) you are not allowed to manage'); */
        /*         return $this->redirect($this->generateUrl('GUser_AdminShowProfile', array('id' => $id))); */
        /*     } */
        /* } */
        
        $form = $this->createForm(new UserEditType(), $user, array(
                                      'cancel_action' => $this->generateUrl('GUser_AdminListUsers')
                                  ));

        $originalGroupAllocations = $user->getGroupAllocations()->toArray();
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            foreach ($originalGroupAllocations as $originalGroupAllocation) {
                if (! $user->getGroupAllocations()->contains($originalGroupAllocation)) {
                    $em->remove($originalGroupAllocation);
                }
            }

            $em->flush();
                
            $um->updateUser($user);
            $this->get('session')->getFlashBag()->add('success', 'User ' . $user->__toString().' updated');
            return $this->redirect($this->generateUrl('GUser_AdminListUsers'));
        }

        return array(
            'form' => $form->createView(),
            'user' => $user
        );
    }


    /**
     * Display of a user's profile
     *
     * @param int $id the key of user entity in the database
     *
     * @return array
     *
     * @Route("/{id}/show", name="GUser_AdminShowProfile", requirements={"id" = "\d+"})
     * @Template()
     */
    public function showProfileAction($id)
    {
        $um = $this->get('gesseh_user.user_manager');
        $user = $um->getRepository()->getCompleteUser($id);

        if (null == $user) {
            throw $this->createNotFoundException("Unable to find this user");
        }

        foreach ($user->getGroupAllocations() as $groupAllocation) {
            if ($this->get('gesseh_user.role_checker')->hasRole('ROLE_ADMIN_DISPLAY_USER', $groupAllocation->getGroup())) {
                return array(
                    'user' => $user
                ); 
            }
        }
        
        $this->get('session')->getFlashBag()->add('error', 'You are not allowed to display the user "'.$user->__toString().'"');
        return $this->redirect($this->generateUrl('GUser_AdminListUsers'));
    }


    /**
     * Edition of many users
     *
     * Available actions are:
     * - deletion of these users
     * - manual modification of some of their group: ending/deleting an allocation to one group, adding another group
     * - upgrade of their group allocation, based on group's ranking configuration
     *
     * @param Request $request
     * 
     * @return array
     *
     * @Route("/users/edit", name="GUser_AdminEditUsers")
     * @Template()
     */
    public function editUsersAction(Request $request)
    {
        $um = $this->get('gesseh_user.user_manager');
        $groupsHierarchy = $this->get('gesseh_user.groups_hierarchy');

        if ($request->getMethod() != 'POST') {
            return $this->redirect($this->generateUrl('GUser_AdminListUsers'));
        }

        $postRequest = $request->request;

        $usersId = array();
        foreach ($postRequest as $parameter => $value) {
            if (preg_match('#user_#', $parameter)) {
                $usersId[] = substr($parameter, 5);
            }
        }

        $users = $um->getRepository()->getCompleteByIds($usersId);

        if (! $users) {
            $this->get('session')->getFlashBag()->add('error', 'No user selected');
            return $this->redirect($this->generateUrl('GUser_AdminListUsers'));
        }

        $changeGroupAction = null;
        if (null != $postRequest->get('changeGroupAllocations', null)) {
            $changeGroupAction = 'change';
        }
        elseif (null != $postRequest->get('upgradeGroupAllocations', null)) {
            $changeGroupAction = 'upgrade';
        }
        
        if (null != $postRequest->get('deleteUsers', null)) {
            $deletionResults = $um->deleteUsers($users);
            if ($deletionResults['deletedUsersName']) {
                $this->get('session')->getFlashBag()->add('success', 'Deletion of "'.implode(', ', $deletionResults['deletedUsersName']).'" completed');
            }
            if ($deletionResults['notDeletedUsersName']) {
                $this->get('session')->getFlashBag()->add('warning', 'User(s) "'.implode(', ', $deletionResults['notDeletedUsersName']).'" has(have) only been removed from the group(s) you are allowed to manage');
            }

            return $this->redirect($this->generateUrl('GUser_AdminListUsers'));
        }

        elseif (null != $changeGroupAction) {
            if ($changeGroupAction == 'upgrade') {
                $form = $this->createForm(new UpgradeGroupAllocationType($this->get('gesseh_user.role_checker'), $groupsHierarchy, $users), null, array(
                                              'cancel_action' => $this->generateUrl('GUser_AdminListUsers')
                                          ));
            }
            else {
                $form = $this->createForm(new ChangeUsersGroupAllocationType($this->get('gesseh_user.role_checker'), $users), null, array(
                                              'cancel_action' => $this->generateUrl('GUser_AdminListUsers')
                                          ));                
            }
            
            $form->handleRequest($request);


            if ($form->isValid()) {

                $updatedUsers = $nonRemovedGroups = $nonAddedGroups = array();
                $dateChange = $form->get('dateChange')->getData();

                if ($changeGroupAction == 'upgrade') {
                    $availableGroups = $groupsHierarchy->getReachableStagedGroups(array($form->get('familyGroup')->getData()));
                    $newGroup = null;
                }
                else {
                    $newGroup = $form->get('newGroup')->getData();
                    $oldGroup = $form->get('oldGroup')->getData();
                }


                foreach ($users as $user){
                    if ($changeGroupAction == 'upgrade') {
                        $oldGroupAllocations = array();
                        foreach ($user->getGroupAllocations() as $groupAllocation) {
                            if (in_array ($groupAllocation->getGroup(), $availableGroups)) {
                                $oldGroupAllocations[] = $groupAllocation;
                            }
                        }
                        
                        $oldGroupAllocations = $this->get('gesseh_core.period_handler')->getLastPeriods($oldGroupAllocations, $dateChange);
                        if (count ($oldGroupAllocations) == 1) {
                            $oldGroupAllocation = $oldGroupAllocations[0];
                        }
                        else {
                            $oldGroupAllocation = null;
                        }

                        if (null != $oldGroupAllocation) {
                            $newGroup = $groupsHierarchy->getNextStagedGroup($oldGroupAllocation->getGroup());
                        }

                        $oldGroup = $oldGroupAllocation;
                    }

                    
                    if (null != $newGroup) {
                        $addGroupAllocation = $um->addGroupAllocationToUser($newGroup, $user, $dateChange, null, false);
                        if ($addGroupAllocation == UserManager::NO_ERROR) {
                            $updatedUsers[] = $user->__toString();
                        }
                        else {
                            $nonAddedGroups[$user->__toString()] = $addGroupAllocation;
                        }
                    }

                    if (null != $oldGroup) {

                        if ($changeGroupAction == 'change' and $form->get('deleteGroup')->getData()) {
                            $removeGroupAllocation = $um->removeGroupAllocationToUser($oldGroup, $user, null);
                        }
                        else {
                            $removeGroupAllocation = $um->removeGroupAllocationToUser($oldGroup, $user, $dateChange);
                        }

                        if ($removeGroupAllocation === UserManager::NO_ERROR) {
                            $updatedUsers[] = $user->__toString();
                        }
                        else {
                            $nonRemovedGroups[$user->__toString()] = $removeGroupAllocation === UserManager::NO_ACTION ? "No group to end/remove" : $removeGroupAllocation;
                        }
                    }
                }
                    
                $um->flush();

                if ($updatedUsers) {
                    $updatedUsers = array_unique($updatedUsers);
                    $usersFlashBag = count($updatedUsers)<=self::MAX_NB_USERS_FLASHBAG?implode(", ", $updatedUsers):"the selected users";
                    $this->get('session')->getFlashBag()->add('success', 'Groups updated for '.$usersFlashBag);
                }
                if ($nonAddedGroups) {
                    $usersFlashBag = array();
                    foreach ($nonAddedGroups as $u => $e) {
                        $usersFlashBag[] = '"'.$u.'" : '.$e;
                    }
                    $usersFlashBag = implode('<br/>', $usersFlashBag);
                    $this->get('session')->getFlashBag()->add('warning', 'The group "'.$newGroup->__toString().'" has not been added for this(these) user(s) for the following reasons :<br/>'.$usersFlashBag);
                }
                if ($nonRemovedGroups) {
                    $usersFlashBag = array();
                    foreach ($nonRemovedGroups as $u => $e) {
                        $usersFlashBag[] = '"'.$u.'" : '.$e;
                    }
                    $usersFlashBag = implode('. ', $usersFlashBag);
                    $this->get('session')->getFlashBag()->add('warning', 'The group "'.$oldGroup->__toString().'" has not been removed for this(these) user(s) for the following reasons : '.$usersFlashBag);
                }

                return $this->redirect($this->generateUrl('GUser_AdminListUsers'));
            }
        }

        else {
            $this->get('session')->getFlashBag()->add('error', "Missing action to perform");
            return $this->redirect($this->generateUrl('GUser_AdminListUsers'));
        }

        return array(
            'users' => $users,
            'form' => $form->createView(),
            'changeGroupAction' => $changeGroupAction
        );
    }


    /**
     * Edition of the roles of a user
     *
     * Form for allocating a role to a user, and display of the roles already allocated with proposed deletion action
     *
     * @param Request $request
     * @param int $id the key of user entity in database
     *
     * @return array
     *
     * @Route("/{id}/roles/edit", name="GUser_AdminEditRoleApplications", requirements={"id" = "\d+"})
     * @Template()
     */
    public function editRoleApplicationsAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $um = $this->get('gesseh_user.user_manager');
        $roleChecker = $this->get('gesseh_user.role_checker');

        $user = $um->getRepository()->getWithRoles($id);

        $roleApplication = new RoleApplication();
        
        $form = $this->createForm('role_application', $roleApplication);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $object = $form->get('group')->getData();
            /* echo $object->__toString(); */
            $roleApplication->setObjectType(implode('', array_slice(explode('\\', get_class($object)), -1)));
            $roleApplication->setObjectId($object->getId());
            
            $user->addRoleApplication($roleApplication);
            
            $em->persist($roleApplication);
            
            $em->flush();
            $um->updateUser($user);

            $this->get('session')->getFlashBag()->add('success', 'Role "'.$roleApplication->getRole()->__toString().'" on "'.$object->__toString().'" added');

            return $this->redirect($this->generateUrl('GUser_AdminEditRoleApplications', array('id' => $id)));
        }

        return array(
            'user' => $user,
            'roleApplications' => $this->get('gesseh_user.role_application_manager')->getRolesAndObjects($user->getRoleApplications()->toArray(), true),
            'form' => $form->createView()
        );
    }


    /**
     * Deletion of a user's role
     *
     * @param int $userId the key of user entity in database
     * @param int $roleAppId the key of RoleAllocation entity in the database
     *
     * @Route("/{userId}/role/{roleAppId}/delete", name="GUser_AdminDeleteRoleApplication", requirements={"userId" = "\d+", "roleAppId" = "\d+"})
     */
    public function deleteRoleApplicationAction($userId, $roleAppId)
    {
        $em = $this->getDoctrine()->getManager();
        $um = $this->get('gesseh_user.user_manager');

        $user = $um->getRepository()->getWithRoles($userId);

        foreach($user->getRoleApplications() as $roleApplication) {
            if ($roleAppId == $roleApplication->getId()) {
                $user->removeRoleApplication($roleApplication);
                $em->remove($roleApplication);
            }
        }

        $em->flush();
        $um->updateUser($user);

        $this->get('session')->getFlashBag()->add('success', 'Role deleted');
        
        return $this->redirect($this->generateUrl('GUser_AdminEditRoleApplications', array('id' => $userId)));
    }
}
