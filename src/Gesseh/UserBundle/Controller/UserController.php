<?php

namespace Gesseh\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Gesseh\UserBundle\Form\UserProfileType;

/**
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="GUser_Index")
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('GUser_ListUsers'));
    }

    /**
     * @Route("/users", name="GUser_ListUsers")
     * @Template()
     * @Security("has_role('ROLE_USER_DISPLAY_USER')")
     */
    public function listUsersAction(Request $request)
    {
        $DEFAULT_SEARCH_CRITERIA = array('surname' => 'asc', 'firstname' => 'asc');

        $em = $this->getDoctrine()->getManager();
        
        $groups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_USER_DISPLAY_USER', 'Group');
        $groups = $this->get('gesseh_user.groups_hierarchy')->sortByStageByFamily($groups, 'asc');
        
        $displayedGroups = array();
        foreach ($groups as $group) {
            if (null != $request->get('group'.$group->getId(), null)) {
                $displayedGroups[] = $group;
            }
        }
        if (empty ($displayedGroups)) {
            $displayedGroups = $groups;
        }

        $search = $request->get('search', null);
        $possibleSearchCriteria = array('surname', 'firstname', 'username');
        $searchCriteria = array();
        if (null != $search)
        {
            foreach ($possibleSearchCriteria as $criterion)
            {
                if (null != $request->get($criterion.'Criterion', null)) {
                    $searchCriteria[$criterion] = 'asc';
                }
            }
        }
        if (empty($searchCriteria)) {
            $searchCriteria = $DEFAULT_SEARCH_CRITERIA;
        }

        $users = $em->getRepository('GessehUserBundle:User')->getCompleteByGroupAndName($displayedGroups, $search, $searchCriteria);

        return array(
            'users' => $users,
            'search' => $search,
            'searchCriteria' => $searchCriteria,
            'groups' => $groups,
            'displayedGroups' => $displayedGroups
        );       
    }

    /**
     * @Route("/profile/{id}", name="GUser_ShowProfile", requirements={"id" = "\d+"}, defaults={"id" = -1})
     * @Template()
     * @Security("has_role('ROLE_USER_SHOW_PROFILE')")
     */
    public function showProfileAction($id)
    {
        if ($id > 0) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('GessehUserBundle:User')->find($id);
        }
        else {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        }

        if (null == $user) {
            throw $this->createNotFoundException("Unable to find this user");
        }

        return array(
            'user' => $user
        );
    }

    /**
     * @Route("/profile/edit", name="GUser_EditProfile")
     * @Template()
     * @Security("has_role('ROLE_USER_UPDATE_PROFILE')")
     */
    public function editProfileAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $form = $this->createForm(new UserProfileType(), $user, array(
                                      'cancel_action' => $this->generateUrl('GUser_ShowProfile')
                                  ));
        
        $um = $this->get('gesseh_user.user_manager');

        $form->handleRequest($request);
        if ($form->isValid()) {
            $um->updateUser($user);
            $this->get('session')->getFlashBag()->add('success', 'Profile updated');
            return $this->redirect($this->generateUrl('GUser_ShowProfile'));
        }

        return array (
            'form' => $form->createView(),
            'user' => $user
        );
    }

    /**
     * Display email of selected users
     *
     * @param Request $request
     *
     * @Route("/emails", name="GUser_ListEmails")
     * @Template()
     * @Security("has_role('ROLE_USER_DISPLAY_USER')")
     */
    public function listEmailsAction(Request $request)
    {
        if ($request->getMethod() != 'POST') {
            return $this->redirect($this->generateUrl('GUser_ListUsers'));
        }

        $postRequest = $request->request;
        $em = $this->getDoctrine()->getManager();

        $availableGroups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_USER_DISPLAY_USER', 'Group');

        /* $concernedGroups = array(); */
        /* foreach ($availableGroups as $availableGroup) { */
        /*     if (null != $postRequest->get('group'.$availableGroup->getId(), null)) { */
        /*         $concernedGroups[] = $availableGroup; */
        /*     } */
        /* } */

        $availableUsers = $em->getRepository('GessehUserBundle:User')->getCompleteByGroup($availableGroups, array(new \DateTime('now')));

        $usersDisplayedEmail = array();
        $usersNonDisplayedEmail = array();
        foreach ($availableUsers as $availableUser) {
            if (null != $postRequest->get('user_'.$availableUser->getId(), null)) {
                if ($availableUser->getDisplayEmail()) {
                    $usersDisplayedEmail[] = $availableUser;
                }
                else {
                    $usersNonDisplayedEmail[] = $availableUser->getName();
                }
            }
        }

        if (! empty($usersNonDisplayedEmail)) {
            $this->get('session')->getFlashBag()->add('warning', "The email address is not available for theses users: ".implode(', ', $usersNonDisplayedEmail));
        }
                
        if (empty ($usersDisplayedEmail)) {
            $this->get('session')->getFlashBag()->add('warning', "No email address available");

            return $this->redirect($this->generateUrl('GUser_ListUsers'));
        }

        return array(
            'usersDisplayedEmail' => $usersDisplayedEmail,
            /* 'usersNonDisplayedEmail' => $usersNonDisplayedEmail, */
        );
    }
}