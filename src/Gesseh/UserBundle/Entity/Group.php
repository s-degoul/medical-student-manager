<?php

namespace Gesseh\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Group
 *
 * @ORM\Table(name="gesseh_group")
 * @ORM\Entity(repositoryClass="Gesseh\UserBundle\Entity\GroupRepository")
 */
class Group extends BaseGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, unique=false)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="family", type="boolean")
     */
    private $family;

    /**
     * @var integer
     *
     * @ORM\Column(name="stage", type="integer", length=2, unique=false, nullable=true)
     */
    private $stage;

    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\UserBundle\Entity\Group")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Gesseh\UserBundle\Entity\GroupAllocation", mappedBy="group", cascade={"persist","remove"})
     */
    private $groupAllocations;

    /* /\** */
    /*  * @ORM\OneToMany(targetEntity="Gesseh\UserBundle\Entity\UserAdminGroup", mappedBy="group", cascade={"persist","remove"}) */
    /*  *\/ */
    /* private $adminUsers; */

    public function __construct($name)
    {
        parent::__construct($name);
        $this->family = false;
        $this->groupAllocations = new ArrayCollection();
        /* $this->adminUsers = new ArrayCollection(); */
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getCompleteTitle()
    {
        if (null != $this->getParent() and ! $this->isFamily()) {
            return $this->getParent()->getCompleteTitle().' - '.$this->getTitle();
        }

        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parent
     *
     * @param \Gesseh\UserBundle\Entity\Group $parent
     * @return Group
     */
    public function setParent(Group $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Gesseh\UserBundle\Entity\Group 
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return Group
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
  
    /**
     * Set family
     *
     * @param boolean $family
     * @return Group
     */
    public function setFamily($family)
    {
        $this->family = $family;

        return $this;
    }
    
    /**
     * Is family ?
     *
     * @return boolean
     */
    public function isFamily()
    {
        return $this->family;
    }

    /**
     * Set stage
     *
     * @param integer $stage
     * @return Group
     */
    public function setStage($stage)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return integer
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * Get group allocations
     *
     * @return ArrayCollection 
     */
    public function getGroupAllocations()
    {
        return $this->groupAllocations;
    }
    
    /**
     * Add a group allocation
     *
     * @return self
     */
    public function addGroupAllocation(GroupAllocation $groupAllocation)
    {
        if (! $this->groupAllocations->contains($groupAllocation)) {
            $this->groupAllocations->add($groupAllocation);
            $groupAllocation->setGroup($this);
        }

        return $this;
    }

    /** Remove a group allocation
     *
     * @return self
     */
    public function removeGroupAllocation(GroupAllocation $groupAllocation)
    {
        $this->groupAllocations->removeElement($groupAllocation);

        return $this;
    }

     
    /* public static function loadValidatorMetadata(ClassMetadata $metadata) */
    /* { */
    /*     $metadata->addConstraint(new GroupRecursiveHierarchy()); */
    /* } */
}
