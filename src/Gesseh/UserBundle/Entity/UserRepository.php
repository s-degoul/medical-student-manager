<?php

namespace Gesseh\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Gesseh\UserBundle\Entity\Group;
use Gesseh\CoreBundle\Service\PeriodHandler\PeriodInterface;


/**
 * UserRepository
 */
class UserRepository extends EntityRepository
{
    private function getUserQuery()
    {
        return $this->createQueryBuilder('u');
    }
    
    private function getCompleteUserQuery()
    {
        $qb = $this->getUserQuery();
        $qb->leftJoin('u.groupAllocations', 'ag')
            ->addSelect('ag')
            ->leftJoin('ag.group', 'agg')
            ->addSelect('agg');
            /* ->leftJoin('u.adminGroups', 'ag') */
            /* ->addSelect('ag'); */
        return $qb;
    }
    
    private function getByName($qb, $search = null, $searchCriteria = array('surname' => 'asc', 'firstname' => 'asc'))
    {
        if (null != $search)
        {
            $separator = '';
            $criteriaQuery = '';
            foreach ($searchCriteria as $searchCriterion => $order)
            {
                // TO BE IMPROVED BY USING FUNCTIONS OF QUERY BUILDER
                
                $criteriaQuery .= $separator.'u.'.$searchCriterion.' LIKE :search';
                $separator = ' OR ';
            }

            $qb->andWhere($criteriaQuery);

            $qb->setParameter('search', '%'.$search.'%');
        }

        foreach ($searchCriteria as $searchCriterion => $order) {
            $qb->addOrderBy('u.'.$searchCriterion, $order);
        }

        return $qb;
    }

    public function getCompleteUser($id)
    {
        $qb = $this->getCompleteUserQuery();
        $qb->where($qb->expr()->eq('u.id', ':id'))
            ->setParameter('id', $id);

        return $qb->getQuery()
            ->getSingleResult();
    }

    private function getByGroupAndPeriod($qb, array $groups, $period = null)
    {
        $groupsId = array();

        foreach ($groups as $group) {
            if ($group instanceof Group) {
                $groupsId[] = $group->getId();
            }
            elseif (is_numeric ($group)) {
                $groupsId[] = $group;
            }
            else {
                throw new \InvalidArgumentException("groups must be an array of groups or groups' ids");
            }
        }

        $qb->andWhere($qb->expr()->in('agg.id', $groupsId));

        
        $startDate = $endDate = null;
        if ($period instanceof PeriodInterface) {
            $startDate = $period->getStartDate();
            $endDate = $period->getEndDate();
        }
        elseif (is_array($period)) {
            $startDate = $period[0] instanceof \DateTime ? $period[0] : null;
            $endDate = isset ($period[1]) and $period[1] instanceof \DateTime ? $period[1] : null;
        }
        elseif (null != $period) {
            throw new \InvalidArgumentException("Argument period must be an instance of PeriodInterface or an array");
        }

        if (null != $startDate) {
            $qb->andWhere('ag.endDate > :startDate OR ag.endDate IS NULL')
                ->setParameter('startDate', $startDate);
        }
        if (null != $endDate) {
            $qb->andWhere('ag.startDate < :endDate OR ag.startDate IS NULL')
                ->setParameter('endDate', $endDate);
        }

        return $qb;
    }

    public function getCompleteByName($search = null, $searchCriteria = array('surname' => 'asc', 'firstname' => 'asc'))
    {
        $qb = $this->getCompleteUserQuery();
        $this->getByName($qb, $search, $searchCriteria);

        return $qb->getQuery()
            ->getResult();
    }


    public function getCompleteByGroup(array $groups = array(null), $period = null)
    {
        $qb = $this->getCompleteUserQuery();
        $this->getByGroupAndPeriod($qb, $groups, $period);

        return $qb->getQuery()
            ->getResult();
    }

    
    public function getCompleteByGroupAndName(array $groups = array(null), $search = null, $searchCriteria = array('surname' => 'asc', 'firstname' => 'asc'), $period = null)
    {
        $qb = $this->getCompleteUserQuery();
        $this->getByGroupAndPeriod($qb, $groups, $period);
        $this->getByName($qb, $search, $searchCriteria);

        return $qb->getQuery()
            ->getResult();
    }


    public function getCountByUsername($username)
    {
        $qb = $this->getUserQuery();
        $qb->select('COUNT(u)')
            ->where('u.username = :username')
            ->setParameter('username', $username);

        return $qb->getQuery()
                  ->getSingleScalarResult();
    }

    public function getByIds(array $ids)
    {
        $qb = $this->getUserQuery();
        $qb->andWhere($qb->expr()->in('u.id', ':ids'))
            ->setParameter('ids', $ids);

        return $qb->getQuery()
            ->getResult();
    }

    public function getCompleteByIds(array $ids)
    {
        $qb = $this->getCompleteUserQuery();
        $qb->andWhere($qb->expr()->in('u.id', ':ids'))
            ->setParameter('ids', $ids);

        return $qb->getQuery()
            ->getResult();
    }

    public function getWithRoles($id)
    {
        $qb = $this->getUserQuery();
        $qb->leftJoin('u.roleApplications', 'ra')
            ->addSelect('ra');

        $qb->where($qb->expr()->eq('u.id', ':id'))
            ->setParameter('id', $id);

        return $qb->getQuery()
            ->getSingleResult();
    }
}
