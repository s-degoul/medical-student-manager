<?php

namespace Gesseh\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\GroupInterface;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * User
 *
 * @ORM\Table(name="gesseh_user")
 * @ORM\Entity(repositoryClass="Gesseh\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Firstname
     *
     * @var string
     * 
     * @ORM\Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * Surname
     * 
     * @var string
     * 
     * @ORM\Column(name="surname", type="string", length=100, nullable=false)
     */
    private $surname;

    /**
     * Title (Mr., Mrs., Pr.,...)
     *
     * @var string
     * 
     * @ORM\Column(name="title", type="string", length=20, nullable=true)
     */
    private $title;

    /**
     * Phone number
     *
     * @var string
     * 
     * @ORM\Column(name="phone", type="string", length=25, nullable=true)
     */
    private $phone;

    /**
     * Address: street name & street number
     *
     * @var string
     * 
     * @ORM\Column(name="street", type="string", length=100, nullable=true)
     */
    private $street;

    /**
     * Address: postcode
     *
     * @var string
     * 
     * @ORM\Column(name="postcode", type="string", length=10, nullable=true)
     */
    private $postcode;
    
    /**
     * Address: city
     *
     * @var string
     * 
     * @ORM\Column(name="city", type="string", length=100, nullable=true)
     */
    private $city;

    /**
     * Address: country
     *
     * @var string
     * 
     * @ORM\Column(name="country", type="string", length=100, nullable=true)
     */
    private $country;

    /**
     * Should the email be displayed to other users?
     *
     * @var bool
     *
     * @ORM\Column(name="displayEmail", type="boolean", nullable=false)
     */
    private $displayEmail;

    /**
     * Should the phone number be displayed to other users?
     *
     * @var bool
     *
     * @ORM\Column(name="displayPhone", type="boolean", nullable=false)
     */
    private $displayPhone;
    
    /**
     * Groups allocations for the user
     *
     * @ORM\OneToMany(targetEntity="Gesseh\UserBundle\Entity\GroupAllocation", mappedBy="user", cascade={"persist","remove"})
     */
    protected $groupAllocations;

    /**
     * Roles of the user
     *
     * @ORM\OneToMany(targetEntity="Gesseh\UserBundle\Entity\RoleApplication", mappedBy="user", cascade={"persist","remove"})
     */
    protected $roleApplications;


    /**
     * Constructor function
     *
     * Set default values for some properties: enabled, displayEmail, displayPhone, groupAllocations, roleApplications
     */
    public function __construct()
    {
        parent::__construct();
        $this->setEnabled(true);
        $this->setDisplayEmail(true);
        $this->setDisplayPhone(false);
        $this->groupAllocations = new ArrayCollection();
        $this->roleApplications = new ArrayCollection();
    }


    /**
     * Hydration function
     *
     * @param array $data
     */
    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {
            $method = 'set'.ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    
    /**
     * Get id
     *
     * @return int 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return User
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set phone number
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone number
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set street name and street number
     *
     * @param string $street
     *
     * @return User
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return User
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set displayEmail status
     *
     * @param bool $displayEmail
     *
     * @return User
     */
    public function setDisplayEmail($displayEmail)
    {
        $this->displayEmail = $displayEmail;

        return $this;
    }

    /**
     * Get displayEmail status
     *
     * @return bool
     */
    public function getDisplayEmail()
    {
        return $this->displayEmail;
    }
    
    /**
     * Set displayPhone status
     *
     * @param bool $displayPhone
     *
     * @return User
     */
    public function setDisplayPhone($displayPhone)
    {
        $this->displayPhone = $displayPhone;

        return $this;
    }

    /**
     * Get displayPhone
     *
     * @return boolean
     */
    public function getDisplayPhone()
    {
        return $this->displayPhone;
    }

            
    /**
     * Get groupAllocations
     *
     * @return ArrayCollection
     */
    public function getGroupAllocations()
    {
        return $this->groupAllocations;
    }

    /**
     * Get group allocations for a date
     *
     * @param DateTime $date
     *
     * @return ArrayCollection
     */
    public function getGroupAllocationsForDate(\DateTime $date = null)
    {
        if (null === $date) {
            return $this->getGroupAllocations();
        }
        
        $groupAllocationsForDate = new ArrayCollection();
        foreach ($this->getGroupAllocations() as $groupAllocation) {
            if (($groupAllocation->getStartDate() <= $date or null == $groupAllocation->getStartDate()) and ($groupAllocation->getEndDate() >= $date or null == $groupAllocation->getEndDate())) {
                $groupAllocationsForDate->add($groupAllocation);
            }
        }

        return $groupAllocationsForDate;
    }

    /**
     * Get groups, corresponding of each group allocation
     *
     * @return array
     */
    public function getGroups()
    {
        $groups = array();
        foreach ($this->getGroupAllocations() as $groupAllocation) {
            $groups[] = $groupAllocation->getGroup();
        }

        return $groups;
    }
    

    /**
     * Get current group allocations
     *
     * @return ArrayCollection
     */
    public function getCurrentGroupAllocations()
    {
        return $this->getGroupAllocationsForDate(new \DateTime('now'));
    }
    
    
    /**
     * Add a groupAllocation
     *
     * @param GroupAllocation $groupAllocation
     *
     * @return User
     */
    public function addGroupAllocation(GroupAllocation $groupAllocation)
    {
        if (! $this->groupAllocations->contains($groupAllocation)){
            $this->groupAllocations->add($groupAllocation);
            $groupAllocation->setUser($this);
        }

        return $this;
    }

    /**
     * Delete a groupAllocation
     *
     * @param GroupAllocation $groupAllocation
     *
     * @return User
     */
    public function removeGroupAllocation(GroupAllocation $groupAllocation)
    {
        $this->groupAllocations->removeElement($groupAllocation);

        return $this;
    }


    /**
     * Get roleApplications
     *
     * @return ArrayCollection
     */
    public function getRoleApplications()
    {
        return $this->roleApplications;
    }

    
    /**
     * Add a roleApplication
     *
     * @param RoleApplication $roleApplication
     *
     * @return User
     */
    public function addRoleApplication(RoleApplication $roleApplication)
    {
        if (! $this->roleApplications->contains($roleApplication)) {
            $this->roleApplications->add($roleApplication);
            $roleApplication->setUser($this);
        }

        return $this;
    }


    /**
     * Delete a roleApplication
     *
     * @param RoleApplication $roleApplication
     *
     * @return User
     */
    public function removeRoleApplication(RoleApplication $roleApplication)
    {
        $this->roleApplications->removeElement($roleApplication);

        return $this;
    }

    
    /**
     * Get roles
     *
     * Even if it's defined in BaseUser, this function is re-defined here, to prevent roles inheritance from groups
     *
     * @return array
     */
    public function getRoles()
    {
        $roles = array();

        foreach ($this->roleApplications as $roleApplication) {
            $roles[] = $roleApplication->getRole()->getRole();
        }
        // we need to make sure to have at least one role
        //$roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }


    /**
     * Get Firstname and Surname
     *
     * 'firstname surname' if order = 0 (default), 'surname firstname' otherwise
     *
     * @param int $order determine in which order firstname and surname are displayed
     *
     * @return string
     */
    public function getName($order = 0)
    {
        if ($order != 0)
            return $this->getFirstName().' '.mb_strtoupper($this->getSurname());

        return mb_strtoupper($this->getSurname()).' '.$this->getFirstName();
    }

    /**
     * Get text representation of the user entity
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get formated adress
     *
     * @return string
     */
    public function getAdress()
    {
        $adress = '';
        $separator = '';
        
        if (null != $this->getStreet()) {
            $adress .= $separator.$this->getStreet();
            $separator = ', ';
        }
        if (null != $this->getPostcode()) {
            $adress .= $separator.$this->getPostcode();
            $separator = ' ';
        }
        if (null != $this->getCity()) {
            $adress .= $separator.$this->getCity();
            $separator = ', ';
        }
        if (null != $this->getCountry()) {
            $adress .= $separator.$this->getCountry();
        }

        return $adress;
    }
}
