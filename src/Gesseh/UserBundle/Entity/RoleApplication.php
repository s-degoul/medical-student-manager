<?php

namespace Gesseh\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoleApplication
 *
 * @ORM\Table(name="role_application")
 * @ORM\Entity(repositoryClass="Gesseh\UserBundle\Entity\RoleApplicationRepository")
 */
class RoleApplication
{
    /* const ALL_ID = -1; */
    /* const FAMILY_ID = -2; */
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Gesseh\UserBundle\Entity\User", inversedBy="roleApplications")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\UserBundle\Entity\Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id", nullable=false)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="objectType", type="string", length=30, nullable=true)
     */
    private $objectType;

    /**
     * @var integer
     *
     * @ORM\Column(name="objectId", type="integer", nullable=true)
     */
    private $objectId;

    /* /\** */
    /*  * @var boolean */
    /*  * */
    /*  * @ORM\Column(name="application", type="boolean", nullable=false) */
    /*  *\/ */
    /* private $application; */

    /* /\** */
    /*  * @var integer */
    /*  * */
    /*  * @ORM\Column(name="priority", type="integer", nullable=true) */
    /*  *\/ */
    /* private $priority; */


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return RoleApplication
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /* /\** */
    /*  * Set actorId */
    /*  * */
    /*  * @param integer $actorId */
    /*  * @return RoleApplication */
    /*  *\/ */
    /* public function setActorId($actorId) */
    /* { */
    /*     $this->actorId = $actorId; */

    /*     return $this; */
    /* } */

    /* /\** */
    /*  * Get actorId */
    /*  * */
    /*  * @return integer  */
    /*  *\/ */
    /* public function getActorId() */
    /* { */
    /*     return $this->actorId; */
    /* } */

    /**
     * Set role
     *
     * @param Role $role
     * @return RoleApplication
     */
    public function setRole(Role $role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return Role
     */
    public function getRole()
    {
        return $this->role;
    }
    
    /**
     * Set objectType
     *
     * @param string $objectType
     * @return RoleApplication
     */
    public function setObjectType($objectType)
    {
        $this->objectType = $objectType;

        return $this;
    }

    /**
     * Get objectType
     *
     * @return string 
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     * Set objectId
     *
     * @param integer $objectId
     * @return RoleApplication
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId
     *
     * @return integer 
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /* /\** */
    /*  * Set application */
    /*  * */
    /*  * @param boolean $application */
    /*  * @return RoleApplication */
    /*  *\/ */
    /* public function setApplication($application) */
    /* { */
    /*     $this->application = $application; */

    /*     return $this; */
    /* } */

    /* /\** */
    /*  * Get application */
    /*  * */
    /*  * @return boolean  */
    /*  *\/ */
    /* public function getApplication() */
    /* { */
    /*     return $this->application; */
    /* } */

    /* /\** */
    /*  * Set priority */
    /*  * */
    /*  * @param integer $priority */
    /*  * @return RoleApplication */
    /*  *\/ */
    /* public function setPriority($priority) */
    /* { */
    /*     $this->priority = $priority; */

    /*     return $this; */
    /* } */

    /* /\** */
    /*  * Get priority */
    /*  * */
    /*  * @return integer  */
    /*  *\/ */
    /* public function getPriority() */
    /* { */
    /*     return $this->priority; */
    /* } */


    public function __toString()
    {
        return $this->role->__toString().' '.$this->objectType.'('.$this->objectId.')';
    }
}
