<?php

namespace Gesseh\CourseBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Gesseh\UserBundle\Entity\User;

/**
 * AuthorRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AuthorRepository extends EntityRepository
{
    private function getAuthorQuery()
    {
        return $this->createQueryBuilder('a');
    }

    private function getAuthorWithUserQuery()
    {
        $qb = $this->getAuthorQuery();

        $qb->leftJoin('a.user', 'u')
            ->addSelect('u');

        return $qb;
    }

    private function getCompleteAuthorQuery()
    {
        $qb = $this->getAuthorWithUserQuery();

        $qb->leftJoin('a.courses', 'c')
            ->addSelect('c');

        return $qb;
    }

    public function getByUser(User $user)
    {
        $qb = $this->getCompleteAuthorQuery();

        $qb->where($qb->expr()->eq('u.id', ':id'))
            ->setParameter('id', $user->getId());

        return $qb->getQuery()
            ->getOneOrNullResult();
    }

    public function getAll()
    {
        $qb = $this->getCompleteAuthorQuery();

        return $qb->getQuery()
            ->getResult();
    }

    /* public function getCompleteById($id) */
    /* { */
    /*     $qb = $this->getCompleteAuthorQuery(); */

    /*     $qb->andWhere($qb->expr()->eq('a.id', $id)); */

    /*     return $qb->getQuery() */
    /*         ->getSingleResult(); */
    /* } */

    public function getCompleteWithCoursesAndGroupsById($id)
    {
        $qb = $this->getCompleteAuthorQuery();

        $qb->leftJoin('c.groups', 'g')
            ->addSelect('g')
            ->andWhere($qb->expr()->eq('a.id', $id));

        return $qb->getQuery()
            ->getSingleResult();
    }
}
