<?php

namespace Gesseh\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gesseh\UserBundle\Entity\User;

/**
 * Author
 *
 * @ORM\Table(name="author")
 * @ORM\Entity(repositoryClass="Gesseh\CourseBundle\Entity\AuthorRepository")
 */
class Author
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Gesseh\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;
    
    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=100, nullable=true)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=20, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var discipline
     *
     *@ORM\Column(name="discipline", type="string", length=255, nullable=true)
     */
    private $discipline;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\CourseBundle\Entity\Course", mappedBy="authors")
     */
    private $courses;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Author
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Author
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Author
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Author
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Author
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set discipline
     *
     * @param string $discipline
     * @return Author
     */
    public function setDiscipline($discipline)
    {
        $this->discipline = $discipline;

        return $this;
    }

    /**
     * Get discipline
     *
     * @return string 
     */
    public function getDiscipline()
    {
        return $this->discipline;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Author
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get courses
     *
     * @return ArrayCollection
     */
    public function getCourses()
    {
        return $this->courses;
    }

    public function getName($order = 0)
    {
        if (null != $this->getUser()) {
            return $this->getUser()->getName($order);
        }
        
        if ($order != 0) {
            return $this->getFirstName().' '.mb_strtoupper($this->getSurname());
        }

        return mb_strtoupper($this->getSurname()).' '.$this->getFirstName();
    }
        
    public function __toString()
    {
        if (null != $this->getUser()) {
            return $this->getUser()->__toString();
        }
        
        return $this->getName();
    }

    public function getAvailableTitle()
    {
        if (null == $this->getTitle() and null != $this->getUser()) {
            return $this->getUser()->getTitle();
        }

        return $this->getTitle();
    }
        
    public function getAvailableEmail()
    {
        if (null == $this->getEmail() and null != $this->getUser()) {
            return $this->getUser()->getEmail();
        }

        return $this->getEmail();
    }
}
