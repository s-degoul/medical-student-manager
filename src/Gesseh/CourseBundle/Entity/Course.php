<?php

namespace Gesseh\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Gesseh\CourseBundle\Entity\Author;
use Gesseh\CourseBundle\Entity\Licence;
use Gesseh\CourseBundle\Entity\CourseMaterial;
use Gesseh\CourseBundle\Entity\Tag;
use Gesseh\UserBundle\Entity\Group;

/**
 * Course
 *
 * @ORM\Table(name="course")
 * @ORM\Entity(repositoryClass="Gesseh\CourseBundle\Entity\CourseRepository")
 */
class Course
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\CourseBundle\Entity\Author", inversedBy="courses")
     * @ORM\JoinTable(name="course_author",
     joinColumns={@ORM\JoinColumn(name="course_id", referencedColumnName="id")},
     inverseJoinColumns={@ORM\JoinColumn(name="author_id", referencedColumnName="id")})
     */
    private $authors;

    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\CourseBundle\Entity\Licence", inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $licence;

    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="course_group",
     joinColumns={@ORM\JoinColumn(name="course_id", referencedColumnName="id")},
     inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")})
    */
    private $groups;

    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="course_visibility",
     joinColumns={@ORM\JoinColumn(name="course_id", referencedColumnName="id")},
     inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")})
    */
    private $visibilities;

    /**
     * @ORM\OneToMany(targetEntity="Gesseh\CourseBundle\Entity\CourseMaterial", mappedBy="course", cascade={"persist","remove"})
     */
    private $courseMaterials;
    
    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\CourseBundle\Entity\Tag", inversedBy="courses", cascade={"persist"})
     * @ORM\JoinTable(name="course_tag",
     joinColumns={@ORM\JoinColumn(name="course_id", referencedColumnName="id")},
     inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")})
    */
    private $tags;


    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        $this->courseMaterials = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Course
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Course
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Course
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add an author
     *
     * @param \Gesseh\CourseBundle\Entity\Author $author
     * @return Course
     */
    public function addAuthor(Author $author)
    {
        if (!$this->authors->contains($author)) {
            $this->authors->add($author);
        }

        return $this;
    }

    /**
     * Remove an author
     *
     * @param \Gesseh\CourseBundle\Entity\Author $author
     * @return Course
     */
    public function removeAuthor(Author $author)
    {
        $this->authors->removeElement($author);

        return $this;
    }

    /**
     * Get authors
     *
     * @return ArrayCollection 
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Set licence
     *
     * @param \Gesseh\CourseBundle\Entity\Licence $licence
     * @return Course
     */
    public function setLicence(Licence $licence)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return \Gesseh\CourseBundle\Entity\Licence 
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Add a group
     *
     * @param Group $group
     * @return Course
     */
    public function addGroup(Group $group)
    {
        if (! $this->groups->contains($group)) {
            $this->groups->add($group);
        }

        return $this;
    }

    /**
     * Remove a group
     *
     * @param Group $group
     * @return Course
     */
    public function removeGroup(Group $group)
    {
        $this->groups->removeElement($group);

        return $this;
    }

    /**
     * Get groups
     *
     * @return ArrayCollection 
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Add a visibility
     *
     * @param Group $visibilities
     * @return Course
     */
    public function addVisibility(Group $visibility)
    {
        if (! $this->visibilities->contains($visibility)) {
            $this->visibilities->add($visibility);
        }

        return $this;
    }

    /**
     * Remove a visibility
     *
     * @param Group $visibility
     * @return Course
     */
    public function removeVisibility(Group $visibility)
    {
        $this->visibilities->removeElement($visibility);

        return $this;
    }

    /**
     * Get visibilities
     *
     * @return ArrayCollection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Add a courseMaterial
     *
     * @param CourseMaterial $courseMaterial
     * @return Course
     */
    public function addCourseMaterial(CourseMaterial $courseMaterial)
    {
        if (! $this->courseMaterials->contains($courseMaterial)) {
            $this->courseMaterials->add($courseMaterial);
            $courseMaterial->setCourse($this);
        }

        return $this;
    }

    /**
     * Remove a courseMaterial
     *
     * @param CourseMaterial $courseMaterial
     * @return Course
     */
    public function removeCourseMaterial(CourseMaterial $courseMaterial)
    {
        $this->courseMaterials->removeElement($courseMaterial);

        return $this;
    }

    /**
     * Get courseMaterials
     *
     * @return ArrayCollection 
     */
    public function getCourseMaterials()
    {
        return $this->courseMaterials;
    }

   /**
     * Add a tag
     *
     * @param Tag $tag
     * @return Course
     */
    public function addTag(Tag $tag)
    {
        if (! $this->tags->contains($tag)) {
            $this->tags->add($tag);
        }

        return $this;
    }

    /**
     * Remove a tag
     *
     * @param Tag $tag
     * @return Course
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    /**
     * Get tags
     *
     * @return ArrayCollection 
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function __toString()
    {
        return $this->getTitle();
    }
}
