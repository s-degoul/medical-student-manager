<?php

namespace Gesseh\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Gesseh\CourseBundle\Entity\Course;
use Gesseh\UserBundle\Entity\Group;

/**
 * Tag
 *
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="Gesseh\CourseBundle\Entity\TagRepository")
 */
class Tag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="rank", type="integer", nullable=true)
     */
    private $rank;
    
    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="tag_group",
     joinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")},
     inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")})
    */
    private $groups;

    /**
     * @ORM\ManyToMany(targetEntity="Gesseh\CourseBundle\Entity\Course", mappedBy="tags")
     */
    private $courses;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->courses = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return Tag
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return Tag
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }

    
    /**
     * Add a group
     *
     * @param Group $group
     * @return Tag
     */
    public function addGroup(Group $group)
    {
        if (! $this->groups->contains($group)) {
            $this->groups->add($group);
        }

        return $this;
    }

    /**
     * Remove a group
     *
     * @param Group $group
     * @return Tag
     */
    public function removeGroup(Group $group)
    {
        $this->groups->removeElement($group);

        return $this;
    }

    /**
     * Get groups
     *
     * @return ArrayCollection 
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Get courses
     *
     * @return ArrayCollection
     */
    public function getCourses()
    {
        return $this->courses;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
