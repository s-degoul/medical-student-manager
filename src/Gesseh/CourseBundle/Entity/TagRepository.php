<?php

namespace Gesseh\CourseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TagRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TagRepository extends EntityRepository
{
    private function getTagQuery()
    {
        return $this->createQueryBuilder('t');
    }

    private function getCompleteTagQuery()
    {
        $qb = $this->getTagQuery();

        $qb->leftJoin('t.groups', 'g')
            ->addSelect('g')
            ->leftJoin('t.courses', 'c')
            ->addSelect('c');

        return $qb;
    }

    public function getAll()
    {
        $qb = $this->getCompleteTagQuery();

        return $qb->getQuery()
            ->getResult();
    }

    public function getCompleteTag($id)
    {
        $qb = $this->getCompleteTagQuery();
        $qb->where($qb->expr()->eq('t.id', $id));

        return $qb->getQuery()
            ->getOneOrNullResult();
    }
}
