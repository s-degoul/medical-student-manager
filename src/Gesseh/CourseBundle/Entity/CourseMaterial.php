<?php

namespace Gesseh\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gesseh\CourseBundle\Entity\Licence;
use Gesseh\CourseBundle\Entity\Course;
/* use Gesseh\CoreBundle\Service\FileHandler\File as File; */
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * CourseMaterial
 *
 * @ORM\Table(name="course_material")
 * @ORM\Entity(repositoryClass="Gesseh\CourseBundle\Entity\CourseMaterialRepository")
 * @Vich\Uploadable
 */
class CourseMaterial /* extends File */
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="course_material", fileNameProperty="name")
     */
    private $courseMaterialFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\CourseBundle\Entity\Licence")
     * @ORM\JoinColumn(name="licence_id", referencedColumnName="id", nullable=true)
     */
    private $licence;

    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\CourseBundle\Entity\Course", inversedBy="courseMaterials")
     *
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", nullable=false)
     */
    private $course;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CourseMaterial
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * set a file
     *
     * @param File|UploadedFile $courseMaterialFile
     * @return CourseMaterial
     */
    public function setCourseMaterialFile(File $courseMaterialFile = null)
    {
        $this->courseMaterialFile = $courseMaterialFile;

        if ($courseMaterialFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');

            $this->name = $courseMaterialFile->getFileName();
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getCourseMaterialFile()
    {
        return $this->courseMaterialFile;
    }

    /**
     * set date of update
     *
     * @param \DateTime
     * @return CourseMaterial
     */
    public function setUpdatedAt(\DateTime $date)
    {
        $this->updatedAt = $date;

        return $this;
    }

    /**
     * get date of update
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return CourseMaterial
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CourseMaterial
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set licence
     *
     * @param Licence $licence
     * @return CourseMaterial
     */
    public function setLicence(Licence $licence = null)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return Licence 
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Set course
     *
     * @param Course $course
     * @return CourseMaterial
     */
    public function setCourse(Course $course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return Course 
     */
    public function getCourse()
    {
        return $this->course;
    }

    public function __toString()
    {
        if (null != $this->getTitle()) {
            return $this->getTitle();
        }
        
        return $this->getName();
    }
}
