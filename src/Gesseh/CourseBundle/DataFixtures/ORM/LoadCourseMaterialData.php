<?php

namespace Gesseh\CourseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Gesseh\CourseBundle\Entity\CourseMaterial;

class LoadCourseMaterialData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $courseMaterials = array(
            array(
                'name' => "56c31ef32c2fa_formation_zotero.pdf",
                'updatedAt' => new \DateTime('now'),
                'title' => "support",
                'description' => null,
                'course' => 'introduction_zotero'
            ),
            array(
                'name' => "56c31ef32cceb_formation_Zotero-sources.zip",
                'updatedAt' => new \DateTime('now'),
                'title' => "sources",
                'description' => "sources en Latex (édition initiale avec Org mode pour Emacs)",
                'course' => 'introduction_zotero'
            ),
            array(
                'name' => "56c3225c7a3d9_Curr_Op_Anaesth_2010-anesthesia_severe_COPD.pdf",
                'updatedAt' => new \DateTime('now'),
                'title' => "article",
                'description' => null,
                'course' => 'anesthesia_copd'
            ),
            array(
                'name' => "56c3239ff3f96_file_format_science.odp",
                'updatedAt' => new \DateTime('now'),
                'title' => "source",
                'description' => "format odp (LibreOffice Impress)\nConversion en PDF grâce à l'extension Expand animations",
                'course' => 'file_format_science'
            ),
            array(
                'name' => "56c3239ff3625_file_format_science.pdf",
                'updatedAt' => new \DateTime('now'),
                'title' => "presentation",
                'description' => null,
                'course' => 'file_format_science'
            ),
            array(
                'name' => "56c325617fb34_these-libre_sante.pdf",
                'updatedAt' => new \DateTime('now'),
                'title' => "document",
                'description' => null,
                'course' => 'these_libre_sante'
            ),
            array(
                'name' => "56c3200721a10_09.11.26-PEC_NVPO.pdf",
                'updatedAt' => new \DateTime('now'),
                'title' => "référenciel",
                'description' => null,
                'course' => 'nvpo_postop'
            ),
            array(
                'name' => "56c3216624612_09.11.26-article_gestion_traitement_peri-op.pdf",
                'updatedAt' => new \DateTime('now'),
                'title' => "article",
                'description' => null,
                'course' => 'traitements_long_cours'
            ),
        );

        $listCourseMaterials = array();
        
        foreach ($courseMaterials as $i => $courseMaterial) {
            $listCourseMaterials[$i] = new CourseMaterial();

            $listCourseMaterials[$i]->setName($courseMaterial['name']);
            $listCourseMaterials[$i]->setUpdatedAt($courseMaterial['updatedAt']);
            /* $listCourseMaterials[$i]->setPath($courseMaterial['path']); */
            /* $listCourseMaterials[$i]->setMimeType($courseMaterial['mimeType']); */
            $listCourseMaterials[$i]->setTitle($courseMaterial['title']);
            $listCourseMaterials[$i]->setDescription($courseMaterial['description']);
            if (isset ($courseMaterial['licence'])) {
                $listCourseMaterials[$i]->setLicence($this->getReference('licence_'.$courseMaterial['licence']));
            }
            $listCourseMaterials[$i]->setCourse($this->getReference('course_'.$courseMaterial['course']));
            

            $manager->persist($listCourseMaterials[$i]);

            $this->addReference('courseMaterial_'.$courseMaterial['name'], $listCourseMaterials[$i]);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}