<?php

namespace Gesseh\CourseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Gesseh\CourseBundle\Entity\Licence;

class LoadLicenceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $licences = array(
            array(
                'name' => 'CC_BY_SA',
                'title' => "Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International",
                'shortTitle' => "CC-BY-SA",
                'description' => "Share — copy and redistribute the material in any medium or format\nAdapt — remix, transform, and build upon the material for any purpose, even commercially.\nThe licensor cannot revoke these freedoms as long as you follow the license terms.",
                'website' => "http://creativecommons.org/licenses/by-sa/4.0/",
                'free' => 1
            ),
            array(
                'name' => 'GNU_v3',
                'title' => "Licence publique générale version 3",
                'shortTitle' => "GNU v3",
                'description' => null,
                'website' => "http://www.gnu.org/licenses/gpl.html",
                'free' => 1
            ),
            array(
                'name' => 'copyright',
                'title' => "Copyright",
                'shortTitle' => "copyright",
                'description' => "aucun droit",
                'website' => null,
                'free' => 0
            ),
        );

        $listLicences = array();
        
        foreach ($licences as $i => $licence) {
            $listLicences[$i] = new Licence();

            $listLicences[$i]->setTitle($licence['title']);
            $listLicences[$i]->setShortTitle($licence['shortTitle']);
            $listLicences[$i]->setDescription($licence['description']);
            $listLicences[$i]->setWebsite($licence['website']);
            $listLicences[$i]->setFree($licence['free']);

            $manager->persist($listLicences[$i]);

            $this->addReference('licence_'.$licence['name'], $listLicences[$i]);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}