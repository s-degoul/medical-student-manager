<?php

namespace Gesseh\CourseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Gesseh\CourseBundle\Entity\Tag;

class LoadTagData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tags = array(
            array(
                'pseudo_id' => 'anesth_module_1',
                'name' => "DES anesth Module 1",
                'title' => "Module 1",
                'rank' => 1,
                'groups' => array('DES_anesth')
            ),
            array(
                'pseudo_id' => 'anesth_module_2',
                'name' => "DES anesth Module 2",
                'title' => "Module 2",
                'rank' => 2,
                'groups' => array('DES_anesth')
            ),
            array(
                'pseudo_id' => 'anesth_module_3',
                'name' => "DES anesth Module 3",
                'title' => "Module 3",
                'rank' => 3,
                'groups' => array('DES_anesth')
            ),
            array(
                'pseudo_id' => 'anesth_module_4',
                'name' => "DES anesth Module 4",
                'title' => "Module 4",
                'rank' => 3,
                'groups' => array('DES_anesth')
            ),
            array(
                'pseudo_id' => 'cardio_module_1',
                'name' => "DES cardio Module 1",
                'title' => "Module 1",
                'rank' => 1,
                'groups' => array('DES_cardio')
            ),
            array(
                'pseudo_id' => 'cardio_module_2',
                'name' => "DES cardio Module 2",
                'title' => "Module 2",
                'rank' => 2,
                'groups' => array('DES_cardio')
            ),
            array(
                'pseudo_id' => 'intubation',
                'name' => "intubation",
                'title' => null,
                'rank' => null,
                'groups' => array()
            ),
            array(
                'pseudo_id' => 'cpa',
                'name' => "CPA",
                'title' => null,
                'rank' => null,
                'groups' => array()
            ),
            array(
                'pseudo_id' => 'nvpo',
                'name' => "NVPO",
                'title' => null,
                'rank' => null,
                'groups' => array()
            ),
            array(
                'pseudo_id' => 'choc',
                'name' => "choc",
                'title' => null,
                'rank' => null,
                'groups' => array()
            ),
            array(
                'pseudo_id' => 'pharmacologie',
                'name' => "pharmacologie",
                'title' => null,
                'rank' => null,
                'groups' => array()
            ),
            array(
                'pseudo_id' => 'cardio',
                'name' => "Cardiologie",
                'title' => null,
                'rank' => null,
                'groups' => array()
            ),
            array(
                'pseudo_id' => 'partage_notes',
                'name' => "Partage de notes",
                'title' => "Partage de notes",
                'rank' => 4,
                'groups' => array()
            ),
            array(
                'pseudo_id' => 'biostatistiques',
                'name' => "Biostatistiques",
                'title' => null,
                'rank' => null,
                'groups' => array()
            ),
            array(
                'pseudo_id' => 'methodologie',
                'name' => "Méthodologie",
                'title' => null,
                'rank' => null,
                'groups' => array()
            ),
            array(
                'pseudo_id' => 'licence_libre',
                'name' => "licence libre",
                'title' => null,
                'rank' => null,
                'groups' => array()
            ),
        );

        $listTags = array();
        
        foreach ($tags as $i => $tag) {
            $listTags[$i] = new Tag();

            $listTags[$i]->setName($tag['name']);
            $listTags[$i]->setTitle($tag['title']);
            $listTags[$i]->setRank($tag['rank']);
            foreach ($tag['groups'] as $group) {
                $listTags[$i]->addGroup($this->getReference('group_'.$group));
            }

            $manager->persist($listTags[$i]);

            $this->addReference('tag_'.$tag['pseudo_id'], $listTags[$i]);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}