<?php

namespace Gesseh\CourseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Gesseh\CourseBundle\Entity\Author;

class LoadAuthorData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $authors = array(
            array(
                'name' => 'w.robin',
                'user' => null,
                'firstname' => "William",
                'surname' => "Robin",
                'title' => "Dr",
                'email' => null,
                'discipline' => "MCU PH d'anesthésie-réanimation",
                'description' => "Travaille en réanimation chirurgicale"
            ),
            array(
                'name' => 'b.durand',
                'user' => null,
                'firstname' => "Benoît",
                'surname' => "Durand",
                'title' => "Pr",
                'email' => "benoit.durand@univ-lille5.fr",
                'discipline' => "Professeur d'anesthésie-réanimation",
                'description' => null
            ),
            array(
                'user' => 'user0',
                'firstname' => null,
                'surname' => null,
                'title' => null,
                'email' => "samuel.degoul@opmbx.org",
                'discipline' => "interne en anesthésie-réanimation",
                'description' => "Centres d'intérêt : anesthésie, informatique libre, statistiques, recherche reproductible"
            ),
            array(
                'name' => 'sfar',
                'user' => null,
                'firstname' => null,
                'surname' => 'SFAR',
                'title' => null,
                'email' => null,
                'discipline' => null,
                'description' => "Société Française d'Anesthésie et de Réanimation"
            ),
            array(
                'name' => 'other',
                'user' => null,
                'firstname' => null,
                'surname' => 'Autre',
                'title' => null,
                'email' => null,
                'discipline' => null,
                'description' => null
            ),
        );

        $listAuthors = array();
        
        foreach ($authors as $i => $author) {
            $listAuthors[$i] = new Author();

            if (null != $author['user']) {
                $listAuthors[$i]->setUser($this->getReference('user_'.$author['user']));
            }
            
            $listAuthors[$i]->setFirstname($author['firstname']);
            $listAuthors[$i]->setSurname($author['surname']);
            $listAuthors[$i]->setTitle($author['title']);
            $listAuthors[$i]->setEmail($author['email']);
            $listAuthors[$i]->setDiscipline($author['discipline']);
            $listAuthors[$i]->setDescription($author['description']);

            $manager->persist($listAuthors[$i]);

            if (null != $author['user']) {
                $this->addReference('author_'.$author['user'], $listAuthors[$i]);
            }
            elseif (isset ($author['name']) and null != $author['name']) {
                $this->addReference('author_'.$author['name'], $listAuthors[$i]);
            }
            else {
                throw new \Exception("No name for datafixture Author : ".$author['surname'].' '.$author['firstname']);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}