<?php

namespace Gesseh\CourseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Gesseh\CourseBundle\Entity\Course;

class LoadCourseData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $courses = array(
            array(
                'name' => 'nvpo_postop',
                'title' => "Prise en charge des nausées et vomissements postopératoires",
                'description' => "Conférence d'expert Présenté au congrès de la SFAR, 2007",
                'active' => 1,
                'licence' => 'copyright',
                'authors' => array('sfar'),
                'groups' => array('DES_anesth'),
                'visibilities' => array('DES_anesth', 'DESC_rea_med'),
                'tags' => array('nvpo','pharmacologie','anesth_module_1')
            ),
            array(
                'name' => 'traitements_long_cours',
                'title' => "Traitement au long cours : que faire avant l'anesthésie ?",
                'description' => null,
                'active' => 1,
                'licence' => 'copyright',
                'authors' => array('other'),
                'groups' => array('DES_anesth'),
                'visibilities' => array('DES_anesth'),
                'tags' => array('cpa','anesth_module_1')
            ),
            array(
                'name' => 'anesthesia_copd',
                'title' => "Anesthesia for patients with severe chronic obstructive pulmonary disease",
                'description' => null,
                'active' => 1,
                'licence' => 'copyright',
                'authors' => array('other'),
                'groups' => array('DES_anesth'),
                'visibilities' => array('DES_anesth'),
                'tags' => array('anesth_module_4')
            ),
            array(
                'name' => 'introduction_zotero',
                'title' => "Introduction à Zotero",
                'description' => "Zotero est un excellent outils de gestion de références bibliographiques. Ce document est un support de présentation pour un cours introductif (à un TP par exemple)",
                'active' => 1,
                'licence' => 'CC_BY_SA',
                'authors' => array('user0'),
                'groups' => array('DES_anesth'),
                'visibilities' => array(),
                'tags' => array('partage_notes','methodologie')
            ),
            array(
                'name' => 'file_format_science',
                'title' => "File format for scientific text document",
                'description' => "Description de l'intérêt de l'utilisation de format ouvert pour les documents scientifiques en une diapo",
                'active' => 1,
                'licence' => 'CC_BY_SA',
                'authors' => array('user0'),
                'groups' => array('DES_anesth'),
                'visibilities' => array(),
                'tags' => array('partage_notes','licence_libre')
            ),
            array(
                'name' => 'these_libre_sante',
                'title' => "Thèse : Libre et Santé",
                'description' => "Comprendre le logiciel libre et ses enjeux pour les professions de santé (par Nicolas Floquet http://www.librehealthcare.flqt.fr/)",
                'active' => 1,
                'licence' => 'CC_BY_SA',
                'authors' => array('other'),
                'groups' => array('DES_anesth'),
                'visibilities' => array(),
                'tags' => array('partage_notes','licence_libre')
            ),
        );

        $listCourses = array();
        
        foreach ($courses as $i => $course) {
            $listCourses[$i] = new Course();
            
            $listCourses[$i]->setTitle($course['title']);
            $listCourses[$i]->setDescription($course['description']);
            $listCourses[$i]->setActive($course['active']);
            $listCourses[$i]->setLicence($this->getReference('licence_'.$course['licence']));

            foreach ($course['authors'] as $author) {
                $listCourses[$i]->addAuthor($this->getReference('author_'.$author));
            }
            foreach ($course['groups'] as $group) {
                $listCourses[$i]->addGroup($this->getReference('group_'.$group));
            }
            foreach ($course['visibilities'] as $visilibity) {
                $listCourses[$i]->addVisibility($this->getReference('group_'.$visilibity));
            }
            foreach ($course['tags'] as $tag) {
                $listCourses[$i]->addTag($this->getReference('tag_'.$tag));
            }

            $manager->persist($listCourses[$i]);

            $this->addReference('course_'.$course['name'], $listCourses[$i]);
        }            

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}