<?php

namespace Gesseh\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;


class CourseMaterialType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('courseMaterialFile', 'vich_file', array(
                      'required' => false,
                      'allow_delete' => true,
                      'download_link' => true,
                      'label' => "File"
                  ))
            /* ->add('name', 'text', array( */
            /*           'required' => true, */
            /*           'label' => "File name" */
            /*       )) */
            ->add('title', 'text', array(
                      'required' => false,
                      'label' => "Title"
                  ))
            ->add('description', 'textarea', array(
                      'required' => false,
                      'label' => "Description"
                  ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CourseBundle\Entity\CourseMaterial',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'course_material';
    }
}
