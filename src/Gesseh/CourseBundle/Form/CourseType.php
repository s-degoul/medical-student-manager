<?php

namespace Gesseh\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Gesseh\UserBundle\Service\RoleChecker\RoleChecker;

/**
 * CourseType
 */
class CourseType extends AbstractType
{
    private $roleChecker;

    public function __construct(RoleChecker $roleChecker)
    {
        $this->roleChecker = $roleChecker;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('authors', 'entity', array(
                      'class' => 'GessehCourseBundle:Author',
                      'property' => 'Name',
                      'multiple' => true,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'label' => "Authors"
                  ))
            ->add('title', 'text', array(
                      'required' => true,
                      'label' => "Title"
                  ))
            ->add('description', 'textarea', array(
                      'required' => false,
                      'label' => "Description"
                  ))
            ->add('active', ChoiceType::class, array(
                      'choices'  => array(
                          'Yes' => true,
                          'No' => false,
                      ),
                      'choices_as_values' => true,
                      'required' => false,
                      'placeholder' => false,
                      'expanded' => true,
                      'multiple' => false,
                      'data' => true,
                      'label' => "Active ?"
                  ))
            ->add('licence', 'entity', array(
                      'class' => 'GessehCourseBundle:Licence',
                      'property' => 'shortTitle',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => true,
                      'label' => "Licence"
                  ))
            ->add('groups', 'entity', array(
                      'class' => 'GessehUserBundle:Group',
                      'choices' => array_merge($this->roleChecker->getReachableObjects('ROLE_ADMIN_COURSE', 'Group'), $this->roleChecker->getReachableObjects('ROLE_USER_MANAGE_COURSE', 'Group')),
                      'property' => 'completeTitle',
                      'multiple' => true,
                      'expanded' => false,
                      'required' => true,
                      'empty_value' => '--choose--',
                      'label' => "Groups"
                  ))
            ->add('visibilities', 'entity', array(
                      'class' => 'GessehUserBundle:Group',
                      'property' => 'completeTitle',
                      'multiple' => true,
                      'expanded' => false,
                      'required' => false,
                      'empty_value' => '--choose--',
                      'label' => "Visibility"
                  ))
            ->add('courseMaterials', 'collection', array(
                      'type' => 'course_material',
                      'allow_add' => true,
                      'allow_delete' => true,
                      'by_reference' => false,
                      'required' => false,
                  ))
            ->add('tags', 'entity', array(
                      'class' => 'GessehCourseBundle:Tag',
                      'property' => 'name',
                      'multiple' => true,
                      'expanded' => true,
                      'required' => true,
                      'label' => "Tags"
                  ))
            ->add('newTags', 'collection', array(
                      'type' => 'tag_subtype',
                      'mapped' => false,
                      'required' => false,
                      'allow_add' => true,
                      'allow_delete' => true,
                      'by_reference' => false
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ))
            ->add('reset', 'reset', array(
                      'label' => "Reset"
                  ));
    }

    public function getName()
    {
        return 'course';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CourseBundle\Entity\Course',
            ));
    }
}
