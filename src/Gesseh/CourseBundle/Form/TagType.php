<?php

namespace Gesseh\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class TagType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                      'required' => true,
                      'label' => "Name"
                  ))
            ->add('title', 'text', array(
                      'required' => false,
                      'label' => "Title"
                  ))
            ->add('rank', 'integer', array(
                      'required' => false,
                      'precision' => 0,
                      'label' => "Rank"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ))
            ->add('reset', 'reset', array(
                      'label' => "Reset"
                  ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CourseBundle\Entity\Tag',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tag';
    }
}
