<?php

namespace Gesseh\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class AuthorUserAutoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('user')
            ->remove('firstname')
            ->remove('surname')
            ->remove('title');
    }

    public function getParent()
    {
        return new AuthorType();
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CourseBundle\Entity\Author',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'author_user_auto_type';
    }
}
