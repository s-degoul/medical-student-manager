<?php

namespace Gesseh\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class AuthorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', 'entity', array (
                      'class' => 'GessehUserBundle:User',
                      'property' => 'name',
                      'multiple' => false,
                      'expanded' => false,
                      'required' => false,
                      'empty_value' => '--choose--',
                      'label' => "User"
                  ))
            ->add('firstname', 'text', array(
                      'required' => false,
                      'label' => "Firstname"
                  ))
            ->add('surname', 'text', array(
                      'required' => false,
                      'label' => "Surname"
                  ))
            ->add('title', 'text', array(
                      'required' => false,
                      'label' => "Title"
                  ))
            ->add('email', 'email', array(
                      'required' => false,
                      'label' => 'Email',
                  ))
            ->add('discipline', 'text', array(
                      'required' => false,
                      'label' => "Discipline"
                  ))
            ->add('description', 'textarea', array(
                      'required' => false,
                      'label' => "Description"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ))
            ->add('reset', 'reset', array(
                      'label' => "Reset"
                  ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CourseBundle\Entity\Author',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'author';
    }
}
