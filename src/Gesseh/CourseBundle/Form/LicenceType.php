<?php

namespace Gesseh\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * LicenceType
 */
class LicenceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                      'required' => true,
                      'label' => "Title"
                  ))
            ->add('shortTitle', 'text', array(
                      'required' => false,
                      'label' => "Short title"
                  ))
            ->add('description', 'textarea', array(
                      'required' => false,
                      'label' => "Description"
                  ))
            ->add('website', 'url', array(
                      'default_protocol' => 'http',
                      'required' => false,
                      'label' => "Website"
                  ))
            ->add('free', ChoiceType::class, array(
                      'choices'  => array(
                          'Yes' => true,
                          'No' => false,
                      ),
                      'choices_as_values' => true,
                      'required' => true,
                      'placeholder' => false,
                      'expanded' => true,
                      'multiple' => false,
                      'label' => "Free licence ?"
                  ))
            ->add('save', 'submit', array(
                      'label' => "Save"
                  ))
            ->add('reset', 'reset', array(
                      'label' => "Reset"
                  ));
    }

    public function getName()
    {
        return 'licence';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Gesseh\CourseBundle\Entity\Licence',
            ));
    }
}
