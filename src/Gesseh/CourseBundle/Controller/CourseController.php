<?php

namespace Gesseh\CourseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\Request;

use Gesseh\CourseBundle\Entity\Course;
use Gesseh\CourseBundle\Form\CourseType;
use Gesseh\CourseBundle\Entity\Licence;
use Gesseh\CourseBundle\Form\LicenceType;
use Gesseh\CourseBundle\Entity\Tag;
use Gesseh\CourseBundle\Form\TagType;
use Gesseh\CourseBundle\Entity\Author;
use Gesseh\CourseBundle\Form\AuthorType;
use Gesseh\CourseBundle\Form\AuthorUserAutoType;


/**
 * @Route("")
 */
class CourseController extends Controller
{
    /**
     * List of available courses
     *
     * @Route("/courses", name="GCourse_ListCourses")
     * @Template()
     */
    public function listCoursesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $courses = array();
        $groups = array();

        // Some courses are visible even for non authenticated users (courses with visibility = null)
        if (! $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $courses = $em->getRepository('GessehCourseBundle:Course')->getCourseWithNoVisibilityRestriction();
        }
        else {
            $groups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_USER_DISPLAY_COURSE', 'Group');
            $groups = $this->get('gesseh_user.groups_hierarchy')->getParentGroups($groups);

            $courses = $em->getRepository('GessehCourseBundle:Course')->getCourseByVisibilities($groups, true);
            /* foreach($courses as $c) { */
            /*     echo $c->getTitle(); */
            /* } */
        }

        // To sort courses by family group
        $coursesGroupedByFamilies = array();
        foreach ($this->get('gesseh_user.groups_hierarchy')->getFamilies($groups) as $family) {
            $coursesGroupedByFamilies[] = array(
                'family' => $family,
                'courses' => array(),
                /* 'tags' => array() */
            );
        }
        // For courses not concerned by any reachable family group
        $coursesGroupedByFamilies[] = array(
            'family' => 'others',
            'courses' => array(),
            /* 'tags' => array() */
        );

        // List of main tags (with a rank)
        $rankedTags = array();
        // List of other tags
        $otherTags = array();
        
        foreach ($courses as $course) {
            $families = $this->get('gesseh_user.groups_hierarchy')->getFamilies($course->getGroups()->toArray());
            $considered = false;
            foreach ($coursesGroupedByFamilies as $key => $coursesGroupedByFamily) {
                if (in_array ($coursesGroupedByFamily['family'], $families)) {
                    $coursesGroupedByFamilies[$key]['courses'][] = $course;
                    $considered = true;
                }
            }
            if (! $considered) {
                $coursesGroupedByFamilies[count($coursesGroupedByFamilies)-1]['courses'][] = $course;
            }

            foreach ($course->getTags() as $tag) {
                if (null != $tag->getRank() and ! in_array ($tag, $rankedTags)) {
                    $rankedTags[] = $tag;
                }
                elseif (null == $tag->getRank() and ! in_array($tag, $otherTags)) {
                    $otherTags[] = $tag;
                }
            }
        }

        // Asc. sorting of ranked tags
        usort($rankedTags, function($a, $b) {
                return $a->getRank() > $b->getRank() ? 1 : -1;
            });
        // Alphabetical sorting of other tags
        usort($otherTags, function($a, $b) {
                $al = strtolower($a->getTitle());
                $bl = strtolower($b->getTitle());
                if ($al == $bl) {return 0;}
                return ($al > $bl) ? 1 : -1;
            });
        // Tags searched
        $searchTags = array();
        foreach (array_merge($rankedTags, $otherTags) as $tag) {
            if (null != $request->get('tag_'.$tag->getId(), null)) {
                $searchTags[] = $tag;
            }
        }

        if (!empty ($searchTags)) {
            foreach ($coursesGroupedByFamilies as $key1 => $coursesGroupedByFamily) {
                foreach ($coursesGroupedByFamily['courses'] as $key2 => $course) {
                    $tagPresent = false;
                    foreach ($course->getTags() as $tag) {
                        if (in_array ($tag, $searchTags)) {
                            $tagPresent = true;
                            break;
                        }
                    }
                    if (false == $tagPresent) {
                        unset($coursesGroupedByFamilies[$key1]['courses'][$key2]);
                    }
                }
            }
        }

        return array(
            'coursesGroupedByFamilies' => $coursesGroupedByFamilies,
            'rankedTags' => $rankedTags,
            'otherTags' => $otherTags,
            'searchTags' => $searchTags,
            'authenticatedUser' => $this->getUser()
        );
    }


    /**
     * Download a file of a course
     *
     * @Route("/coursematerial/{id}/download", name="GCourse_DownloadCourseMaterial", requirements={"id" = "\d+"})
     */
    public function downloadCourseMaterialAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $courseMaterial = $em->getRepository('GessehCourseBundle:CourseMaterial')->find($id);

        if (null == $courseMaterial) {
            throw $this->createNotFoundException("Unable to find this course material");
        }

        $downloadHandler = $this->get('vich_uploader.download_handler');

        return $downloadHandler->downloadObject($courseMaterial, $fileField = 'courseMaterialFile');
    }


    /**
     * @Route("/course/new", name="GCourse_NewCourse")
     * @Template("GessehCourseBundle:Course:editCourse.html.twig")
     * @Security("has_role('ROLE_ADMIN_COURSE') or has_role('ROLE_USER_MANAGE_COURSE')")
     */
    public function newCourseAction()
    {
        $em = $this->getDoctrine()->getManager();

        $course = new Course;

        $form = $this->createForm(new CourseType($this->get('gesseh_user.role_checker')), $course, array(
                                      'cancel_action' => $this->generateUrl('GCourse_ListCourses')
                                  ));
        $request = $this->get('request');

        $form->handleRequest($request);

        $newTagsErrors = array();
        if ($form->get('newTags')->getData()) {
            $newTagsErrors = $this->get('validator')->validate($form->get('newTags')->getData());
        }
            
        if ($form->isValid() and count($newTagsErrors) == 0) {
            foreach ($form->get('newTags')->getData() as $newTag) {
                /* echo get_class($n).'<br>'; */
                $course->addTag($newTag);
            }
            
            $em->persist($course);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Course "'.$course->__toString().'" registered');

            return $this->redirect($this->generateUrl('GCourse_ListCourses'));
        }
        

        return array(
            'form' => $form->createView(),
            'newTagsErrors' => $newTagsErrors
        );
    }

    /**
     * Edit a course
     *
     * @Route("/course/{id}/edit", name="GCourse_EditCourse", requirements={"id" = "\d+"})
     * @Template()
     * @Security("has_role('ROLE_ADMIN_COURSE') or has_role('ROLE_USER_MANAGE_COURSE')")
     */
    public function editCourseAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $course = $em->getRepository('GessehCourseBundle:Course')->find($id);

        if (null == $course) {
            throw $this->createNotFoundException("Unable to find this course");
        }

        $form = $this->createForm(new CourseType($this->get('gesseh_user.role_checker')), $course, array(
                                      'cancel_action' => $this->generateUrl('GCourse_ListCourses')
                                  ));
        $request = $this->get('request');

        $originalCourseMaterials = $course->getCourseMaterials()->toArray();

        $form->handleRequest($request);

        $newTagsErrors = array();
        if ($form->get('newTags')->getData()) {
            $newTagsErrors = $this->get('validator')->validate($form->get('newTags')->getData());
        }
        
        if ($form->isValid() and count($newTagsErrors) == 0) {
            foreach ($originalCourseMaterials as $originalCourseMaterial) {
                if (! $course->getCourseMaterials()->contains($originalCourseMaterial)) {
                    $em->remove($originalCourseMaterial);
                }
            }
            
            foreach ($form->get('newTags')->getData() as $newTag) {
                /* echo get_class($n).'<br>'; */
                $course->addTag($newTag);
            }
            
            $em->persist($course);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Course "'.$course->__toString().'" updated');

            return $this->redirect($this->generateUrl('GCourse_ListCourses'));
        }

        return array(
            'form' => $form->createView(),
            'newTagsErrors' => $newTagsErrors
        );
    }
    
    /**
     * Delete a course
     *
     * @Route("/course/{id}/delete", name="GCourse_DeleteCourse", requirements={"id" = "\d+"})
     * @Security("has_role('ROLE_ADMIN_COURSE') or has_role('ROLE_USER_MANAGE_COURSE')")
     */
    public function deleteCourseAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $course = $em->getRepository('GessehCourseBundle:Course')->find($id);

        if (! $course) {
            throw $this->createNotFoundException("Unable to find this course");
        }

        $em->remove($course);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Course "' . $course->__toString() . '" deleted');

        return $this->redirect($this->generateUrl('GCourse_ListCourses'));
    }

    
   /**
     * Manage licences
     *
     * @Route("/licence/management", name="GCourse_AdminManageLicence")
     * @Template()
     * @Security("has_role('ROLE_ADMIN_LICENCE')")
     */
    public function manageLicencesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $licences = $em->getRepository('GessehCourseBundle:Licence')->getAll();

        $newLicence = new Licence();
        $form = $this->createForm(new LicenceType(), $newLicence);
        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($newLicence);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Registration of licence "' . $newLicence->__toString() . '" completed');

            return $this->redirect($this->generateUrl('GCourse_AdminManageLicence'));
        }

        return array(
            'form' => $form->createView(),
            'licences' => $licences
        );
    }

    
    /**
     * Edit a licence
     *
     * @Route("/licence/{id}/edit", name="GCourse_AdminEditLicence", requirements={"id" = "\d+"})
     * @Template("GessehCourseBundle:Course:manageLicences.html.twig")
     */
    public function editLicenceAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $licences = $em->getRepository('GessehCourseBundle:Licence')->getAll();
        $licence = $em->getRepository('GessehCourseBundle:Licence')->getCompleteLicence($id);

        $form = $this->createForm(new LicenceType(), $licence, array(
                                      'cancel_action' => $this->generateUrl('GCourse_AdminManageLicence')
                                  ));

        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Licence "'.$licence->__toString().'" updated');
            return $this->redirect($this->generateUrl('GCourse_AdminManageLicence'));
        }

        return array(
            'licences' => $licences,
            'id' => $id,
            'form' => $form->createView()
        );
    }

    /**
     * Delete a licence
     *
     * @Route("/licence/{id}/delete", name="GCourse_AdminDeleteLicence", requirements={"id" = "\d+"})
     */
    public function deleteLicenceAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $licence = $em->getRepository('GessehCourseBundle:Licence')->getCompleteLicence($id);

        if (! $licence) {
            throw $this->createNotFoundException("Unable to find this licence");
        }
        
        if (count ($licence->getCourses()) > 0) {
            $this->get('session')->getFlashBag()->add('warning', "Licence \"".$licence->__toString()."\" can't be deleted because it is used by at least one course");
        }
        else {
            $this->get('session')->getFlashBag()->add('success', 'Licence "'.$licence->__toString().'" deleted');
            $em->remove($licence);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('GCourse_AdminManageLicence'));
    }

    /**
     * Show licence informations
     *
     * @Route("/licence/{id}/show", name="GCourse_ShowLicence", requirements={"id" = "\d+"})
     * @Template()
     */
    public function showLicenceAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $licence = $em->getRepository("GessehCourseBundle:Licence")->find($id);

        if (! $licence) {
            throw $this->createNotFoundException("Unable to find this licence");
        }

        return array(
            'licence' => $licence
        );
    }


    /**
     * Manage tags
     *
     * @Route("/tag/management", name="GCourse_AdminManageTag")
     * @Template()
     * @Security("has_role('ROLE_ADMIN_TAG')")
     */
    public function manageTagsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tags = $em->getRepository('GessehCourseBundle:Tag')->getAll();

        $newTag = new Tag();
        $form = $this->createForm(new TagType(), $newTag);
        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($newTag);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Registration of tag "'.$newTag->__toString().'" completed');

            return $this->redirect($this->generateUrl('GCourse_AdminManageTag'));
        }

        return array(
            'form' => $form->createView(),
            'tags' => $tags
        );
    }
 
    /**
     * Edit a tag
     *
     * @Route("/tag/{id}/edit", name="GCourse_AdminEditTag", requirements={"id" = "\d+"})
     * @Template("GessehCourseBundle:Course:manageTags.html.twig")
     */
    public function editTagAction($id)
    {
       $em = $this->getDoctrine()->getManager();

        $tags = $em->getRepository('GessehCourseBundle:Tag')->getAll();
        $tag = $em->getRepository('GessehCourseBundle:Tag')->getCompleteTag($id);

        $form = $this->createForm(new TagType(), $tag, array(
                                      'cancel_action' => $this->generateUrl('GCourse_AdminManageTag')
                                  ));

        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Tag "'.$tag->__toString().'" updated');
            return $this->redirect($this->generateUrl('GCourse_AdminManageTag'));
        }

        return array(
            'tags' => $tags,
            'id' => $id,
            'form' => $form->createView()
        );
    }

    /**
     * Delete a tag
     *
     * @Route("/tag/{id}/delete", name="GCourse_AdminDeleteTag", requirements={"id" = "\d+"})
     */
    public function deleteTagAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $tag = $em->getRepository('GessehCourseBundle:Tag')->getCompleteTag($id);

        if (! $tag) {
            throw $this->createNotFoundException("Unable to find this tag");
        }

        if (count ($tag->getCourses()) > 0) {
            $this->get('session')->getFlashBag()->add('warning', "Tag \"".$tag->__toString()."\" can't be deleted because it is used by at least one course");
        }
        else {
            $this->get('session')->getFlashBag()->add('success', 'Tag "'.$tag->__toString().'" deleted');
            $em->remove($tag);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('GCourse_AdminManageTag'));
    }


    /**
     * Manage authors
     *
     * @Route("/author/management", name="GCourse_AdminManageAuthor")
     * @Template()
     * @Security("has_role('ROLE_ADMIN_AUTHOR') or has_role('ROLE_USER_ADD_COURSE')")
     */
    public function manageAuthorsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $authors = array();
        
        if ($this->get('security.authorization_checker')->isGranted(new Expression("has_role('ROLE_ADMIN_AUTHOR')"))) {
            $authors = $em->getRepository('GessehCourseBundle:Author')->getAll();
            $author = new Author();
            $form = $this->createForm(new AuthorType(), $author);
        }
        else {
            $author = $em->getRepository('GessehCourseBundle:Author')->getByUser($this->getUser());
            if (null == $author) {
                $author = new Author();
            }
            $author->setUser($this->getUser());
            $form = $this->createForm(new AuthorUserAutoType(), $author);
        }
           
        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($author);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Registration of author "'.$author->__toString().'" completed');

            return $this->redirect($this->generateUrl('GCourse_AdminManageAuthor'));
        }

        return array(
            'form' => $form->createView(),
            'authors' => $authors
        );
    }
 
    /**
     * Edit an author
     *
     * @Route("/author/{id}/edit", name="GCourse_AdminEditAuthor", requirements={"id" = "\d+"})
     * @Template("GessehCourseBundle:Course:manageAuthors.html.twig")
     * @Security("has_role('ROLE_ADMIN_AUTHOR')")
     */
    public function editAuthorAction($id)
    {
       $em = $this->getDoctrine()->getManager();

        $authors = $em->getRepository('GessehCourseBundle:Author')->getAll();
        $author = $em->getRepository('GessehCourseBundle:Author')->getCompleteWithCoursesAndGroupsById($id);

        $reachableAdminCourseGroups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_ADMIN_COURSE', 'Group');

        $unreachableAdminCourseGroups = array();
        
        foreach ($author->getCourses() as $course) {
            foreach ($course->getGroups() as $group) {
                if (! in_array($group, $reachableAdminCourseGroups)) {
                    $unreachableAdminCourseGroups[] = $course->__toString().' (group '.$group->__toString().')';
                }
            }
        }

        if (count ($unreachableAdminCourseGroups) > 0) {
            $this->get('session')->getFlashBag()->add('warning', "You are not allowed to modify this author because he/she did course(s) for group(s) you are not allowed to manage : ".implode(', ', $unreachableAdminCourseGroups));
            return $this->redirect($this->generateUrl('GCourse_AdminManageAuthor'));
        }
                
        $form = $this->createForm(new AuthorType(), $author, array(
                                      'cancel_action' => $this->generateUrl('GCourse_AdminManageAuthor')
                                  ));

        $request = $this->get('request');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Author "'.$author->__toString().'" updated');
            return $this->redirect($this->generateUrl('GCourse_AdminManageAuthor'));
        }

        return array(
            'authors' => $authors,
            'id' => $id,
            'form' => $form->createView()
        );
    }

    /**
     * Delete an author
     *
     * @Route("/author/{id}/delete", name="GCourse_AdminDeleteAuthor", requirements={"id" = "\d+"})
     * @Security("has_role('ROLE_ADMIN_AUTHOR') or has_role('ROLE_USER_ADD_COURSE')")
     * @Template()
     */
    public function deleteAuthorAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->get('security.authorization_checker')->isGranted(new Expression("has_role('ROLE_ADMIN_AUTHOR')"))) {
            $author = $em->getRepository('GessehCourseBundle:Author')->getCompleteWithCoursesAndGroupsById($id);
        }
        else {
            $author = $em->getRepository('GessehCourseBundle:Author')->getByUser($this->getUser());
        }

        if (! $author) {
            throw $this->createNotFoundException("Unable to find this author");
        }

        if ($this->get('security.authorization_checker')->isGranted(new Expression("has_role('ROLE_ADMIN_AUTHOR')"))) {
            $reachableAdminCourseGroups = $this->get('gesseh_user.role_checker')->getReachableObjects('ROLE_ADMIN_COURSE', 'Group');

            $unreachableAdminCourseGroups = array();
        
            foreach ($author->getCourses() as $course) {
                foreach ($course->getGroups() as $group) {
                    if (! in_array($group, $reachableAdminCourseGroups)) {
                        $unreachableAdminCourseGroups[] = $course->__toString().' (group '.$group->__toString().')';
                    }
                }
            }

            if (count ($unreachableAdminCourseGroups) > 0) {
                $this->get('session')->getFlashBag()->add('warning', "You are not allowed to delete this author because he/she did course(s) for group(s) you are not allowed to manage : ".implode(', ', $unreachableAdminCourseGroups));
                return $this->redirect($this->generateUrl('GCourse_AdminManageAuthor'));
            }
        }

        $getRequest = $this->get('request')->request;
        if (null != $getRequest->get('confirm_delete_author', null)) {
            foreach($author->getCourses() as $course) {
                $course->removeAuthor($author);
            }
            $this->get('session')->getFlashBag()->add('success', 'Author "'.$author->__toString().'" deleted');
            $em->remove($author);
            $em->flush();

            return $this->redirect($this->generateUrl('GCourse_AdminManageAuthor'));
        }

        return array(
            'remainingCourses' => $author->getCourses(),
            'author' => $author
        );
    }    
}