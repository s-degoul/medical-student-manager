<?php

namespace Gesseh\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class HomeController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('GHome_Presentation'));
    }
    
    /**
     * @Route("/home/presentation", name="GHome_Presentation")
     * @Template()
     */
    public function presentationAction()
    {
        return array();
    }

    /**
     * @Route("/home/contact", name="GHome_Contact")
     * @Template()
     */
    public function contactAction()
    {
        return array();
    }
}
