<?php

namespace Gesseh\ScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gesseh\CoreBundle\Service\PeriodHandler\PeriodInterface;
use Gesseh\CoreBundle\Entity\Job;
use Gesseh\UserBundle\Entity\User;


/**
 * JobSchedule
 *
 * @ORM\Table(name="job_schedule")
 * @ORM\Entity(repositoryClass="Gesseh\ScheduleBundle\Repository\JobScheduleRepository")
 */
class JobSchedule implements PeriodInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime", nullable=false)
     */
    private $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\CoreBundle\Entity\Job")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=false)
     */
    private $job;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return JobSchedule
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return JobSchedule
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set user
     *
     * @param \Gesseh\UserBundle\Entity\User $user
     *
     * @return JobSchedule
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Gesseh\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set job
     *
     * @param \Gesseh\CoreBundle\Entity\Job $job
     *
     * @return JobSchedule
     */
    public function setJob(Job $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return \Gesseh\CoreBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }
}
