<?php

namespace Gesseh\ScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SchedulePattern
 *
 * @ORM\Table(name="schedule_pattern")
 * @ORM\Entity(repositoryClass="Gesseh\ScheduleBundle\Repository\SchedulePatternRepository")
 */
class SchedulePattern
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="objectType", type="string", length=30, nullable=false)
     */
    private $objectType;

    /**
     * @var int
     *
     * @ORM\Column(name="objectId", type="integer", nullable=false)
     */
    private $objectId;

    /**
     * @var array
     *
     * @ORM\Column(name="days", type="array", nullable=false)
     */
    private $days;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startTime", type="time", nullable=false)
     */
    private $startTime;

    /**
     * @var string
     *
     * @ORM\Column(name="duration", type="integer", nullable=false)
     */
    private $duration;

    /**
     * @var int
     *
     * @ORM\Column(name="rank", type="integer", nullable=true)
     */
    private $rank;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set objectType
     *
     * @param string $objectType
     *
     * @return SchedulePattern
     */
    public function setObjectType($objectType)
    {
        $this->objectType = $objectType;

        return $this;
    }

    /**
     * Get objectType
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     * Set objectId
     *
     * @param integer $objectId
     *
     * @return SchedulePattern
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId
     *
     * @return int
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set days
     *
     * @param array $days
     *
     * @return SchedulePattern
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days
     *
     * @return array
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return SchedulePattern
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set duration
     *
     * @param string $duration
     *
     * @return SchedulePattern
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return SchedulePattern
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return int
     */
    public function getRank()
    {
        return $this->rank;
    }
}

