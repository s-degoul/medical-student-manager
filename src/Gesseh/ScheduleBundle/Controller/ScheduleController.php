<?php

namespace Gesseh\ScheduleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * @Route("/schedule")
 */
class ScheduleController extends Controller
{
    /**
     * @Route("/schedule", name="GSchedule_ShowSchedule")
     * @Template()
     */
    public function showScheduleAction()
    {
        return array();
    }
}