<?php

namespace Gesseh\ScheduleBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Gesseh\ScheduleBundle\Entity\JobSchedule;

class LoadJobScheduleData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $jobSchedules = array(
            array(
                'job' => 'int_rea_NC',
                'user' => 'user0',
                'startDate' => new \DateTime('2016-03-07 08:00:00'),
                'endDate' => new \DateTime('2016-03-07 13:00:00')
            ),
            array(
                'job' => 'int_rea_NC',
                'user' => 'user0',
                'startDate' => new \DateTime('2016-03-07 14:00:00'),
                'endDate' => new \DateTime('2016-03-07 17:30:00')
            ),
            array(
                'job' => 'int_rea_NC',
                'user' => 'user0',
                'startDate' => new \DateTime('2016-03-08 08:00:00'),
                'endDate' => new \DateTime('2016-03-08 13:00:00')
            ),
            array(
                'job' => 'int_rea_NC',
                'user' => 'user0',
                'startDate' => new \DateTime('2016-03-08 14:00:00'),
                'endDate' => new \DateTime('2016-03-08 17:30:00')
            ),
            array(
                'job' => 'int_rea_NC',
                'user' => 'user0',
                'startDate' => new \DateTime('2016-03-09 08:00:00'),
                'endDate' => new \DateTime('2016-03-09 13:00:00')
            ),
            array(
                'job' => 'int_rea_NC',
                'user' => 'user0',
                'startDate' => new \DateTime('2016-03-07 08:00:00'),
                'endDate' => new \DateTime('2016-03-07 13:00:00')
            ),
            array(
                'job' => 'int_rea_NC',
                'user' => 'user0',
                'startDate' => new \DateTime('2016-03-10 14:00:00'),
                'endDate' => new \DateTime('2016-03-10 18:00:00')
            ),
            array(
                'job' => 'int_rea_NC',
                'user' => 'user0',
                'startDate' => new \DateTime('2016-03-11 08:00:00'),
                'endDate' => new \DateTime('2016-03-11 13:00:00')
            ),
            array(
                'job' => 'int_rea_NC',
                'user' => 'user0',
                'startDate' => new \DateTime('2016-03-11 14:00:00'),
                'endDate' => new \DateTime('2016-03-11 17:00:00')
            ),
            array(
                'job' => 'int_rea_NC',
                'user' => 'user0',
                'startDate' => new \DateTime('2016-03-12 08:00:00'),
                'endDate' => new \DateTime('2016-03-12 13:30:00')
            ),
        );

        foreach ($jobSchedules as $jobSchedule) {
            $js = new JobSchedule();

            $js->setJob($this->getReference('job_'.$jobSchedule['job']));
            $js->setUser($this->getReference('user_'.$jobSchedule['user']));
            $js->setStartDate($jobSchedule['startDate']);
            $js->setEndDate($jobSchedule['endDate']);

            $manager->persist($js);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}