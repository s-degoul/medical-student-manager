<?php

namespace Gesseh\CalendarBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class GessehCalendarBundle extends Bundle
{
    public function getParent()
    {
        return 'BladeTesterCalendarBundle';
    }
}
