<?php

namespace Gesseh\DutyBundle\Classes;

use Gesseh\DutyBundle\Entity\PlannedDuty;

class Schedule
{
    private $plannedDuties;
    
    public function __construct()
    {
        $this->plannedDuties = array();
    }
    
    public function getPlannedDuties()
    {
        return $this->plannedDuties;
    }
    
    public function addPlannedDuty(PlannedDuty $plannedDuty)
    {
        $this->plannedDuties[] = $plannedDuty;
        
        return $this;
    }
    
    public function addPlannedDuties(array $plannedDuties)
    {
        //~ $this->plannedDuties = array_merge($this->plannedDuties, $plannedDuties);
        foreach ($plannedDuties as $plannedDuty)
        {
            $this->addPlannedDuty($plannedDuty);
        }
        
        return $this;
    }
}
