<?php

namespace Gesseh\DutyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Gesseh\DutyBundle\Entity\DutyPost;
use Gesseh\DutyBundle\Entity\PlannedDuty;
use Gesseh\DutyBundle\Form\PlannedDutyScheduleType;
//~ use Gesseh\DutyBundle\Form\PlannedDutyHandler;
use Gesseh\DutyBundle\Entity\PlannedDutyValidator;

use Gesseh\DutyBundle\Classes\Schedule;

/**
 * ManageDuty controller.
 */

class ManageDutyController extends Controller
{
    /**
     * @Route ("/create/schedule", name="GDuty_create_schedule")
     * @Template()
     */
    public function createScheduleAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $availableRoutes = array(
                        'GDuty_create_schedule_form' => "formulaire",
                        'GDuty_create_schedule_file' => "fichier",
                        'GDuty_create_schedule_auto' => "automatique"
                        );
        
        if($this->getRequest()->getMethod() == "POST")
        {
            $post = $this->getRequest()->request;
            
            if (array_key_exists($post->get('route'), $availableRoutes))
                return $this->redirect($this->generateURL($post->get('route'),
                                                   array('startDate' => $post->get('startDate'), 'endDate' => $post->get('endDate'), 'dutyPostId' => $post->get('dutyPostId'))));
        }
        
        $dutyPostsList = $em->getRepository('GessehDutyBundle:DutyPost')->findAll();
        
        return array('dutyPostsList' => $dutyPostsList, 'availableRoutes' => $availableRoutes);
    }
    
    /**
     * @Route ("/create/schedule/form/{dutyPostId}/{startDate}_{endDate}", name="GDuty_create_schedule_form", requirements={"dutyPostId"="\d+", "startDate"="\d{4}\-\d{2}\-\d{2}", "endDate"="\d{4}\-\d{2}\-\d{2}"})
     * @Template()
     */
    public function createScheduleByFormAction($dutyPostId, $startDate, $endDate)
    {
        $em = $this->getDoctrine()->getManager();
        $periodManager = $this->get('gesseh_core.period_manager');
        $schedulePeriod = $periodManager->createPeriod(new \DateTime($startDate.' 00:00:00'), new \DateTime($endDate.' 23:59:59'));
        
        $usersList = $em->getRepository('GessehUserBundle:User')->findAll();
        $dutyPost = $em->getRepository('GessehDutyBundle:DutyPost')->find($dutyPostId);
        $existingPlannedDuties = $em->getRepository('GessehDutyBundle:PlannedDuty')->getByCriterion($schedulePeriod->getStartDate(), $schedulePeriod->getEndDate(), array('dutyPost' => $dutyPostId));
        
        $periodsList = $periodManager->getSubPeriodsList($schedulePeriod);
        
        $schedule = new Schedule();
        
        foreach ($periodsList as $period)
        {
            $getPreviousPeriods = $periodManager->getPreviousPeriods($period, $existingPlannedDuties, true);
            $plannedDutiesToAdd = $getPreviousPeriods[0];
            $existingPlannedDuties = $getPreviousPeriods[1];
            $schedule->addPlannedDuties($plannedDutiesToAdd);
            
            $plannedDuty = new PlannedDuty($dutyPost, $period->getStartDate(), $period->getEndDate());
            $schedule->addPlannedDuty($plannedDuty);
        }
        $schedule->addPlannedDuties($existingPlannedDuties);
        
        $form = $this->createFormBuilder($schedule)
                    ->add('plannedDuties', 'collection', array('type' => new PlannedDutyScheduleType()))
                    ->getForm();
        
        $violationsList = array();
        
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST')
        {
            $form->bind($request);
//~ return new Response('<pre>'.print_r($request->request, true).'</pre>');

            $cacheManager = $this->get('gesseh_core.cache_manager');
            $cacheManager->addCache('schedule_planned_duties', $schedule->getPlannedDuties());
           
            foreach ($schedule->getPlannedDuties() as $plannedDuty)
            {
                $validator = $this->get('validator');
                $violations = $validator->validate($plannedDuty);
                
                if ($violations->count() > 0)
                {
                    $key = spl_object_hash($plannedDuty);
                    $violationsList[$key] = array (
                                    'startDate' => $plannedDuty->getStartDate(),
                                    'endDate' => $plannedDuty->getEndDate(),
                                    'violations' => array());
                    
                    for ($i = 0; $i < $violations->count(); $i++)
                    {
                        if ($violations->has($i))
                            $violationsList[$key]['violations'][] = $violations->get($i);
                    }
                }
                else
                {
                    $em->persist($plannedDuty);
                }
            }
            
            $em->flush();
            
            //~ $this->get('session')->getFlashBag()->add('notice', "Planning enregistré");
        }
        
        return array('dutyPost' => $dutyPost, 'form' => $form->createView(), 'violationsList' => $violationsList, 'schedulePeriod' => $schedulePeriod);
    }
}
