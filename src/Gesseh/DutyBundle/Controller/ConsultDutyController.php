<?php

namespace Gesseh\DutyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * ConsultDuty controller.
 */

class ConsultDutyController extends Controller
{
    /**
     * @Route("/", name="GDuty_homepage")
     * @Template()
     */
	public function indexAction()
	{
		$route = 'GDuty_consult_schedule';
		
		return $this->redirect($this->generateUrl($route));
	}
	
	/**
     * @Route("/consult/schedule", name="GDuty_consult_schedule")
     * @Template() 
	 */
    public function consultScheduleAction()
    {
        $em = $this->getDoctrine()->getManager();
        $duty_post_list = $em->getRepository('GessehDutyBundle:DutyPost')->findAll();
        $user_list = $em->getRepository('GessehUserBundle:User')->findAll();
        
        $post = $this->getRequest()->request;
        
        $duties = $em->getRepository('GessehDutyBundle:PlannedDuty')->getByCriterion(new \DateTime("2014-04-01 00:00:00"), new \DateTime("2014-10-31 00:00:00"), array('user' => $post->get('user'), 'dutyPost' => $post->get('duty_post')));      

		return array('duties' => $duties, 'dutyPostList' => $duty_post_list, 'userList' => $user_list);
    }
    
    /**
     * @Route("/consult/duty/{id}", name="GDuty_consult_duty")
     * @Template() 
	 */
    public function consultDutyAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $duty = $em->getRepository('GessehDutyBundle:PlannedDuty')->getById($id);
        
        return array('duty' => $duty);
    }
}
