<?php

namespace Gesseh\DutyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlannedDutyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', 'datetime', array('time_widget' => 'single_text'))
            ->add('endDate', 'datetime', array('time_widget' => 'single_text'))
            ->add('dutyPost', 'entity', array(
                            'class' => 'GessehDutyBundle:DutyPost',
                            'property' => 'name',
                            'multiple' => false,
                            'expanded' => false,
                            'empty_value' => '--choisir--'))
            ->add('user', 'entity', array(
                            'class' => 'GessehUserBundle:User',
                            'property' => 'usernameCanonical',
                            'multiple' => false,
                            'expanded' => false,
                            'empty_value' => '--choisir--'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => '\Gesseh\DutyBundle\Entity\PlannedDuty'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gesseh_dutybundle_plannedduty';
    }
}
