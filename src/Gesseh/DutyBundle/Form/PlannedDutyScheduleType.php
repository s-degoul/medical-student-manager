<?php

namespace Gesseh\DutyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlannedDutyScheduleType extends PlannedDutyType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->remove('dutyPost');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gesseh_dutybundle_planneddutyschedule';
    }
}
