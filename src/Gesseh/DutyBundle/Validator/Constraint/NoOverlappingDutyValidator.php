<?php

namespace Gesseh\DutyBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;
use Gesseh\CoreBundle\Service\PeriodManager\PeriodManager;
use Gesseh\CoreBundle\Service\CacheManager\CacheManager;


class NoOverlappingDutyValidator extends ConstraintValidator
{
    private $em;
    private $periodManager;
    private $cacheManager;
    
    public function __construct(EntityManager $em, PeriodManager $periodManager, CacheManager $cacheManager)
    {
        $this->em = $em;
        $this->periodManager = $periodManager;
        $this->cacheManager = $cacheManager;
    }
    
    public function validate($plannedDuty, Constraint $constraint)
    {
        $MIN_INTERVAL = 'PT24H';

        $overlappingPeriod = $this->periodManager->createPeriod(clone($plannedDuty->getStartDate()), clone($plannedDuty->getEndDate()));
        $overlappingPeriod = $this->periodManager->enlargePeriod($overlappingPeriod, new \DateInterval($MIN_INTERVAL));
        
        $userId = $plannedDuty->getUser()->getId();
        
        //~ $closeDuties = array();
        //~ foreach ($allPlannedDutiesForm as $duty)
        //~ {
            //~ if($duty !== $this->plannedDuty and $duty->getDateStart() < $this->plannedDuty->getDateStart() and $duty->getDateEnd() > $period->getDateMin())
                //~ $closeDuties[] = $duty;
        //~ }
        
        $schedulePlannedDuties = $this->cacheManager->getCache('schedule_planned_duties', true)->getValues();

        $scheduleStartDate = reset($schedulePlannedDuties)->getStartDate();
        $scheduleEndDate = end($schedulePlannedDuties)->getEndDate();
        
        if (!$this->cacheManager->hasCache('planned_duties_by_user'))
        {
            $this->cacheManager->addCache('planned_duties_by_user');
            $plannedDutiesByUser = array();
        }
        else
            $plannedDutiesByUser = $this->cacheManager->getCache('planned_duties_by_user', true)->getValues();
            
        if (!array_key_exists ($userId, $plannedDutiesByUser))
        {
            $plannedDutiesByUser[$userId] = $this->em->getRepository('GessehDutyBundle:PlannedDuty')->getByCriterion($scheduleStartDate, $scheduleEndDate, array('user' => $userId));
            foreach ($schedulePlannedDuties as $schedulePlannedDuty)
            {
                if ($schedulePlannedDuty->getUser()->getId() == $userId)
                    $plannedDutiesByUser[$userId][] = $schedulePlannedDuty;
            }
            
            $this->cacheManager->getCache('planned_duties_by_user', true)->setValues($plannedDutiesByUser);
        }
        
        $overlappingDuties = array(); //$this->em->getRepository('GessehDutyBundle:PlannedDuty')->getByCriterion($period->getStartDate(), $period->getEndDate(), array('user' => array($plannedDuty->getUser()->getId())));
        
        foreach ($overlappingDuties as $key => $duty)
        {
            if ($plannedDuty == $duty)
                unset ($overlappingDuties[$key]);
        }
        
        if (!empty ($overlappingDuties))
        {
            $this->context->addViolation($constraint->message, array());//, count($overlappingDuties), null, 2);
        }
    }
}
