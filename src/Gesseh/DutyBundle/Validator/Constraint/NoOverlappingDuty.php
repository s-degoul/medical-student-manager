<?php

namespace Gesseh\DutyBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NoOverlappingDuty extends Constraint
{
    public $message = "Une autre garde est déjà enregistrée pour cette période !";
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
    public function validatedBy()
    {
        return 'no_overlapping_duty';
    }
}
