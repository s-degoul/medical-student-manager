<?php

namespace Gesseh\DutyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DutyPost
 *
 * @ORM\Table(name="duty_post")
 * @ORM\Entity(repositoryClass="Gesseh\DutyBundle\Entity\DutyPostRepository")
 */
class DutyPost
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\CoreBundle\Entity\Department", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $departement;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DutyPost
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set departement
     *
     * @param \Gesseh\CoreBundle\Entity\Department $departement
     * @return DutyPost
     */
    public function setDepartement(\Gesseh\CoreBundle\Entity\Department $departement)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return \Gesseh\CoreBundle\Entity\Department 
     */
    public function getDepartement()
    {
        return $this->departement;
    }
}
