<?php
namespace Gesseh\DutyBundle\Entity;


class PlannedDutyValidator
{
    private $plannedDuty, $em, $container;
    
    public function __construct(\Gesseh\DutyBundle\Entity\PlannedDuty $plannedDuty, \Symfony\Component\DependencyInjection\Container $container)
    {
        $this->plannedDuty = $plannedDuty;
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
    }
    
    public function hasNotCloseDuties(array $allPlannedDutiesForm)
    {
        $MIN_INTERVAL = 'PT24H';
        
        $period = $this->container->get('gesseh_core.period_manager');
        $period->setDateMin($this->plannedDuty->getDateStart());
        $period->setDateMax($this->plannedDuty->getDateEnd());
        $period->setPeriodAround(new \DateInterval($MIN_INTERVAL));
        
        $closeDuties = array();
        foreach ($allPlannedDutiesForm as $duty)
        {
            if($duty !== $this->plannedDuty and $duty->getDateStart() < $this->plannedDuty->getDateStart() and $duty->getDateEnd() > $period->getDateMin())
                $closeDuties[] = $duty;
        }
        
        $closeDuties = array_merge($closeDuties, $this->em->getRepository('GessehDutyBundle:PlannedDuty')->getByCriterion($period->getDateMin(), $period->getDateMax(), array('user' => array($this->plannedDuty->getUser()->getId()))));
        
        if (!empty ($closeDuties))
            return $closeDuties;
        
        return true;
    }
    
    public function isValid()
    {
        
    }
}
