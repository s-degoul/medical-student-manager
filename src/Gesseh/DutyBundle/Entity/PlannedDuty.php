<?php

namespace Gesseh\DutyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Gesseh\DutyBundle\Validator\Constraint\NoOverlappingDuty;
use Gesseh\CoreBundle\Service\PeriodHandler\Period;

/**
 * PlannedDuty
 *
 * @ORM\Table(name="planned_duty")
 * @ORM\Entity(repositoryClass="Gesseh\DutyBundle\Entity\PlannedDutyRepository")
 */
class PlannedDuty extends Period
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime")
     */
    private $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\DutyBundle\Entity\DutyPost", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $dutyPost;
    
    /**
     * @ORM\ManyToOne(targetEntity="Gesseh\UserBundle\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
     private $user;


    public function __construct(\Gesseh\DutyBundle\Entity\DutyPost $dutyPost = null, \DateTime $startDate = null, \DateTime $endDate = null, \Gesseh\UserBundle\Entity\User $user = null)
    {
        $this->dutyPost = $dutyPost;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->user = $user;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return PlannedDuty
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return PlannedDuty
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set dutyPost
     *
     * @param \Gesseh\DutyBundle\Entity\DutyPost $dutyPost
     * @return PlannedDuty
     */
    public function setDutyPost(\Gesseh\DutyBundle\Entity\DutyPost $dutyPost)
    {
        $this->dutyPost = $dutyPost;

        return $this;
    }

    /**
     * Get dutyPost
     *
     * @return \Gesseh\DutyBundle\Entity\DutyPost 
     */
    public function getDutyPost()
    {
        return $this->dutyPost;
    }

    /**
     * Set user
     *
     * @param \Gesseh\UserBundle\Entity\User $user
     * @return PlannedDuty
     */
    public function setUser(\Gesseh\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Gesseh\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    public function __toString()
    {
        return "Garde du ".$this->startDate->format('d/m/Y H i')." au ".$this->endDate->format('d/m/Y H i');
    }
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new NoOverlappingDuty());
    }
}
